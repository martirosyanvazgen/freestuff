@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Редактирование Страница',
    'activePage' => 'pages',
    'activeNav' => 'pages',
])
@section('title')
    <title>Редактирование Страница</title>
@endsection
@section('content')
    <div class="panel-header">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Редактирование Страница') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{route('admin.pages') }}"
                                   class="btn btn-primary btn-round">{{ __('Обратно к списк') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-regular">
                            <ul class="nav nav-tabs " id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                                       aria-controls="profile" aria-selected="false">Armenia</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                                       aria-controls="contact" aria-selected="false">Russia</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                       aria-controls="home" aria-selected="true">English</a>
                                </li>
                            </ul>
                            <form method="POST" action="{{ route('admin.pages.update', $page->id) }}">
                                @csrf
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="profile" role="tabpanel"
                                         aria-labelledby="profile-tab">
                                        <div class="form-group">
                                            <label for="title_am" class="col-form-label">Заголовок <span
                                                        class="asterisk">*</span></label>
                                            <input type="text" name="title_am" id="title_am" class="form-control{{ $errors->has('title_am') ? ' is-invalid' : '' }}"
                                                   placeholder="{{  $page->title_am }}"  value="{{old('title_am', $page->title_am)}}">
                                            @include('alerts.feedback', ['field' => 'title_am'])
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Текст <span class="asterisk">*</span></label>
                                            <textarea name="text_am" id="text-am" class="form-control{{ $errors->has('text_am') ? ' is-invalid' : '' }}">
                                                {{old('text_am', $page->text_am)}}
                                            </textarea>
                                            @include('alerts.feedback', ['field' => 'text_am'])
                                        </div>
                                    </div>

                                    <div class="tab-pane fade " id="home" role="tabpanel" aria-labelledby="home-tab">
                                        <div class="form-group">
                                            <label for="title_ru" class="col-form-label">Заголовок  </label>
                                            <input type="text" name="title_ru" id="title_ru" class="form-control{{ $errors->has('title_ru') ? ' is-invalid' : '' }}"
                                                   placeholder="{{ $page->title_ru }}"   value="{{old('title_ru', $page->title_ru)}}">
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Текст </label>
                                            <textarea name="text_ru" id="text-ru" class="form-control{{ $errors->has('text_ru') ? ' is-invalid' : '' }}">
                                                {{old('text_ru', $page->text_ru)}}
                                            </textarea>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                        <div class="form-group">
                                            <label for="title_en" class="col-form-label">Заголовок </label>
                                            <input type="text" name="title_en" id="title_en" class="form-control{{ $errors->has('title_en') ? ' is-invalid' : '' }}"
                                                   placeholder="{{$page->title_en}}" value="{{old('title_en', $page->title_en)}}">
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Текст </label>
                                            <textarea name="text_en" id="text-en" class="form-control{{ $errors->has('text_en') ? ' is-invalid' : '' }}">
                                                {{old('text_en', $page->text_en)}}
                                            </textarea>
                                        </div>
                                    </div>

                                    @foreach ($errors->all() as $error)
                                        <span class="has-error">
                                            <small style="color: #fb6340">{{ $error }}</small><br>
                                        </span>
                                    @endforeach
                                    <br><br>
                                    <div class="text-left">
                                        <button type="submit" class="btn btn-primary mt-4">Сoхранить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.config.autoParagraph = false;
        CKEDITOR.replace('text_am', {
            filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace('text_ru', {
            filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace('text_en', {
            filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
    </script>
@endpush
