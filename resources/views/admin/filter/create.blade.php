@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Фильтры',
    'activePage' => 'filters',
    'activeNav' => 'filters',
])

@section('title')
    <title>Фильтры</title>
@endsection

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Фильтры</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('admin.filters') }}" class="btn btn-primary btn-round">Обратно к списку</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-regular">
                            <ul class="nav nav-tabs " id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home"
                                       role="tab" aria-controls="home" aria-selected="true">Армянский</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#ru" role="tab"
                                       aria-selected="false">Русский</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#en" role="tab"
                                       aria-selected="false">Английский</a>
                                </li>
                            </ul>
                            <form method="post" action="{{ route('admin.filters.store') }}">
                                @csrf
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-value-am">Значение <span class="asterisk">*</span> </label>
                                            <input type="text" name="value_am" id="input-value-am" class="form-control{{ $errors->has('value_am') ? ' is-invalid' : '' }}" placeholder="Значение" value="{{ old('value_am') }}">

                                            @include('alerts.feedback', ['field' => 'value_am'])
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="ru" role="tabpanel" aria-labelledby="ru-tab">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-value-ru">Значение</label>
                                            <input type="text" name="value_ru" id="input-value-ru" class="form-control{{ $errors->has('value_ru') ? ' is-invalid' : '' }}" placeholder="Значение" value="{{ old('value_ru') }}">

                                            @include('alerts.feedback', ['field' => 'value_ru'])
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-value-en">Значение</label>
                                            <input type="text" name="value_en" id="input-value-en" class="form-control{{ $errors->has('value_en') ? ' is-invalid' : '' }}" placeholder="Значение" value="{{ old('value_en') }}">

                                            @include('alerts.feedback', ['field' => 'value_en'])
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label" for="select-filter-group-id">Группа Фильтров <span class="asterisk">*</span> </label>
                                        <select class="form-control" name="group_id" id="select-filter-group-id">
                                            @if($filterGroups->count())
                                                @foreach($filterGroups as $filterGroup)
                                                    <option {{ (old('group_id') == $filterGroup->id)? 'selected' : ''  }} value="{{ $filterGroup->id }}">{{ $filterGroup->name_ru }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @include('alerts.feedback', ['field' => 'group_id'])
                                    </div>

                                    @foreach ($errors->all() as $error)

                                        <span class="has-error">
                                            <small style="color: #fb6340">{{ $error }}</small><br>
                                        </span>

                                    @endforeach
                                    <br/><br/>
                                    <div class="text-left">
                                        <button type="submit" class="btn btn-primary mt-4">Дoбавить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection