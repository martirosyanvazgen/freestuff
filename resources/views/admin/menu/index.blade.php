@extends('layouts.app', [
    'namePage' => 'Меню',
    'class' => 'sidebar-mini',
    'activePage' => 'menus',
    'activeNav' => '',
])

@section('title')
    <title>Меню</title>
@endsection
@section('content')
    <div class="panel-header">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary btn-round text-white pull-right"
                           href="{{ route('admin.menu.create') }}">Добавить меню</a>
                        <h4 class="card-title">Меню</h4>
                        <div class="col-12 mt-2">
                            @include('alerts.success', ['key' => 'success'])
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Заголовок</th>
                                <th>Адрес</th>
                                <th>Страница</th>
                                <th>Родитель</th>
                                <th>Тип</th>
                                <th class="disabled-sorting text-right">Действия</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Заголовок</th>
                                <th>Адрес</th>
                                <th>Страница</th>
                                <th>Родитель</th>
                                <th>Тип</th>
                                <th class="disabled-sorting text-right">Действия</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($menus as $key => $menu)
                                <tr>
                                    <td>
                                        {{ ($menus->firstItem() + $key).'.' }}
                                    </td>
                                    <td>{{$menu['title_am']}}</td>
                                    <td>{{$menu['slug']}}</td>
                                    <td>{{$menu->menuPage['title_am']}}</td>
                                    <td>
                                        @if($menu['parent_id'])
                                            {{$menu->parent['title_am']}}
                                        @else
                                            No parent
                                        @endif
                                    </td>
                                    <td><span class="btn btn-primary submit" data-type="{{ $menu->menuType['name'] }}" id="{{ $menu['id'] }}">{{ $menu->menuType['name'] }}</span></td>
                                    <td class="text-right">
                                        <a type="button" href="{{route("admin.menu.edit", $menu['id'])}}" rel="tooltip"
                                           class="btn btn-success btn-icon btn-sm " data-original-title="" title="">
                                            <i class="now-ui-icons ui-2_settings-90"></i>
                                        </a>
                                        <form action="{{ route('admin.menu.destroy', $menu['id']) }}" method="post"
                                              style="display:inline-block;" class="delete-form">
                                            @csrf
                                            @method('delete')
                                            <button type="button" rel="tooltip"
                                                    class="btn btn-danger btn-icon btn-sm delete-button"
                                                    data-original-title="" title=""
                                                    onclick="confirm('Вы хотите удалить меню по ИД:{{'#'.$menu->id}}') ? this.parentElement.submit() : ''">
                                                <i class="now-ui-icons ui-1_simple-remove"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
@endsection

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".submit").click(function(){

                $type = $(this).attr("data-type");
                $id = $(this).attr('id');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "menus/update/menuType",
                    method: "POST",
                    data: { type:$type, id:$id },
                    success: function () {
                        function refresh(){
                            location = ''
                        }
                        refresh();
                    }
                })
            });
        });
    </script>
@endpush