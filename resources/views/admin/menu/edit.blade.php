@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'меню',
    'activePage' => 'menus',
    'activeNav' => '',
])

@section('title')
    <title>Редактировать меню</title>
@endsection

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0"><strong class="wi-page-title" id="active-menus-page">Редактирование меню - </strong>{{$item['title_am']}}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('admin.menus') }}" class="btn btn-primary btn-round">Обратно к списку</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-regular">
                            <ul class="nav nav-tabs " id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home"
                                       role="tab" aria-controls="home" aria-selected="true">Армянский</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#ru" role="tab"
                                       aria-selected="false">Русский</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#en" role="tab"
                                       aria-selected="false">Английский</a>
                                </li>
                            </ul>
                        <form method="post" action="{{ route('admin.menu.update', $item['id']) }}" autocomplete="off"
                            enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="pl-lg-4">
                                <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-title-am">Заголовок <span class="asterisk">*</span> </label>
                                            <input type="text" name="title_am" id="input-title-am" class="form-control{{ $errors->has('title_am') ? ' is-invalid' : '' }}" placeholder="Заголовок" value="{{ old('title_am', $item->title_am) }}">

                                            @include('alerts.feedback', ['field' => 'title_am'])
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="ru" role="tabpanel" aria-labelledby="ru-tab">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-title-ru">Заголовок</label>
                                            <input type="text" name="title_ru" id="input-title-ru" class="form-control{{ $errors->has('title_ru') ? ' is-invalid' : '' }}" placeholder="Заголовок" value="{{ old('title_ru', $item->title_ru) }}">

                                            @include('alerts.feedback', ['field' => 'title_ru'])
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-title-en">Заголовок</label>
                                            <input type="text" name="title_en" id="input-title-en" class="form-control{{ $errors->has('title_en') ? ' is-invalid' : '' }}" placeholder="Заголовок" value="{{ old('title_en', $item->title_en) }}">

                                            @include('alerts.feedback', ['field' => 'title_en'])
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-slug">Адрес <span class="asterisk">*</span> </label>
                                        <input type="text" name="slug" id="input-slug" class="form-control{{ $errors->has('slug') ? ' is-invalid' : '' }}" placeholder="Адрес" value="{{ old('slug', $item->slug) }}">

                                        @include('alerts.feedback', ['field' => 'slug'])
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label" for="input-order">Порядок <span class="asterisk">*</span> </label>
                                        <input type="text" name="order" id="input-order" class="form-control{{ $errors->has('order') ? ' is-invalid' : '' }}" placeholder="Порядок" value="{{ old('order', $item->order) }}">

                                        @include('alerts.feedback', ['field' => 'order'])
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label" for="input-role">Страница <span class="asterisk">*</span> </label>
                                        <select class="form-control{{ $errors->has('page_id') ? ' is-invalid' : '' }}" name="page_id">
                                            <option value="">Выберите страницу</option>
                                            @foreach($pages as $key => $page)
                                                <option value="{{ $page['id'] }}" {{ old('page_id', $item->page_id) == $page['id'] ? 'selected' : '' }}>{{ $page['title_am'] }}</option>
                                            @endforeach
                                        </select>

                                        @include('alerts.feedback', ['field' => 'page_id'])
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label" for="input-role">Родитель меню</label>
                                        <select class="form-control{{ $errors->has('parent_id') ? ' is-invalid' : '' }}" name="parent_id">
                                            <option value="">Выберите родительское меню</option>
                                            @foreach($menus as $key => $menu)
                                                <option value="{{ $menu['id'] }}" {{ old('parent_id', $item->parent_id) == $menu['id'] ? 'selected' : '' }} {{ $menu['parent_id'] == $item['id'] ? 'style=display:none': '' }}>{{ $menu['title_am'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">Сoхранить</button>
                                </div>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection