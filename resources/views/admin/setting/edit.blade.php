@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Настройки',
    'activePage' => 'settings',
    'activeNav' => '',
])

@section('title')
    <title>Настройки</title>
@endsection
@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Настройки</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('admin.settings') }}" class="btn btn-primary btn-round">Обратно к
                                    списку</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-regular">
                            <ul class="nav nav-tabs " id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#am"
                                       role="tab" aria-controls="home" aria-selected="true">Армянский</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#ru" role="tab"
                                       aria-selected="false">Русский</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#en" role="tab"
                                       aria-selected="false">Английский</a>
                                </li>
                            </ul>
                            <form method="post" action="{{ route('admin.setting.update', $setting['id']) }}">
                                @csrf
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="am" role="tabpanel"
                                         aria-labelledby="home-tab">
                                        <div class="form-group">
                                            <label class="form-control-label" for="value-am">Значение <span
                                                        class="asterisk">*</span> </label>
                                            <input type="text" name="value_am" id="value-am"
                                                   class="form-control{{ $errors->has('value_am') ? ' is-invalid' : '' }}"
                                                   placeholder="Заголовок" value="{{ old('value_am', $setting['value_am']) }}">

                                            @include('alerts.feedback', ['field' => 'value_am'])
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="ru" role="tabpanel" aria-labelledby="ru-tab">
                                        <div class="form-group">
                                            <label class="form-control-label" for="value-ru">Значение</label>
                                            <input type="text" name="value_ru" id="value-ru"
                                                   class="form-control{{ $errors->has('value_ru') ? ' is-invalid' : '' }}"
                                                   placeholder="Заголовок" value="{{ old('value_ru', $setting['value_ru']) }}">

                                            @include('alerts.feedback', ['field' => 'value_ru'])
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                                        <div class="form-group">
                                            <label class="form-control-label" for="value-en">Значение</label>
                                            <input type="text" name="value_en" id="value-en"
                                                   class="form-control{{ $errors->has('value_en') ? ' is-invalid' : '' }}"
                                                   placeholder="Заголовок" value="{{ old('value_en', $setting['value_en']) }}">

                                            @include('alerts.feedback', ['field' => 'value_en'])
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label" for="key">Ключ значение <span
                                                    class="asterisk">*</span> </label>
                                        <input type="text" name="key" id="key"
                                               class="form-control{{ $errors->has('key') ? ' is-invalid' : '' }}"
                                               placeholder="Ключ значение" value="{{ old('key', $setting['key']) }}" readonly>

                                        @include('alerts.feedback', ['field' => 'key'])
                                    </div>

                                    <br/><br/>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-success mt-4">Сoхранить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
