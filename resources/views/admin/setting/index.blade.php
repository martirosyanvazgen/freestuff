@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Настройки',
    'activePage' => 'settings',
    'activeNav' => '',
])

@section('title')
    <title>Настройки</title>
@endsection
@section('content')
    <div class="panel-header"></div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary btn-round text-white pull-right" href="{{ route('admin.setting.create') }}">Добавить</a>
                        <h4 class="card-title">Настройки</h4>
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar"></div>
                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Ключ значение</th>
                                <th>Значение</th>
                                <th class="disabled-sorting text-right">{{ __('Действия') }}</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Ключ значение</th>
                                <th>Значение</th>
                                <th class="disabled-sorting text-right">{{ __('Действия') }}</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($settings as $key =>$item)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$item->key}}</td>
                                    <td>{{$item->value_ru}}</td>
                                    <td class="text-right">
                                        <a type="button" href="{{route("admin.setting.edit", $item['id'])}}" rel="tooltip"
                                           class="btn btn-success btn-icon btn-sm " data-original-title="" title="">
                                            <i class="now-ui-icons ui-2_settings-90"></i>
                                        </a>
                                        <form action="{{ route('admin.setting.delete', $item['id']) }}" method="post"
                                              style="display:inline-block;" class="delete-form">
                                            @csrf
                                            @method('delete')
                                            <button type="button" rel="tooltip"
                                                    class="btn btn-danger btn-icon btn-sm delete-button"
                                                    data-original-title="" title=""
                                                    onclick="confirm('Вы хотите удалить настройка по ИД:{{'#'.$item->id}}') ? this.parentElement.submit() : ''">
                                                <i class="now-ui-icons ui-1_simple-remove"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

