@extends('layouts.app', [
    'namePage' => 'Метки',
    'class' => 'sidebar-mini',
    'activePage' => 'labels',
    'activeNav' => '',
])

@section('title')
    <title>Метки</title>
@endsection
@section('content')
    <div class="panel-header">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary btn-round text-white pull-right"
                           href="{{ route('admin.label.create') }}">Добавить</a>
                        <h4 class="card-title">Метки</h4>
                        <div class="col-12 mt-2">
                            @include('alerts.success', ['key' => 'success'])
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Заголовок</th>
                                <th>Ключ значение</th>
                                <th>Значок</th>
                                <th class="disabled-sorting text-right">Действия</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Заголовок</th>
                                <th>Ключ значение</th>
                                <th>Значок</th>
                                <th class="disabled-sorting text-right">Действия</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($labels as $key => $label)
                                <tr>
                                    <td>
                                        {{ ($labels->firstItem() + $key).'.' }}
                                    </td>
                                    <td>{{$label['title_am']}}</td>
                                    <td>{{$label['key']}}</td>
                                    <td>
                                    @if($label['icon'] != null)
                                            <img src="{{ asset( '/storage/'.$label['icon'] . '_small.' . $label['ext']) }}"
                                                 width="50" height="50">
                                    @else
                                        нет значка
                                    @endif
                                    </td>
                                    <td class="text-right">
                                        <a type="button" href="{{route("admin.label.edit", $label['id'])}}" rel="tooltip"
                                           class="btn btn-success btn-icon btn-sm " data-original-title="" title="">
                                            <i class="now-ui-icons ui-2_settings-90"></i>
                                        </a>
                                        <form action="{{ route('admin.label.delete', $label['id']) }}" method="post"
                                              style="display:inline-block;" class="delete-form">
                                            @csrf
                                            @method('delete')
                                            <button type="button" rel="tooltip"
                                                    class="btn btn-danger btn-icon btn-sm delete-button"
                                                    data-original-title="" title=""
                                                    onclick="confirm('Вы хотите удалить метка по ИД:{{'#'.$label->id}}') ? this.parentElement.submit() : ''">
                                                <i class="now-ui-icons ui-1_simple-remove"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
@endsection