@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Обновить данные пользователя',
    'activePage' => 'user',
    'activeNav' => '',
])

@section('title')
    <title>Обновить данные пользователя</title>
@endsection

@section('css')
    <style>

        .box {
            margin: 15px;
            background-color: white;
            border-radius: 5px;
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.24), 0 1px 2px rgba(0, 0, 0, 0.24);
            transition: all 0.3s cubic-bezier(.25, .8, .25, 1);
            overflow: hidden;
        }

        .upload-options {
            position: relative;
            height: 75px;
            cursor: pointer;
            overflow: hidden;
            text-align: center;
            transition: background-color ease-in-out 150ms;
        }


        .js--image-preview {
            height: 180px;
            width: 100%;
            position: relative;
            overflow: hidden;
            background-image: url('');
            background-color: white;
            background-position: center center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        @keyframes ripple {
            100% {
                opacity: 0;
                transform: scale(2.5);
            }
        }
    </style>
@endsection

@section('content')
    <div class="panel-header">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Обновить данные пользователя</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('admin.users') }}" class="btn btn-primary btn-round">Обратно к
                                    списку</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('admin.user.update', $user) }}" autocomplete="off"
                              enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="pl-lg-4">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-name">Имя</label> <span
                                            class="asterisk">*</span>
                                    <input type="text" name="name" id="input-name"
                                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           placeholder="Имя" value="{{ old('name', $user->name) }}">

                                    @include('alerts.feedback', ['field' => 'name'])
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label" for="input-surname">Фамилия</label> <span
                                            class="asterisk">*</span>
                                    <input type="text" name="surname" id="input-surname"
                                           class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}"
                                           placeholder="Фамилия" value="{{ old('surname', $user->surname) }}">

                                    @include('alerts.feedback', ['field' => 'surname'])
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label" for="input-login">Логин</label> <span
                                            class="asterisk">*</span>
                                    <input type="text" name="login" id="input-login"
                                           class="form-control{{ $errors->has('login') ? ' is-invalid' : '' }}"
                                           placeholder="Логин" value="{{ old('login', $user->login) }}">

                                    @include('alerts.feedback', ['field' => 'login'])
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label" for="input-email">Эл. адрес</label> <span
                                            class="asterisk">*</span>
                                    <input type="email" name="email" id="input-email"
                                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           placeholder="Эл. адрес" value="{{ old('email', $user->email) }}">

                                    @include('alerts.feedback', ['field' => 'email'])
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label" for="region_id">Область <span
                                                class="asterisk">*</span> </label>
                                    <select class="form-control{{ $errors->has('region_id') ? ' is-invalid' : '' }}"
                                            name="region_id" id="region_id">
                                        @foreach($regions as $key => $region)
                                            <option value="{{ $region['id'] }}" {{ old('region_id', $user['region_id']) == $region['id'] ? 'selected' : '' }}>{{ $region['name_ru'] }}</option>
                                        @endforeach
                                    </select>

                                    @include('alerts.feedback', ['field' => 'region_id'])
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label" for="city_id">Город <span
                                                class="asterisk">*</span> </label>
                                    <select class="form-control{{ $errors->has('city_id') ? ' is-invalid' : '' }}"
                                            name="city_id" id="city_id">
                                        @foreach($cities as $key => $city)
                                            <option value="{{ $city['id'] }}"
                                                    {{ old('city_id') == $city['id'] ? 'selected' : '' }} region-id="{{ $city['region_id'] }}">{{ $city['name_ru'] }}</option>
                                        @endforeach
                                    </select>

                                    @include('alerts.feedback', ['field' => 'city_id'])
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label" for="address">Адрес</label>
                                    <input type="text" name="address" id="address"
                                           class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}"
                                           placeholder="Адрес" value="{{ old('address', $user['address']) }}">

                                    @include('alerts.feedback', ['field' => 'address'])
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label" for="phone_number">Телефонный номер</label> <span
                                            class="asterisk">*</span>
                                    <input type="text" name="phone_number" id="phone_number"
                                           class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}"
                                           placeholder="Телефонный номер"
                                           value="{{ old('phone_number', $user['phone_number']) }}">

                                    @include('alerts.feedback', ['field' => 'phone_number'])
                                </div>
                                <div class="row">
                                    <div class="box col-md-2" style="padding: 0 !important;margin-right: 15px">
                                        <div class="js--image-preview"
                                             @if($user->image)
                                             style="background-image: url({{ asset('/storage/'.$user->image) }})"
                                                @endif
                                        >
                                            @if($user->image)
                                                <div class="text-right">
                                                    <a href="{{ route('admin.user.delete.profile.image',$user->id) }}">
                                                        <button type="button" class="btn btn-danger remove"
                                                                style="margin: 0 !important;"><i
                                                                    class="now-ui-icons ui-1_simple-remove"></i>
                                                        </button>
                                                    </a>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="upload-options">
                                            <label for="banner" class="col-form-label btn btn-primary"
                                                   style="color: #fff;">
                                                Изображение профиля</label><span
                                                    id="icon-selected" style="display: none">Изображение профиля</span>
                                            <input type="file" name="image" id="banner" class="image-upload"
                                                   accept="image/*" style="display:none"><br/>
                                        </div>
                                    </div>
                                </div>
                                <input type="checkbox" id="notification" name="notification"
                                       value=""
                                       @if($user->notification_on == '1') checked @endif
                                > Уведомление<br>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">Сoхранить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function () {
            if ($("#region_id").data('options') === undefined) {
                $("#region_id").data('options', $('#city_id option').clone());
            }

            var id = $("#region_id").val();
            console.log(id);
            var options = $("#region_id").data('options').filter('[region-id=' + id + ']');
            $('#city_id').html(options);
        });

        $("#region_id").change(function () {
            if ($(this).data('options') === undefined) {
                $(this).data('options', $('#city_id option').clone());
            }
            var id = $(this).val();
            console.log(id);
            var options = $(this).data('options').filter('[region-id=' + id + ']');
            $('#city_id').html(options);
        });

        function initImageUpload(box) {
            let uploadField = box.querySelector('.image-upload');

            uploadField.addEventListener('change', getFile);

            function getFile(e) {
                let file = e.currentTarget.files[0];
                checkType(file);
            }

            function previewImage(file) {
                let thumb = box.querySelector('.js--image-preview'),
                    reader = new FileReader();

                reader.onload = function () {
                    thumb.style.backgroundImage = 'url(' + reader.result + ')';
                }
                reader.readAsDataURL(file);
                thumb.className += ' js--no-default';
            }

            function checkType(file) {
                let imageType = /image.*/;
                if (!file.type.match(imageType)) {
                    throw 'Datei ist kein Bild';
                } else if (!file) {
                    throw 'Kein Bild gewählt';
                } else {
                    previewImage(file);
                }
            }

        }

        // initialize box-scope
        var boxes = document.querySelectorAll('.box');

        for (let i = 0; i < boxes.length; i++) {
            let box = boxes[i];
            initDropEffect(box);
            initImageUpload(box);
        }


        /// drop-effect
        function initDropEffect(box) {
            let area, drop, areaWidth, areaHeight, maxDistance, dropWidth, dropHeight, x, y;

            // get clickable area for drop effect
            area = box.querySelector('.js--image-preview');
            area.addEventListener('click', fireRipple);

            function fireRipple(e) {
                area = e.currentTarget
                // create drop
                if (!drop) {
                    drop = document.createElement('span');
                    drop.className = 'drop';
                    this.appendChild(drop);
                }
                // reset animate class
                drop.className = 'drop';

                // calculate dimensions of area (longest side)
                areaWidth = getComputedStyle(this, null).getPropertyValue("width");
                areaHeight = getComputedStyle(this, null).getPropertyValue("height");
                maxDistance = Math.max(parseInt(areaWidth, 10), parseInt(areaHeight, 10));

                // set drop dimensions to fill area
                drop.style.width = maxDistance + 'px';
                drop.style.height = maxDistance + 'px';

                // calculate dimensions of drop
                dropWidth = getComputedStyle(this, null).getPropertyValue("width");
                dropHeight = getComputedStyle(this, null).getPropertyValue("height");

                // calculate relative coordinates of click
                // logic: click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center
                x = e.pageX - this.offsetLeft - (parseInt(dropWidth, 10) / 2);
                y = e.pageY - this.offsetTop - (parseInt(dropHeight, 10) / 2) - 30;

                // position drop and animate
                drop.style.top = y + 'px';
                drop.style.left = x + 'px';
                drop.className += ' animate';
                e.stopPropagation();

            }
        }

    </script>
@endpush