@extends('layouts.app', [
    'namePage' => 'Пользователи',
    'class' => 'sidebar-mini',
    'activePage' => 'users',
    'activeNav' => '',
])

@section('title')
    <title>Пользователи</title>
@endsection

@section('content')
    <div class="panel-header">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary btn-round text-white pull-right"
                           href="{{ route('admin.user.create') }}">Добавить пользователя</a>
                        <h4 class="card-title">Пользователи</h4>
                        <div class="col-12 mt-2">
                            @include('alerts.success', ['key' => 'success'])
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Имя</th>
                                <th>Фамилия</th>
                                <th>Эл.адрес</th>
                                <th>Статус</th>
                                <th>Дата создания</th>
                                <th class="disabled-sorting text-right">Действия</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Имя</th>
                                <th>Фамилия</th>
                                <th>Эл.адрес</th>
                                <th>Статус</th>
                                <th>Дата создания</th>
                                <th class="disabled-sorting text-right">Действия</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>
                                        <span class="avatar avatar-sm rounded-circle">
                                            <img src="@if($user->image){{asset('storage/'. $user->image)}}@else{{asset('img/default-avatar.png')}}@endif" alt=""
                                                 style="max-width: 80px; border-radiu: 100px">

                                        </span>
                                    </td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->surname}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>@if($user->active == true)
                                            Активный
                                        @else
                                            Неактивный
                                        @endif
                                    </td>
                                    <td>{{ $user->created_at->format('d/m/Y H:i') }}</td>
                                    <td class="text-right">
                                        @if($user->id!=auth()->user()->id)
                                            <a type="button" href="{{route("admin.user.edit",$user)}}" rel="tooltip"
                                               class="btn btn-success btn-icon btn-sm " data-original-title="" title="">
                                                <i class="now-ui-icons ui-2_settings-90"></i>
                                            </a>
                                            <form action="{{ route('admin.user.destroy', $user) }}" method="post"
                                                  style="display:inline-block;" class="delete-form">
                                                @csrf
                                                @method('delete')
                                                <button type="button" rel="tooltip"
                                                        class="btn btn-danger btn-icon btn-sm delete-button"
                                                        data-original-title="" title=""
                                                        onclick="confirm('Вы хотите удалить ползователь по ИД:{{'#'.$user->id}}') ? this.parentElement.submit() : ''">
                                                    <i class="now-ui-icons ui-1_simple-remove"></i>
                                                </button>
                                            </form>
                                        @else
                                            <a type="button" href="{{ route('admin.profile') }}" rel="tooltip"
                                               class="btn btn-success btn-icon btn-sm " data-original-title="" title="">
                                                <i class="now-ui-icons ui-2_settings-90"></i>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- end content-->
                </div>

                <div class="card-footer py-4">
                    <nav aria-label="...">
                        <ul class="pagination justify-content-end mb-0">
                            {{ $users->links() }}
                        </ul>
                    </nav>
                </div>
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
@endsection
