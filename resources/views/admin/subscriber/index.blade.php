@extends('layouts.app', [
    'namePage' => 'Подписчики',
    'class' => 'sidebar-mini',
    'activePage' => 'subscribers',
    'activeNav' => '',
])
@section('title')
    <title>Подписчики</title>
@endsection
@section('content')
    <div class="panel-header"></div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ __('Подписчики') }}</h4>
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar"></div>
                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Эл.адрес</th>
                                <th>Статус</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>{{ __('№') }}</th>
                                <th>{{ __('Эл.адрес') }}</th>
                                <th>{{ __('Статус') }}</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($subscribers as $key => $subscriber)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$subscriber->email}}</td>
                                        <td>
                                            @if($subscriber->active == 1)
                                                <span class="table-active"><button type="submit" class="btn btn-success  btn-sm my-0 ">Активный</button></span>
                                            @else
                                                <span class="table-active"><button type="submit" class="btn btn-danger  btn-sm my-0">Неактивный</button></span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
