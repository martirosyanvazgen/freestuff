@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Профиль',
    'activePage' => 'profile',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header
{{--    panel-header-sm--}}
    ">

    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h5 class="title">Редактировать профиль</h5>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('admin.profile.update') }}"
                              autocomplete="off"
                              enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            @include('alerts.success', ['key' => 'profile_update'])
                            <div class="row">
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label>Логин</label>
                                        <input type="text" name="login" class="form-control" placeholder="Логин"
                                               value="{{ old('login', auth('web')->user()->login) }}">
                                        @include('alerts.feedback', ['field' => 'login'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label>Имя</label>
                                        <input type="text" name="name" class="form-control" placeholder="Имя"
                                               value="{{ old('name', auth('web')->user()->name) }}">
                                        @include('alerts.feedback', ['field' => 'name'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label>Фамилия</label>
                                        <input type="text" name="surname" class="form-control" placeholder="Фамилия"
                                               value="{{ old('surname', auth('web')->user()->surname) }}">
                                        @include('alerts.feedback', ['field' => 'surname'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Эл. адрес</label>
                                        <input type="email" name="email" class="form-control" placeholder="Эл. адрес"
                                               value="{{ old('email', auth('web')->user()->email) }}">
                                        @include('alerts.feedback', ['field' => 'email'])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-primary btn-round">Сoхранить</button>
                            </div>
                            <hr class="half-rule"/>
                        </form>
                    </div>
                    <div class="card-header">
                        <h5 class="title">Пароль</h5>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('admin.profile.password') }}"
                              autocomplete="off">
                            @csrf
                            @method('put')
                            @include('alerts.success', ['key' => 'password_status'])
                            @if (\Session::has('password_status_false'))
                                <div class="alert alert-danger alert-dismissible fade show" style="width: 100%">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {!! \Session::get('password_status_false') !!}
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group {{ $errors->has('password') ? ' has-danger' : '' }}">
                                        <label>Действующий пароль</label>
                                        <input class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                               name="old_password" placeholder="Действующий пароль"
                                               type="password">
                                        @include('alerts.feedback', ['field' => 'old_password'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group {{ $errors->has('password') ? ' has-danger' : '' }}">
                                        <label>Новый пароль</label>
                                        <input class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                               placeholder="Новый пароль" type="password" name="password">
                                        @include('alerts.feedback', ['field' => 'password'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group {{ $errors->has('password') ? ' has-danger' : '' }}">
                                        <label>Подтвердите новый пароль</label>
                                        <input class="form-control" placeholder="Подтвердите новый пароль"
                                               type="password" name="password_confirmation">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit"
                                        class="btn btn-primary btn-round ">Изменить пароль</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-user">
                    <div class="image">
                        <img src="{{asset('/img/bg5.jpg')}}" alt="...">
                    </div>
                    <div class="card-body">
                        <div class="author">
                            <a href="#">
                                <img class="avatar border-gray" src="{{asset('/img/default-avatar.png')}}" alt="...">
                                <h5 class="title">{{ auth('web')->user()->name }}</h5>
                            </a>
                            <p class="description">
                                {{ auth('web')->user()->email }}
                            </p>
                        </div>
                    </div>
                    <hr>
                    <div class="button-container">
                        <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">
                            <i class="fab fa-facebook-square"></i>
                        </button>
                        <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">
                            <i class="fab fa-twitter"></i>
                        </button>
                        <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">
                            <i class="fab fa-google-plus-square"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection