@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Категория - Фильтры',
    'activePage' => 'categoryFilterGroups',
    'activeNav' => 'categoryFilterGroups',
])

@section('title')
    <title>Категория - Фильтры</title>
@endsection

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Категория - Фильтры</h3>
                                <h6 class="title" style="color: #9A9A9A; margin-top: 20px" >{{ $category->name_ru }}</h6>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('admin.filters.categoryFilterGroups') }}" class="btn btn-primary btn-round">Обратно к списку</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-regular">
                            <form method="POST" action="{{ route('admin.filters.categoryFilterGroups.update',$category->id) }}">
                                @csrf
                                @method('PUT')
                                <div class="tab-content" id="myTabContent">
                                    @if($filterGroups->count())
                                        @foreach($filterGroups as $filterGroup)
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="filterGroups[]" type="checkbox" value="{{ $filterGroup->id }}"
                                                               @if ($category->filterGroups->contains('id', $filterGroup->id)) checked @endif>
                                                        {{ $filterGroup['name_ru'] }}
                                                        <span class="form-check-sign">
                                                            <span class="check">
                                                                <a href="{{ route('admin.filters.filterGroups.show',$filterGroup->id) }}" target="_blank">
                                                                    <span style="margin-left: 10px;color: #9A9A9A">
                                                                        <i class="fas fa-external-link-alt"></i>
                                                                    </span>
                                                                </a>
                                                            </span>
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                        @endforeach

                                        @else
                                        <h4>Нет данных</h4>
                                    @endif

                                    @foreach ($errors->all() as $error)

                                        <span class="has-error">
                                            <small style="color: #fb6340">{{ $error }}</small><br>
                                        </span>

                                    @endforeach
                                    <br><br>

                                    <div class="text-left">
                                        <button type="submit" class="btn btn-primary mt-4">Сoхранить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection