@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Категория - Фильтры',
    'activePage' => 'categoryFilterGroups',
    'activeNav' => 'categoryFilterGroups',
])

@section('title')
    <title>Категория - Фильтры</title>
@endsection
@section('content')
    <div class="panel-header">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Категория - Фильтры</h4>
                            @include('alerts.success', ['key' => 'success'])
                            @include('alerts.errors')
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                                <th>Адрес (URL)</th>
                                <th>Картинка</th>
                                <th class="disabled-sorting text-right">Действия</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                                <th>Адрес (URL)</th>
                                <th>Картинка</th>
                                <th class="disabled-sorting text-right">Действия</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($categories as $key =>$item)
                                <tr>
                                    <td>
                                        {{ ($categories->firstItem() + $key).'.' }}
                                    </td>
                                    <td>{{$item->name_am}}</td>
                                    <td>{{$item->slug}}</td>
                                    <td>
                                        @if($item->cover)
                                            <img src="{{ asset( '/storage/'.$item['cover'] . '_small.' . $item['ext']) }}"
                                                 width="30" height="30">
                                        @else
                                            Нет картинки
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        <a type="button" href="{{route('admin.filters.categoryFilterGroups.edit',$item)}}" rel="tooltip"
                                           class="btn btn-success btn-icon btn-sm " data-original-title="" title="">
                                            <i class="now-ui-icons ui-2_settings-90"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
@endsection

@push('js')



    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".submit").click(function(){

                $type = $(this).attr("data-type");
                $id = $(this).attr('id');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "menus/update/menuType",
                    method: "POST",
                    data: { type:$type, id:$id },
                    success: function () {
                        function refresh(){
                            location = ''
                        }
                        refresh();
                    }
                })
            });
        });
    </script>



    <script>
        $(document).ready(function () {
            $(".delete-button").click(function () {
                var clickedButton = $(this);
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    confirmButtonText: 'Yes, delete it!',
                    buttonsStyling: false
                }).then((result) => {
                    if (result.value) {
                        clickedButton.parents(".delete-form").submit();
                    }
                })

            })
            $('#datatable').DataTable({
                "pagingType": "full_numbers",
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search records",
                }

            });

            var table = $('#datatable').DataTable();

            // Edit record
            table.on('click', '.edit', function () {
                $tr = $(this).closest('tr');
                if ($($tr).hasClass('child')) {
                    $tr = $tr.prev('.parent');
                }

                var data = table.row($tr).data();
                alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
            });

            // Delete a record
            table.on('click', '.remove', function (e) {
                $tr = $(this).closest('tr');
                if ($($tr).hasClass('child')) {
                    $tr = $tr.prev('.parent');
                }
                table.row($tr).remove().draw();
                e.preventDefault();
            });

            //Like record
            table.on('click', '.like', function () {
                alert('You clicked on Like button');
            });
        });
    </script>
@endpush