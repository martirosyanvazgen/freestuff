@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Блог',
    'activePage' => 'blog',
    'activeNav' => 'blog',
])

@section('title')
    <title>Блог</title>
@endsection

@section('css')
<style>

    .box {
        margin: 15px;
        background-color: white;
        border-radius: 5px;
        box-shadow: 0 1px 3px rgba(0,0,0,0.24), 0 1px 2px rgba(0,0,0,0.24);
        transition: all 0.3s cubic-bezier(.25,.8,.25,1);
        overflow: hidden;
    }

    .upload-options {
        position: relative;
        height: 75px;
        cursor: pointer;
        overflow: hidden;
        text-align: center;
        transition: background-color ease-in-out 150ms;
    }


    .js--image-preview {
        height: 180px;
        width: 100%;
        position: relative;
        overflow: hidden;
        background-image: url('');
        background-color: white;
        background-position: center center;
        background-repeat: no-repeat;
        background-size: cover;
    }

    @keyframes ripple {
        100% {opacity: 0; transform: scale(2.5);}
    }
</style>
@endsection

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Блог</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('admin.blog') }}" class="btn btn-primary btn-round">Обратно к списку</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-regular">
                            <ul class="nav nav-tabs " id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home"
                                       role="tab" aria-controls="home" aria-selected="true">Армянский</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#ru" role="tab"
                                       aria-selected="false">Русский</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#en" role="tab"
                                       aria-selected="false">Английский</a>
                                </li>
                            </ul>
                            <form method="post" action="{{ route('admin.blog.store') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-title-am">Загoловок <span class="asterisk">*</span> </label>
                                            <input type="text" name="title_am" id="input-title-am" class="form-control{{ $errors->has('title_am') ? ' is-invalid' : '' }}" placeholder="Загoловок" value="{{ old('title_am') }}">

                                            @include('alerts.feedback', ['field' => 'title_am'])
                                        </div>
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-description-am">Краткое описание <span class="asterisk">*</span> </label>
                                            <input type="text" name="description_am" id="input-description-am" class="form-control{{ $errors->has('description_am') ? ' is-invalid' : '' }}"
                                                   placeholder="Краткое описание" value="{{ old('description_am') }}">

                                            @include('alerts.feedback', ['field' => 'description_am'])
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="text_am">Текст <span
                                                        class="asterisk">*</span></label>
                                            <textarea name="text_am" id="text_am" class="form-control{{ $errors->has('text_am') ? ' is-invalid' : '' }}" placeholder="Текст">
                                            {{old('text_am')}}
                                        </textarea>
                                            @include('alerts.feedback', ['field' => 'text_am'])
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="ru" role="tabpanel" aria-labelledby="ru-tab">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-title-ru">Загoловок <span class="asterisk">*</span></label>
                                            <input type="text" name="title_ru" id="input-title-ru" class="form-control{{ $errors->has('title_ru') ? ' is-invalid' : '' }}" placeholder="Загoловок" value="{{ old('title_ru') }}">

                                            @include('alerts.feedback', ['field' => 'title_ru'])
                                        </div>
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-description-ru">Краткое описание <span class="asterisk">*</span> </label>
                                            <input type="text" name="description_ru" id="input-description-ru" class="form-control{{ $errors->has('description_ru') ? ' is-invalid' : '' }}"
                                                   placeholder="Краткое описание" value="{{ old('description_ru') }}">

                                            @include('alerts.feedback', ['field' => 'description_ru'])
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="text_ru">Текст </label>
                                            <textarea name="text_ru" id="text_ru" class="form-control{{ $errors->has('text_ru') ? ' is-invalid' : '' }}" placeholder="Текст">
                                            {{old('text_ru')}}
                                        </textarea>
                                            @include('alerts.feedback', ['field' => 'text_ru'])
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-title-en">Загoловок <span class="asterisk">*</span></label>
                                            <input type="text" name="title_en" id="input-title-en" class="form-control{{ $errors->has('title_en') ? ' is-invalid' : '' }}" placeholder="Загoловок" value="{{ old('title_en') }}">

                                            @include('alerts.feedback', ['field' => 'title_en'])
                                        </div>
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-description-en">Краткое описание <span class="asterisk">*</span> </label>
                                            <input type="text" name="description_en" id="input-description-en" class="form-control{{ $errors->has('description_en') ? ' is-invalid' : '' }}"
                                                   placeholder="Краткое описание" value="{{ old('description_en') }}">

                                            @include('alerts.feedback', ['field' => 'description_en'])
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for = 'text_en'>Текст </label>
                                            <textarea name="text_en" id="text_en" class="form-control{{ $errors->has('text_en') ? ' is-invalid' : '' }}" placeholder="Текст">
                                            {{old('text_en')}}
                                        </textarea>
                                            @include('alerts.feedback', ['field' => 'text_en'])
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="box col-md-2" style="padding: 0 !important;margin-right: 15px">
                                            <div class="js--image-preview">
                                            </div>
                                            <div class="upload-options">
                                                <label for="banner" class="col-form-label btn btn-primary" style="color: #fff;">
                                                Баннер</label><span
                                                id="icon-selected" style="display: none">Баннер</span>
                                                <input type="file" name="banner" id="banner" class="image-upload" accept="image/*" style="display:none"><br/>

                                            </div>
                                        </div>

                                        <div class="box col-md-2" style="padding: 0 !important;margin-right: 15px">
                                            <div class="js--image-preview">
                                            </div>
                                            <div class="upload-options">
                                                <label for="cover" class="col-form-label btn btn-primary" style="color: #fff;">
                                                    Картинка</label><span
                                                        id="icon-selected" style="display: none">Картинка</span>
                                                <input type="file" name="cover" id="cover" class="image-upload" accept="image/*" style="display:none"><br/>

                                            </div>
                                        </div>
                                    </div>

                                    @foreach ($errors->all() as $error)

                                        <span class="has-error">
                                            <small style="color: #fb6340">{{ $error }}</small><br>
                                        </span>

                                    @endforeach
                                    <br/><br/>
                                    <div class="text-left">
                                        <button type="submit" class="btn btn-primary mt-4">Дoбавить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.config.autoParagraph = false;
        CKEDITOR.replace('text_am', {
            filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace('text_ru', {
            filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace('text_en', {
            filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
    </script>
    <script>
        function initImageUpload(box) {
            let uploadField = box.querySelector('.image-upload');

            uploadField.addEventListener('change', getFile);

            function getFile(e){
                let file = e.currentTarget.files[0];
                checkType(file);
            }

            function previewImage(file){
                let thumb = box.querySelector('.js--image-preview'),
                    reader = new FileReader();

                reader.onload = function() {
                    thumb.style.backgroundImage = 'url(' + reader.result + ')';
                }
                reader.readAsDataURL(file);
                thumb.className += ' js--no-default';
            }

            function checkType(file){
                let imageType = /image.*/;
                if (!file.type.match(imageType)) {
                    throw 'Datei ist kein Bild';
                } else if (!file){
                    throw 'Kein Bild gewählt';
                } else {
                    previewImage(file);
                }
            }

        }

        // initialize box-scope
        var boxes = document.querySelectorAll('.box');

        for (let i = 0; i < boxes.length; i++) {
            let box = boxes[i];
            initDropEffect(box);
            initImageUpload(box);
        }



        /// drop-effect
        function initDropEffect(box){
            let area, drop, areaWidth, areaHeight, maxDistance, dropWidth, dropHeight, x, y;

            // get clickable area for drop effect
            area = box.querySelector('.js--image-preview');
            area.addEventListener('click', fireRipple);

            function fireRipple(e){
                area = e.currentTarget
                // create drop
                if(!drop){
                    drop = document.createElement('span');
                    drop.className = 'drop';
                    this.appendChild(drop);
                }
                // reset animate class
                drop.className = 'drop';

                // calculate dimensions of area (longest side)
                areaWidth = getComputedStyle(this, null).getPropertyValue("width");
                areaHeight = getComputedStyle(this, null).getPropertyValue("height");
                maxDistance = Math.max(parseInt(areaWidth, 10), parseInt(areaHeight, 10));

                // set drop dimensions to fill area
                drop.style.width = maxDistance + 'px';
                drop.style.height = maxDistance + 'px';

                // calculate dimensions of drop
                dropWidth = getComputedStyle(this, null).getPropertyValue("width");
                dropHeight = getComputedStyle(this, null).getPropertyValue("height");

                // calculate relative coordinates of click
                // logic: click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center
                x = e.pageX - this.offsetLeft - (parseInt(dropWidth, 10)/2);
                y = e.pageY - this.offsetTop - (parseInt(dropHeight, 10)/2) - 30;

                // position drop and animate
                drop.style.top = y + 'px';
                drop.style.left = x + 'px';
                drop.className += ' animate';
                e.stopPropagation();

            }
        }

    </script>
@endpush