@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Категории',
    'activePage' => 'categories',
    'activeNav' => '',
])

@section('title')
    <title>Редактировать категорию</title>
@endsection

@section('css')

    <style>
        .fa-trash:hover {
            cursor: pointer;
        }
    </style>
    <style>

        .box {
            margin: 15px;
            background-color: white;
            border-radius: 5px;
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.24), 0 1px 2px rgba(0, 0, 0, 0.24);
            transition: all 0.3s cubic-bezier(.25, .8, .25, 1);
            overflow: hidden;
        }

        .upload-options {
            position: relative;
            height: 75px;
            cursor: pointer;
            overflow: hidden;
            text-align: center;
            transition: background-color ease-in-out 150ms;
        }


        .js--image-preview {
            height: 180px;
            width: 100%;
            position: relative;
            overflow: hidden;
            background-image: url('');
            background-color: white;
            background-position: center center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        @keyframes ripple {
            100% {
                opacity: 0;
                transform: scale(2.5);
            }
        }
    </style>

@endsection

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0"><strong class="wi-page-title" id="active-menus-page">Редактирование
                                        категорию - </strong>{{$item['name_am']}}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('admin.categories') }}" class="btn btn-primary btn-round">Обратно к
                                    списку</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-regular">
                            <ul class="nav nav-tabs " id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home"
                                       role="tab" aria-controls="home" aria-selected="true">Армянский</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#ru" role="tab"
                                       aria-selected="false">Русский</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#en" role="tab"
                                       aria-selected="false">Английский</a>
                                </li>
                            </ul>
                            <form method="post" action="{{ route('admin.category.update', $item['id']) }}"
                                  autocomplete="off"
                                  enctype="multipart/form-data">
                                @csrf
                                @method('put')
                                <div class="pl-lg-4">
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="home" role="tabpanel"
                                             aria-labelledby="home-tab">
                                            <div class="form-group">
                                                <label class="form-control-label" for="input-name-am">Название <span
                                                            class="asterisk">*</span> </label>
                                                <input type="text" name="name_am" id="input-name-am"
                                                       class="form-control{{ $errors->has('name_am') ? ' is-invalid' : '' }}"
                                                       placeholder="Название"
                                                       value="{{ old('title_am', $item['name_am']) }}">

                                                @include('alerts.feedback', ['field' => 'name_am'])
                                            </div>
                                        </div>

                                        <div class="tab-pane fade" id="ru" role="tabpanel" aria-labelledby="ru-tab">
                                            <div class="form-group">
                                                <label class="form-control-label" for="input-name-ru">Название</label>
                                                <input type="text" name="name_ru" id="input-name-ru"
                                                       class="form-control{{ $errors->has('name_ru') ? ' is-invalid' : '' }}"
                                                       placeholder="Название"
                                                       value="{{ old('name_ru', $item['name_ru']) }}">

                                                @include('alerts.feedback', ['field' => 'name_ru'])
                                            </div>
                                        </div>

                                        <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                                            <div class="form-group">
                                                <label class="form-control-label" for="input-name-en">Название <span
                                                            class="asterisk">*</span></label>
                                                <input type="text" name="name_en" id="input-name-en"
                                                       class="form-control{{ $errors->has('name_en') ? ' is-invalid' : '' }}"
                                                       placeholder="Название"
                                                       value="{{ old('name_en', $item['name_en']) }}">

                                                @include('alerts.feedback', ['field' => 'name_en'])
                                                @if($errors->has('name_en'))
                                                    <input type="hidden" class="error_name_en" value="true">
                                                @else
                                                    <input type="hidden" class="error_name_en" value="false">
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-control-label" for="input-password">Адрес <span
                                                        class="asterisk">*</span> </label>
                                            <input type="text" name="slug" id="input-slug"
                                                   class="form-control{{ $errors->has('slug') ? ' is-invalid' : '' }}"
                                                   placeholder="Адрес" value="{{ old('slug', $item['slug']) }}">

                                            @include('alerts.feedback', ['field' => 'slug'])
                                        </div>

                                        <div class="form-group">
                                            <label class="form-control-label" for="input-role">Родительская
                                                категория</label>
                                            <select class="form-control{{ $errors->has('parent_id') ? ' is-invalid' : '' }}"
                                                    name="parent_id">
                                                <option value="">Выберите родительскую категорию</option>
                                                @foreach($categories as $key => $category)
                                                    <option value="{{ $category['id'] }}" {{ old('parent_id', $item->parent_id) == $category['id'] ? 'selected' : '' }} {{ $category['parent_id'] == $item['id'] ? 'style=display:none': '' }}>{{ $category['name_am'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="row">
                                            <div class="box col-md-2" style="padding: 0 !important;margin-right: 15px">
                                                <div class="js--image-preview"
                                                     @if($item->icon)
                                                     style="background-image: url({{ asset('/storage/'.$item->icon) }})"
                                                        @endif
                                                >
                                                </div>
                                                <div class="upload-options">
                                                    <label for="banner" class="col-form-label btn btn-primary"
                                                           style="color: #fff;">
                                                        Значок  <span
                                                                class="asterisk">*</span></label><span
                                                            id="icon-selected" style="display: none">Значок</span>
                                                    <input type="file" name="icon" id="banner" class="image-upload"
                                                           accept="image/*" style="display:none"><br/>
                                                </div>
                                            </div>

                                            <div class="box col-md-2" style="padding: 0 !important;margin-right: 15px">
                                                <div class="js--image-preview"
                                                     @if($item->cover)
                                                     style="background-image: url({{ asset('/storage/'.$item->cover.'_medium.'.$item->ext) }})"
                                                        @endif
                                                >
                                                    @if($item->cover)
                                                        <div class="text-right">
                                                            <a href="{{ route('admin.category.delete.image', $item->id) }}">
                                                                <button type="button" class="btn btn-danger remove" style="margin: 0 !important;"><i class="now-ui-icons ui-1_simple-remove"></i></button>
                                                            </a>
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="upload-options">
                                                    <label for="cover" class="col-form-label btn btn-primary"
                                                           style="color: #fff;">
                                                        Картинка</label><span
                                                            id="icon-selected" style="display: none">Картинка</span>
                                                    <input type="file" name="cover" id="cover" class="image-upload"
                                                           accept="image/*" style="display:none"><br/>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-success mt-4">Сoхранить</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')

    <script>
        $(document).ready(function () {

            if ($(".error_name_en").val() == "true") {
                $(".tab_regular_en").click();
            }

            $('#input-name-en').on('focusin', function () {
                $(this).data('val', $(this).val());
            });

            $("#input-name-en").on("change", function () {
                $prev = "Prev" + $(this).data("val");
                $current = $(this).val();
                if ($prev === "Prev" && $("#input-slug").val() === "" || "category_" + $(this).data("val") === $("#input-slug").val()) {
                    $("#input-slug").val("category_" + $current);
                }
            });
        });
    </script>
    <script>
        function initImageUpload(box) {
            let uploadField = box.querySelector('.image-upload');

            uploadField.addEventListener('change', getFile);

            function getFile(e) {
                let file = e.currentTarget.files[0];
                checkType(file);
            }

            function previewImage(file) {
                let thumb = box.querySelector('.js--image-preview'),
                    reader = new FileReader();

                reader.onload = function () {
                    thumb.style.backgroundImage = 'url(' + reader.result + ')';
                }
                reader.readAsDataURL(file);
                thumb.className += ' js--no-default';
            }

            function checkType(file) {
                let imageType = /image.*/;
                if (!file.type.match(imageType)) {
                    throw 'Datei ist kein Bild';
                } else if (!file) {
                    throw 'Kein Bild gewählt';
                } else {
                    previewImage(file);
                }
            }

        }

        // initialize box-scope
        var boxes = document.querySelectorAll('.box');

        for (let i = 0; i < boxes.length; i++) {
            let box = boxes[i];
            initDropEffect(box);
            initImageUpload(box);
        }


        /// drop-effect
        function initDropEffect(box) {
            let area, drop, areaWidth, areaHeight, maxDistance, dropWidth, dropHeight, x, y;

            // get clickable area for drop effect
            area = box.querySelector('.js--image-preview');
            area.addEventListener('click', fireRipple);

            function fireRipple(e) {
                area = e.currentTarget
                // create drop
                if (!drop) {
                    drop = document.createElement('span');
                    drop.className = 'drop';
                    this.appendChild(drop);
                }
                // reset animate class
                drop.className = 'drop';

                // calculate dimensions of area (longest side)
                areaWidth = getComputedStyle(this, null).getPropertyValue("width");
                areaHeight = getComputedStyle(this, null).getPropertyValue("height");
                maxDistance = Math.max(parseInt(areaWidth, 10), parseInt(areaHeight, 10));

                // set drop dimensions to fill area
                drop.style.width = maxDistance + 'px';
                drop.style.height = maxDistance + 'px';

                // calculate dimensions of drop
                dropWidth = getComputedStyle(this, null).getPropertyValue("width");
                dropHeight = getComputedStyle(this, null).getPropertyValue("height");

                // calculate relative coordinates of click
                // logic: click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center
                x = e.pageX - this.offsetLeft - (parseInt(dropWidth, 10) / 2);
                y = e.pageY - this.offsetTop - (parseInt(dropHeight, 10) / 2) - 30;

                // position drop and animate
                drop.style.top = y + 'px';
                drop.style.left = x + 'px';
                drop.className += ' animate';
                e.stopPropagation();

            }
        }

    </script>
@endpush