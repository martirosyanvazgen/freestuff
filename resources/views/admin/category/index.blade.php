@extends('layouts.app', [
    'namePage' => 'Категории',
    'class' => 'sidebar-mini',
    'activePage' => 'categories',
    'activeNav' => '',
])
@section('content')
    <div class="panel-header">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary btn-round text-white pull-right"
                           href="{{ route('admin.category.create') }}"> Добавить категорию </a>
                        <h4 class="card-title">Категории</h4>
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar"></div>
                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                                <th>Адрес</th>
                                <th>Картинка</th>
                                <th class="disabled-sorting text-right">Действия</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                                <th>Адрес</th>
                                <th>Картинка</th>
                                <th class="disabled-sorting text-right">Действия</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($categories as $key =>$item)
                                <tr>
                                    <td>
                                        {{ ($categories->firstItem() + $key).'.' }}
                                    </td>
                                    <td>{{$item->name_am}}</td>
                                    <td>{{$item->slug}}</td>
                                    <td>
                                        @if($item->cover)
                                            <img src="{{ asset( '/storage/'.$item['cover'] . '_small.' . $item['ext']) }}"
                                                 width="30" height="30">
                                        @else
                                            Нет картинки
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        <a type="button" href="{{route("admin.category.edit",$item)}}" rel="tooltip"
                                           class="btn btn-success btn-icon btn-sm " data-original-title="" title="">
                                            <i class="now-ui-icons ui-2_settings-90"></i>
                                        </a>
                                        <form action="{{ route('admin.category.destroy', $item) }}" method="post"
                                              style="display:inline-block;" class="delete-form">
                                            @csrf
                                            @method('delete')
                                            <button type="button" rel="tooltip"
                                                    class="btn btn-danger btn-icon btn-sm delete-button"
                                                    data-original-title="" title=""
                                                    onclick="confirm('Вы хотите удалить категорию по ИД:{{'#'.$item->id}}') ? this.parentElement.submit() : ''">
                                                <i class="now-ui-icons ui-1_simple-remove"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav aria-label="...">
                            <ul class="pagination justify-content-end mb-0">
                                {{ $categories->links() }}
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')

@endpush
