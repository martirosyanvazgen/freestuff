@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Группа Фильтров',
    'activePage' => 'filterGroups',
    'activeNav' => 'filterGroups',
])

@section('title')
    <title>Группа Фильтров</title>
@endsection
@section('content')
    <div class="panel-header">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary btn-round text-white pull-right"
                           href="{{ route('admin.filters.filterGroups.create') }}">Добавить Группу Фильтров</a>
                        <h4 class="card-title">ГРУППА ФИЛЬТРОВ</h4>
                        @include('alerts.success', ['key' => 'success'])
                            @include('alerts.errors')
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                        </div>
                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Ключ</th>
                                <th>Название</th>
                                <th>Филтры</th>
                                <th>#ID</th>
                                <th class="disabled-sorting text-right">Действия</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Ключ</th>
                                <th>Название</th>
                                <th>Филтры</th>
                                <th>#ID</th>
                                <th class="disabled-sorting text-right">Действия</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($filterGroups as $key => $filterGroup)
                                <tr>
                                    <td>
                                        {{ ($filterGroups->firstItem() + $key).'.' }}
                                    </td>
                                    <td>{{$filterGroup['key']}}</td>
                                    <td>{{$filterGroup['name_ru']}}</td>
                                    <td>
                                        @if($filterGroup->threeFilters->count())
                                            @foreach($filterGroup->threeFilters  as $k => $filter)
                                                {{ ($k > 0) ? ','.$filter->value_ru : $filter->value_ru}}
                                                @if($k == 2 ) ... @endif
                                            @endforeach
                                        @else
                                            Нет Фильтров
                                        @endif
                                    </td>
                                    <td>{{ '#'.$filterGroup->id }}</td>
                                    <td class="text-right">
                                        <a type="button" href="{{route("admin.filters.filterGroups.show", $filterGroup['id'])}}" rel="tooltip"
                                            class="btn btn-warning btn-icon btn-sm " data-original-title="" title="">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                        <a type="button" href="{{route("admin.filters.filterGroups.edit", $filterGroup['id'])}}" rel="tooltip"
                                           class="btn btn-success btn-icon btn-sm " data-original-title="" title="">
                                            <i class="now-ui-icons ui-2_settings-90"></i>
                                        </a>

                                        <form action="{{ route('admin.filters.filterGroups.delete', $filterGroup['id']) }}" method="POST"
                                              style="display:inline-block;" class="delete-form">
                                            @csrf
                                            @method('delete')
                                            <button type="button" rel="tooltip"
                                                    class="btn btn-danger btn-icon btn-sm delete-button"
                                                    data-original-title="" title=""
                                                    onclick="confirm('Вы хотите удалить Группу Фильтров по ИД:{{'#'.$filterGroup->id}}') ? this.parentElement.submit() : ''">
                                                <i class="now-ui-icons ui-1_simple-remove"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav aria-label="...">
                            <ul class="pagination justify-content-end mb-0">
                                {{ $filterGroups->links() }}
                            </ul>
                        </nav>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
@endsection

@push('js')



    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".submit").click(function(){

                $type = $(this).attr("data-type");
                $id = $(this).attr('id');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "menus/update/menuType",
                    method: "POST",
                    data: { type:$type, id:$id },
                    success: function () {
                        function refresh(){
                            location = ''
                        }
                        refresh();
                    }
                })
            });
        });
    </script>



    <script>
        $(document).ready(function () {
            $(".delete-button").click(function () {
                var clickedButton = $(this);
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    confirmButtonText: 'Yes, delete it!',
                    buttonsStyling: false
                }).then((result) => {
                    if (result.value) {
                        clickedButton.parents(".delete-form").submit();
                    }
                })

            })
            $('#datatable').DataTable({
                "pagingType": "full_numbers",
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search records",
                }

            });

            var table = $('#datatable').DataTable();

            // Edit record
            table.on('click', '.edit', function () {
                $tr = $(this).closest('tr');
                if ($($tr).hasClass('child')) {
                    $tr = $tr.prev('.parent');
                }

                var data = table.row($tr).data();
                alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
            });

            // Delete a record
            table.on('click', '.remove', function (e) {
                $tr = $(this).closest('tr');
                if ($($tr).hasClass('child')) {
                    $tr = $tr.prev('.parent');
                }
                table.row($tr).remove().draw();
                e.preventDefault();
            });

            //Like record
            table.on('click', '.like', function () {
                alert('You clicked on Like button');
            });
        });
    </script>
@endpush