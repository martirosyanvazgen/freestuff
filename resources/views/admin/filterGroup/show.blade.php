@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Группа Фильтров',
    'activePage' => 'filterGroups',
    'activeNav' => 'filterGroups',
])

@section('title')
    <title>Группа Фильтров</title>
@endsection

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">ГРУППА ФИЛЬТРОВ</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('admin.filters.filterGroups') }}" class="btn btn-primary btn-round">Обратно к списку</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-regular">
                            <ul class="nav nav-tabs " id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home"
                                       role="tab" aria-controls="home" aria-selected="true">Армянский</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#ru" role="tab"
                                       aria-selected="false">Русский</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#en" role="tab"
                                       aria-selected="false">Английский</a>
                                </li>
                            </ul>
                                <div class="tab-content" id="myTabContent">

                                    <div class="form-group">
                                        <label class="form-control-label" for="input-key">Ключ <span class="asterisk">*</span> </label>
                                        <input type="text" name="key" id="input-key" class="form-control{{ $errors->has('key') ? ' is-invalid' : '' }}"
                                               placeholder="{{ $filterGroup->key }}"
                                               value="{{ old('key',$filterGroup->key) }}">

                                        @include('alerts.feedback', ['field' => 'key'])
                                    </div>

                                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-name-am">Название <span class="asterisk">*</span> </label>
                                            <input type="text" name="name_am" id="input-name-am" class="form-control{{ $errors->has('name_am') ? ' is-invalid' : '' }}"
                                                   placeholder="{{ $filterGroup->name_am }}"
                                                   value="{{ old('name_am',$filterGroup->name_am) }}">

                                            @include('alerts.feedback', ['field' => 'name_am'])
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="ru" role="tabpanel" aria-labelledby="ru-tab">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-name-ru">Название</label>
                                            <input type="text" name="name_ru" id="input-name-ru" class="form-control{{ $errors->has('name_ru') ? ' is-invalid' : '' }}"
                                                   placeholder="{{ $filterGroup->name_ru }}"
                                                   value="{{ old('name_ru',$filterGroup->name_ru) }}">

                                            @include('alerts.feedback', ['field' => 'name_ru'])
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-name-en">Название</label>
                                            <input type="text" name="name_en" id="input-name-en" class="form-control{{ $errors->has('name_en') ? ' is-invalid' : '' }}"
                                                   placeholder="{{ $filterGroup->name_en }}"
                                                   value="{{ old('name_en',$filterGroup->name_en) }}">

                                            @include('alerts.feedback', ['field' => 'name_en'])
                                        </div>
                                    </div>

                                    @foreach ($errors->all() as $error)

                                        <span class="has-error">
                                            <small style="color: #fb6340">{{ $error }}</small><br>
                                        </span>

                                    @endforeach
                                    <br/><br/>
                                    Список Фильтров
                                    @if($filterGroup->filters()->count())
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Значение</th>
                                                <th scope="col">#ID</th>
                                                <th scope="col" class="text-center">Действия</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($filterGroup->filters as $k => $filter)
                                                <tr>
                                                    <th scope="row">{{ ($k + 1) }}</th>
                                                    <td>{{ $filter['value_ru'] }}</td>
                                                    <th scope="row">{{ '#'.$filter->id}}</th>
                                                    <td class="text-center"><a href="{{ route('admin.filters.edit',$filter->id) }}"><i class="fas fa-pencil-alt"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <h2>нет данных</h2>
                                    @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection