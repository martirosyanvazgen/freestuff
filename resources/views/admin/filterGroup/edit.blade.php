@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Группа Фильтров',
    'activePage' => 'filterGroups',
    'activeNav' => 'filterGroups',
])

@section('title')
    <title>Группа Фильтров</title>
@endsection

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">ГРУППА ФИЛЬТРОВ</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('admin.filters.filterGroups') }}" class="btn btn-primary btn-round">Обратно к списку</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-regular">
                            <ul class="nav nav-tabs " id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home"
                                       role="tab" aria-controls="home" aria-selected="true">Армянский</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#ru" role="tab"
                                       aria-selected="false">Русский</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#en" role="tab"
                                       aria-selected="false">Английский</a>
                                </li>
                            </ul>
                            <form method="POST" action="{{ route('admin.filters.filterGroups.update',$filterGroup->id) }}">
                                @csrf
                                @method('PUT')
                                <div class="tab-content" id="myTabContent">

                                    <div class="form-group">
                                        <label class="form-control-label" for="input-key">Ключ <span class="asterisk">*</span> </label>
                                        <input type="text" name="key" id="input-key" class="form-control{{ $errors->has('key') ? ' is-invalid' : '' }}"
                                               placeholder="{{ $filterGroup->key }}"
                                               value="{{ old('key',$filterGroup->key) }}">

                                        @include('alerts.feedback', ['field' => 'key'])
                                    </div>

                                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-name-am">Название <span class="asterisk">*</span> </label>
                                            <input type="text" name="name_am" id="input-name-am" class="form-control{{ $errors->has('name_am') ? ' is-invalid' : '' }}"
                                                   placeholder="{{ $filterGroup->name_am }}"
                                                   value="{{ old('name_am',$filterGroup->name_am) }}">

                                            @include('alerts.feedback', ['field' => 'name_am'])
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="ru" role="tabpanel" aria-labelledby="ru-tab">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-name-ru">Название</label>
                                            <input type="text" name="name_ru" id="input-name-ru" class="form-control{{ $errors->has('name_ru') ? ' is-invalid' : '' }}"
                                                   placeholder="{{ $filterGroup->name_ru }}"
                                                   value="{{ old('name_ru',$filterGroup->name_ru) }}">

                                            @include('alerts.feedback', ['field' => 'name_ru'])
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-name-en">Название</label>
                                            <input type="text" name="name_en" id="input-name-en" class="form-control{{ $errors->has('name_en') ? ' is-invalid' : '' }}"
                                                   placeholder="{{ $filterGroup->name_en }}"
                                                   value="{{ old('name_en',$filterGroup->name_en) }}">

                                            @include('alerts.feedback', ['field' => 'name_en'])
                                        </div>
                                    </div>

                                    @foreach ($errors->all() as $error)

                                        <span class="has-error">
                                            <small style="color: #fb6340">{{ $error }}</small><br>
                                        </span>

                                    @endforeach
                                    <h4>Фильтры</h4>
                                    <section class="form-group">
                                        <div>
                                            <table class="table table-striped table-bordered">
                                                <thead>
                                                <tr>
                                                    <td>Значение (AM)</td>
                                                    <td>Значение (RU)</td>
                                                    <td>Значение (EN)</td>
                                                    <td>Удалить</td>
                                                </tr>
                                                </thead>
                                                <tbody id="TextBoxContainer">
                                                @if($filterGroup->filters->count() && !(old('value_am')))
                                                    @foreach($filterGroup->filters as $k => $filter)
                                                        <tr>
                                                            <td>
                                                                <input name = "old_value_am[{{$filter->id}}]" type="text" value="{{ $filter->value_am }}" class="form-control" placeholder="Значение" required/>
                                                            </td>
                                                            <td>
                                                                <input name = "old_value_ru[{{$filter->id}}]" type="text" value="{{ $filter->value_ru }}" class="form-control" placeholder="Значение" required/>
                                                            </td>
                                                            <td>
                                                                <input name = "old_value_en[{{$filter->id}}]" type="text" value="{{ $filter->value_en }}" class="form-control" placeholder="Значение" required/>
                                                            </td>
                                                            <td><button type="button" class="btn btn-danger remove">Удалить</button></td>
                                                        </tr>
                                                    @endforeach

                                                    @else

                                                    @if(old('old_value_am'))
                                                        @foreach(old('old_value_am') as $key => $old_name_am)
                                                            <tr>
                                                                <td>
                                                                    <input name = "old_value_am[{{$key}}]" type="text" value="{{ $old_name_am }}" class="form-control" placeholder="Значение" required/>
                                                                </td>
                                                                <td>
                                                                    <input name = "old_value_ru[{{$key}}]" type="text" value="{{ old('old_value_am')[$key] }}" class="form-control" placeholder="Значение" required/>
                                                                </td>
                                                                <td>
                                                                    <input name = "old_value_en[{{$key}}]" type="text" value="{{ old('old_value_am')[$key] }}" class="form-control" placeholder="Значение" required/>
                                                                </td>
                                                                <td><button type="button" class="btn btn-danger remove">Удалить</button></td>
                                                            </tr>
                                                        @endforeach
                                                    @endif

                                                    @if(old('value_am'))
                                                        @foreach(old('value_am') as $k => $name_am)
                                                            <tr>
                                                                <td>
                                                                    <input name = "value_am[]" type="text" value="{{ $name_am }}" class="form-control" placeholder="Значение" required/>
                                                                </td>
                                                                <td>
                                                                    <input name = "value_ru[]" type="text" value="{{ old('value_ru')[$k] }}" class="form-control" placeholder="Значение" required/>
                                                                </td>
                                                                <td>
                                                                    <input name = "value_en[]" type="text" value="{{ old('value_en')[$k] }}" class="form-control" placeholder="Значение" required/>
                                                                </td>
                                                                <td><button type="button" class="btn btn-danger remove">Удалить</button></td>
                                                            </tr>
                                                        @endforeach
                                                    @endif

                                                @endif
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th colspan="5">
                                                        <button id="btnAdd" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls">
                                                            <i class="glyphicon glyphicon-plus-sign"></i>&nbsp; Добавить&nbsp;</button>
                                                    </th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </section>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-success mt-4">Сoхранить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $(function () {
            $("#btnAdd").bind("click", function () {
                var div = $("<tr />");
                div.html(GetDynamicTextBox(""));
                $("#TextBoxContainer").append(div);
            });
            $("body").on("click", ".remove", function () {
                $(this).closest("tr").remove();
            });
        });
        function GetDynamicTextBox(value) {
            return '<td><input name = "value_am[]" type="text" value = "' + value + '" class="form-control" placeholder="Значение" required/></td>' +
                '<td><input name = "value_ru[]" type="text" value = "' + value + '" class="form-control" placeholder="Значение" required/></td>' +
                '<td><input name = "value_en[]" type="text" value = "' + value + '" class="form-control" placeholder="Значение" required/></td>' +
                '<td><button type="button" class="btn btn-danger remove">Удалить</button></td>'
        }
    </script>
@endsection