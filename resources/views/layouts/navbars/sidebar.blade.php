<div class="sidebar" data-color="orange">
    <div class="logo">
        <a href="{{ route('admin.dashboard') }}" class="simple-text logo-mini">
            {{ __('FS') }}
        </a>
        <a href="{{ route('admin.dashboard') }}" class="simple-text logo-normal">
            {{ __('FreeStuff') }}
        </a>
    </div>
    <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">
            <li class="@if ($activePage == 'home') active @endif">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="now-ui-icons design_app"></i>
                    <p>Главная</p>
                </a>
            </li>
            <li class="@if ($activePage == 'subscribers') active @endif">
                <a href="{{ route('admin.subscribers') }}">
                    <i class="now-ui-icons users_single-02"></i>
                    <p>Подписчики</p>
                </a>
            </li>
            <li>
                <a data-toggle="collapse" href="#laravelExamples">
                    <i class="now-ui-icons users_single-02"></i>
                    <p>
                        Пользователи
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse @if ($activePage == 'profile' || $activePage == 'users') show @endif" id="laravelExamples">
                    <ul class="nav">
                        <li class="admin_sub_menu @if ($activePage == 'profile') active @endif">
                            <a href="{{ route('admin.profile') }}">
                                <p> Профиль пользователя </p>
                            </a>
                        </li>
                        <li class="admin_sub_menu @if ($activePage == 'users') active @endif">
                            <a href="{{ route('admin.users') }}">
                                <p> Управление пользователями </p>
                            </a>
                    </ul>
                </div>
            </li>
            <li>
                <a data-toggle="collapse" href="#filters">
                    <i class="fas fa-filter"></i>
                    <p>
                        Фильтры
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse @if ($activePage == 'filterGroups' || $activePage == 'filters' || $activePage == 'categoryFilterGroups') show @endif" id="filters">
                    <ul class="nav">
                        <li class="admin_sub_menu @if ($activePage == 'filterGroups') active @endif">
                            <a href="{{ route('admin.filters.filterGroups') }}">
                                <p> Группа Фильтров </p>
                            </a>
                        </li>
                        <li class="admin_sub_menu @if ($activePage == 'filters') active @endif">
                            <a href="{{ route('admin.filters') }}">
                                <p> Фильтры </p>
                            </a>
                        </li>
                        <li class="admin_sub_menu @if ($activePage == 'categoryFilterGroups') active @endif">
                            <a href="{{ route('admin.filters.categoryFilterGroups') }}">
                                <p> Категория - Фильтры </p>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="@if ($activePage == 'menus') active @endif">
                <a href="{{ route('admin.menus') }}">
                    <i class="fa fa-th-list"></i>
                    <p>Меню</p>
                </a>
            </li>

            <li class="@if ($activePage == 'categories') active @endif">
                <a href="{{ route('admin.categories') }}">
                    <i class="fa fa-list"></i>
                    <p> Категории </p>
                </a>
            </li>
            <li class="@if ($activePage == 'pages') active @endif">
                <a href="{{ route('admin.pages') }}">
                    <i class="fa fa-file"></i>
                    <p>{{ __('Страници') }}</p>
                </a>
            </li>
            <li class="@if ($activePage == 'blog') active @endif">
                <a href="{{ route('admin.blog') }}">
                    <i class="now-ui-icons files_single-copy-04"></i>
                    <p>{{ __('Блог') }}</p>
                </a>
            </li>
            <li class="@if ($activePage == 'settings') active @endif">
                <a href="{{ route('admin.settings') }}">
                    <i class="fa fa-cog"></i>
                    <p>{{ __('Настройки') }}</p>
                </a>
            </li>
            <li class="@if ($activePage == 'labels') active @endif">
                <a href="{{ route('admin.labels') }}">
                    <i class="fas fa-tags"></i>
                    <p>Метки</p>
                </a>
            </li>



{{--            <li class="@if($activePage == 'icons') active @endif">--}}
{{--                <a href="route('page.index','icons') }}">--}}
{{--                    <i class="now-ui-icons education_atom"></i>--}}
{{--                    <p>{{ __('Icons') }}</p>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class="@if($activePage == 'maps') active @endif">--}}
{{--                <a href="{{ route('page.index','maps') }}">--}}
{{--                    <i class="now-ui-icons location_map-big"></i>--}}
{{--                    <p>{{ __('Maps') }}</p>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class="@if ($activePage == 'notifications') active @endif">--}}
{{--                <a href="{{ route('page.index','notifications') }}">--}}
{{--                    <i class="now-ui-icons ui-1_bell-53"></i>--}}
{{--                    <p>{{ __('Notifications') }}</p>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class="@if ($activePage == 'table') active @endif">--}}
{{--                <a href="{{ route('page.index','table') }}">--}}
{{--                    <i class="now-ui-icons design_bullet-list-67"></i>--}}
{{--                    <p>{{ __('Table List') }}</p>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class="@if ($activePage == 'typography') active @endif">--}}
{{--                <a href="{{ route('page.index','typography') }}">--}}
{{--                    <i class="now-ui-icons text_caps-small"></i>--}}
{{--                    <p>{{ __('Typography') }}</p>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class="@if ($activePage == 'upgrade') active @endif active-pro">--}}
{{--                <a href="{{ route('page.index','upgrade') }}">--}}
{{--                    <i class="now-ui-icons arrows-1_cloud-download-93"></i>--}}
{{--                    <p>{{ __('Upgrade to PRO') }}</p>--}}
{{--                </a>--}}
{{--            </li>--}}
        </ul>
    </div>
</div>
