@component('mail::message')
# Introduction

Запрос на сброс пароля.

@component('mail::button', ['url' => env('APP_URL').'/change-password?token='.$token])
Сбросить пароль
@endcomponent

{{ config('app.name') }}
@endcomponent
