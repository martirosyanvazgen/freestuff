<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('ckeditor', 'Admin\CkeditorController@index');
Route::post('ckeditor/upload', 'Admin\CkeditorController@upload')->name('ckeditor.upload');

require_once(__DIR__ . '/Admin/auth.php');

Route::group(['prefix' => 'admin',  'middleware' => 'admin'], function() {
    require_once(__DIR__ . '/Admin/dashboard.php');
    require_once(__DIR__ . '/Admin/user.php');
    require_once(__DIR__ . '/Admin/menu.php');
    require_once(__DIR__ . '/Admin/page.php');
    require_once(__DIR__ . '/Admin/category.php');
    require_once(__DIR__ . '/Admin/setting.php');
    require_once(__DIR__ . '/Admin/profile.php');
    require_once(__DIR__ . '/Admin/permission.php');
    require_once(__DIR__ . '/Admin/subscriber.php');
    require_once(__DIR__ . '/Admin/filter.php');
    require_once(__DIR__ . '/Admin/blog.php');
    require_once(__DIR__ . '/Admin/label.php');
});

require_once(__DIR__ . '/Admin/welcome.php');