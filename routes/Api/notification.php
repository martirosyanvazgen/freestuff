<?php
Route::group(['prefix' => 'notifications','middleware' => 'auth'], function () {
    Route::get('', 'Api\NotificationController@index');
    Route::get('{notification}', 'Api\NotificationController@show');
});