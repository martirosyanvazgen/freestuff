<?php
Route::group(['prefix' => 'roles', 'middleware' => 'auth'], function () {
    Route::get('/', 'Api\RoleController@index');
    Route::post('/store-permission', 'Api\RoleController@storePermission');
    Route::post('/attach-permissions','Api\RoleController@attachPermissions');
    Route::post('/detach-permissions','Api\RoleController@detachPermissions');
    Route::delete('/delete-permission/{id}','Api\RoleController@deletePermission');
    Route::get('/{role}','Api\RoleController@show');
});