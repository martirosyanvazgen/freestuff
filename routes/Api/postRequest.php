<?php
Route::group(['prefix' => 'post_request','middleware' => 'auth'], function () {
    Route::post('/', 'Api\PostRequestController@store');
    Route::post('confirmed', 'Api\PostRequestController@postRequestConfirmed');
    Route::delete('{post_request}', 'Api\PostRequestController@destroy');
    Route::post('post/given', 'Api\PostRequestController@postGiven');
});