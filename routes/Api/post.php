<?php
Route::group(['prefix' => 'posts','middleware' => 'auth'], function () {
    Route::post('/', 'Api\PostController@store');
    Route::post('/{post}', 'Api\PostController@update');
    Route::delete('/{post}', 'Api\PostController@destroy');
});
Route::group(['prefix' => 'posts'], function () {
    Route::get('/', 'Api\PostController@index');
    Route::get('/{post}', 'Api\PostController@show');
});