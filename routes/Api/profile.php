<?php
Route::group(['prefix' => 'profile'], function () {
    Route::get('/posts/{user}', 'Api\ProfileController@posts');
});

Route::group(['prefix' => 'profile', 'middleware' => 'auth'], function () {
    Route::get('/donated/approved/posts', 'Api\ProfileController@donatedPostsApproved');
    Route::get('/taken/approved/posts', 'Api\ProfileController@takenPostsApproved');
    Route::get('/donated/unapproved/posts', 'Api\ProfileController@donatedPostsUnapproved');
    Route::get('/taken/unapproved/posts', 'Api\ProfileController@takenPostsUnapproved');
});
