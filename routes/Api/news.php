<?php
Route::group(['prefix' => 'news','middleware' => 'auth'], function () {
    Route::get('/', 'Api\NewsController@index');
    Route::post('/', 'Api\NewsController@store');
    Route::put('/{news}', 'Api\NewsController@update');
    Route::get('/{news}', 'Api\NewsController@show');
    Route::delete('/{news}', 'Api\NewsController@destroy');
});
