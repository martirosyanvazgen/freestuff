<?php
Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'Auth\AuthController@login');
    Route::get('me', 'Auth\AuthController@me');
    Route::put('/', 'Auth\AuthController@update');
    Route::post('logout', 'Auth\AuthController@logout');
    Route::post('register', 'Auth\AuthController@register');
    Route::post('verify/{token}', 'Auth\AuthController@verify')->name('verify');
    Route::post('refresh-token', 'Auth\AuthController@refresh');
    Route::post('reset-password', 'Auth\ResetPasswordController@sendEmail')->name('reset-password');
    Route::post('change-password', 'Auth\ChangePasswordController@process');
    Route::post('me/change-password', 'Auth\AuthController@changePassword');
    Route::post('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');
    Route::post('{provider}/login', 'Auth\AuthController@login');
});