<?php
Route::group(['prefix' => 'labels'], function () {
    Route::get('/', 'Api\LabelController@index');
    Route::post('', 'Api\LabelController@show');
});
