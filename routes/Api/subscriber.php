<?php
Route::group(['prefix' => 'subscribers'], function () {
    Route::post('/', 'Api\SubscriberController@store');
});