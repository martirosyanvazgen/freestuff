<?php
Route::group(['prefix' => 'categories'], function () {
    Route::get('/', 'Api\CategoryController@index');
    Route::get('/{category}', 'Api\CategoryController@show');
});
