<?php
Route::group(['prefix' => 'menus'], function () {
    Route::get('/', 'Api\MenuController@index');
    Route::get('/{slug}', 'Api\MenuController@show');
});