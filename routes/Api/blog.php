<?php
Route::group(['prefix' => 'blogs'], function () {
    Route::get('/', 'Api\BlogController@index');
    Route::get('/{id}', 'Api\BlogController@show');
});