<?php
Route::group(['prefix' => 'regions_cities'], function () {
    Route::get('/region_city', 'Api\RegionCityController@indexRegionCity');
    Route::get('/city', 'Api\RegionCityController@indexCity');
});