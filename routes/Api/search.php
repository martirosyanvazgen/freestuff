<?php
Route::group(['prefix' => 'search'], function () {
    Route::get('/', 'Api\SearchController@index');
});