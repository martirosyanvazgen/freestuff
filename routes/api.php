<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

require_once(__DIR__ . '/Api/auth.php');
require_once(__DIR__ . '/Api/category.php');
require_once(__DIR__ . '/Api/post.php');
require_once(__DIR__ . '/Api/news.php');
require_once(__DIR__ . '/Api/role.php');
require_once(__DIR__ . '/Api/search.php');
require_once(__DIR__ . '/Api/contact.php');
require_once(__DIR__ . '/Api/profile.php');
require_once(__DIR__ . '/Api/label.php');
require_once(__DIR__ . '/Api/postRequest.php');
require_once(__DIR__ . '/Api/notification.php');
require_once(__DIR__ . '/Api/regionCity.php');
require_once(__DIR__ . '/Api/setting.php');
require_once(__DIR__ . '/Api/menu.php');
require_once(__DIR__ . '/Api/filter.php');
require_once(__DIR__ . '/Api/subscriber.php');
require_once(__DIR__ . '/Api/blog.php');