<?php

Route::group(['prefix' => 'pages'], function () {
    Route::get('/', 'Admin\PageController@index')->name('admin.pages');
    Route::post('/', 'Admin\PageController@store')->name('admin.pages.store');
    Route::get('/create', 'Admin\PageController@create')->name('admin.pages.create');
    Route::get('/{page}', 'Admin\PageController@edit')->name('admin.pages.edit');
    Route::post('/{page}', 'Admin\PageController@update')->name('admin.pages.update');
    Route::delete('/{page}', 'Admin\PageController@destroy')->name('admin.pages.destroy');
});