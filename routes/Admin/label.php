<?php

Route::group(['prefix' => 'labels'], function () {
    Route::get('/', 'Admin\LabelController@index')->name('admin.labels');
    Route::post('/', 'Admin\LabelController@store')->name('admin.label.store');
    Route::get('/create', 'Admin\LabelController@create')->name('admin.label.create');
    Route::get('/{label}', 'Admin\LabelController@edit')->name('admin.label.edit');
    Route::post('/{label}', 'Admin\LabelController@update')->name('admin.label.update');
    Route::delete('/{label}', 'Admin\LabelController@destroy')->name('admin.label.delete');
});
