<?php

Route::group(['prefix' => 'settings'], function () {
    Route::get('/', 'Admin\SettingController@index')->name('admin.settings');
    Route::post('/', 'Admin\SettingController@store')->name('admin.setting.store');
    Route::get('/create', 'Admin\SettingController@create')->name('admin.setting.create');
    Route::get('/{setting}', 'Admin\SettingController@edit')->name('admin.setting.edit');
    Route::post('/{setting}', 'Admin\SettingController@update')->name('admin.setting.update');
    Route::delete('/{setting}', 'Admin\SettingController@destroy')->name('admin.setting.delete');
});
