<?php

Route::group(['prefix' => 'blog'], function () {
    Route::get('/', 'Admin\BlogController@index')->name('admin.blog');
    Route::post('/', 'Admin\BlogController@store')->name('admin.blog.store');
    Route::get('/create', 'Admin\BlogController@create')->name('admin.blog.create');
    Route::get('/{blog}', 'Admin\BlogController@edit')->name('admin.blog.edit');
    Route::post('/{blog}', 'Admin\BlogController@update')->name('admin.blog.update');
    Route::delete('/{blog}', 'Admin\BlogController@destroy')->name('admin.blog.delete');
    Route::get('/delete-image/{blog}', 'Admin\BlogController@deleteImage')->name('admin.blog.delete.image');

});
