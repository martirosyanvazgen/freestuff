<?php

Route::group(['prefix' => 'users', 'middleware' => 'admin'], function () {
    Route::get('/', 'Admin\UserController@index')->name('admin.users');
    Route::get('/create', 'Admin\UserController@create')->name('admin.user.create');
    Route::post('/', 'Admin\UserController@store')->name('admin.user.store');
    Route::get('/{user}', 'Admin\UserController@edit')->name('admin.user.edit');
    Route::put('/{user}', 'Admin\UserController@update')->name('admin.user.update');
    Route::delete('/{user}', 'Admin\UserController@destroy')->name('admin.user.destroy');
    Route::get('/delete-image/{user}', 'Admin\UserController@deleteProfileImage')->name('admin.user.delete.profile.image');
});
