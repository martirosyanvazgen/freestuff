<?php

Route::prefix('permissions')->group(function () {
    Route::get('/', 'Admin\PermissionController@index')->name('admin.permissions');
    Route::get('/create', 'Admin\PermissionController@create')->name('admin.permission.create');
    Route::post('/create', 'Admin\PermissionController@store')->name('admin.permission.store');
    Route::get('/edit/{permission}', 'Admin\PermissionController@edit')->name('admin.permission.edit');
    Route::delete('/{permission}', 'Admin\PermissionController@destroy')->name('admin.permission.destroy');

//    Route::put('/update', 'Admin\ProfileController@update')->name('admin.profile.update');
//    Route::put('/change', 'Admin\ProfileController@changePassword')->name('admin.profile.password');
//    Route::post('/update-photo/{user}', 'Admin\ProfileController@updatePhoto')->name('admin.profile.updatePhoto');
});
