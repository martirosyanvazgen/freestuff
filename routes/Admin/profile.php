<?php

Route::prefix('profile')->group(function () {
    Route::get('/', 'Admin\ProfileController@index')->name('admin.profile');
    Route::put('/update', 'Admin\ProfileController@update')->name('admin.profile.update');
    Route::put('/change', 'Admin\ProfileController@changePassword')->name('admin.profile.password');
    Route::post('/update-photo/{user}', 'Admin\ProfileController@updatePhoto')->name('admin.profile.updatePhoto');
});
