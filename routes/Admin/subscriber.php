<?php

Route::group(['prefix' => 'subscribers'], function () {
    Route::get('subscribers', 'Admin\UserController@getSubscriber')->name('admin.subscribers');
});
