<?php

Route::group(['prefix' => 'categories'], function () {
    Route::get('/', 'Admin\CategoryController@index')->name('admin.categories');
    Route::post('/', 'Admin\CategoryController@store')->name('admin.category.store');
    Route::get('/create', 'Admin\CategoryController@create')->name('admin.category.create');
    Route::get('/edit{category}', 'Admin\CategoryController@edit')->name('admin.category.edit');
    Route::put('/update/{category}', 'Admin\CategoryController@update')->name('admin.category.update');
    Route::delete('/{category}', 'Admin\CategoryController@destroy')->name('admin.category.destroy');
    Route::get('/delete-image/{category}', 'Admin\CategoryController@deleteImage')->name('admin.category.delete.image');
});
