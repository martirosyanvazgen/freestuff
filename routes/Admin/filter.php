<?php

Route::group(['prefix' => 'filters'], function() {
    Route::get('/', 'Admin\FiltersController@index')->name('admin.filters');
    Route::post('/', 'Admin\FiltersController@store')->name('admin.filters.store');
    Route::get('/create', 'Admin\FiltersController@create')->name('admin.filters.create');
    Route::get('/edit/{filter}', 'Admin\FiltersController@edit')->name('admin.filters.edit');
    Route::put('/{filter}', 'Admin\FiltersController@update')->name('admin.filters.update');
    Route::delete('/{filter}', 'Admin\FiltersController@destroy')->name('admin.filters.delete');
    //filter groups
    Route::group(['prefix' => 'filter-groups'], function() {
        Route::get('/', 'Admin\FilterGroupsController@index')->name('admin.filters.filterGroups');
        Route::post('/', 'Admin\FilterGroupsController@store')->name('admin.filters.filterGroups.store');
        Route::get('/create', 'Admin\FilterGroupsController@create')->name('admin.filters.filterGroups.create');
        Route::get('/{filter}', 'Admin\FilterGroupsController@show')->name('admin.filters.filterGroups.show');
        Route::get('/edit/{filter}', 'Admin\FilterGroupsController@edit')->name('admin.filters.filterGroups.edit');
        Route::put('/{filter}', 'Admin\FilterGroupsController@update')->name('admin.filters.filterGroups.update');
        Route::delete('/{filter}', 'Admin\FilterGroupsController@destroy')->name('admin.filters.filterGroups.delete');
    });
    //category filter groups
    Route::group(['prefix' => 'category-filters'], function () {
        Route::get('/', 'Admin\CategoryFilterGroupsController@index')->name('admin.filters.categoryFilterGroups');
        Route::get('/edit/{category}', 'Admin\CategoryFilterGroupsController@edit')->name('admin.filters.categoryFilterGroups.edit');
        Route::put('/{category}', 'Admin\CategoryFilterGroupsController@update')->name('admin.filters.categoryFilterGroups.update');
    });

});
