<?php

Route::group(['prefix' => 'menus'], function () {
    Route::get('privacy-policy', 'Admin\MenuController@index');
    Route::get('/', 'Admin\MenuController@index')->name('admin.menus');
    Route::get('/create', 'Admin\MenuController@create')->name('admin.menu.create');
    Route::post('/store', 'Admin\MenuController@store')->name('admin.menu.store');
    Route::get('/edit/{menu}', 'Admin\MenuController@edit')->name('admin.menu.edit');
    Route::put('/update{menu}', 'Admin\MenuController@update')->name('admin.menu.update');
    Route::delete('/{menu}', 'Admin\MenuController@destroy')->name('admin.menu.destroy');
    Route::post('/update/menuType', 'Admin\MenuController@updateType');
});