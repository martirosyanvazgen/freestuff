<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\PermissionRegistrar;

/**
 * Class PermissionSeeder
 */
class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $permissionsAdmin = [
            'enter_admin',
            'create_sa_user',
            'create_user_not_sa',
            'edit_user_not_sa',
            'create_user',
            'edit_user',
            'delete_user',
            'create_menu',
            'edit_menu',
            'delete_menu',
            'create_category',
            'edit_category',
            'delete_category',
            'create_news',
            'edit_news',
            'delete_news',
            'create_page',
            'edit_page',
            'delete_page',
            'create_post',
            'edit_post',
            'delete_post'
        ];

        $permissionsUser = [
            'create_post',
            'edit_post',
            'delete_post'
        ];

        DB::table('permissions')->insert(array_map(function ($permission) {
            return [
                'name' => $permission,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'guard_name' => 'web',
            ];
        }, $permissionsAdmin));


        DB::table('permissions')->insert(array_map(function ($permission) {
            return [
                'name' => $permission,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'guard_name' => 'api',
            ];
        }, $permissionsUser));
    }
}
