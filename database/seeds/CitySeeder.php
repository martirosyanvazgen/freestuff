<?php

use Illuminate\Database\Seeder;
use App\Models\Region;
use App\Models\City;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = Region::all();

        foreach ($regions as $region){
            $cities = [];
            $regionCities = $this->getCities($region);
            foreach ($regionCities as $regionCity){
                $cities[] = array_merge($regionCity,['region_id' => $region->id]);
            }
            City::insert($cities);
        }
    }

    public function getCities($region){
        $groupedCities = [
            'Yerevan' => [
                [
                    'name_en' => 'Yerevan',
                    'name_ru' => 'Ереван',
                    'name_am' => 'Երևան'
                ]
            ],
            'Aragatsotn' => [
                [
                    'name_en' => 'Ashtarak',
                    'name_ru' => 'Аштарак',
                    'name_am' => 'Աշտարակ'
                ],
                [
                    'name_en' => 'Aparan',
                    'name_ru' => 'Апаран',
                    'name_am' => 'Ապարան'
                ],
                [
                    'name_en' => 'Talin',
                    'name_ru' => 'Талин',
                    'name_am' => 'Թալին'
                ]
            ],
            'Ararat' => [
                [
                    'name_en' => 'Artashat',
                    'name_ru' => 'Арташат',
                    'name_am' => 'Արտաշատ'
                ],
                [
                    'name_en' => 'Ararat',
                    'name_ru' => 'Арарат',
                    'name_am' => 'Արարատ'
                ],
                [
                    'name_en' => 'Vedi',
                    'name_ru' => 'Веди',
                    'name_am' => 'Վեդի'
                ],
                [
                    'name_en' => 'Masis',
                    'name_ru' => 'Масис',
                    'name_am' => 'Մասիս'
                ]
            ],
            'Armavir' => [
                [
                    'name_en' => 'Vagharshapat',
                    'name_ru' => 'Вагаршапат',
                    'name_am' => 'Վաղարշապատ'
                ],
                [
                    'name_en' => 'Armavir',
                    'name_ru' => 'Армавир',
                    'name_am' => 'Արմավիր'
                ],
                [
                    'name_en' => 'Metsamor',
                    'name_ru' => 'Мецамор',
                    'name_am' => 'Մեծամոր'
                ]
            ],
            'Gegharkunik' => [
                [
                    'name_en' => 'Gavar',
                    'name_ru' => 'Гавар',
                    'name_am' => 'Գավառ'
                ],
                [
                    'name_en' => 'Vardenis',
                    'name_ru' => 'Варденис',
                    'name_am' => 'Վարդենիս'
                ],
                [
                    'name_en' => 'Martuni',
                    'name_ru' => 'Мартуни',
                    'name_am' => 'Մարտունի'
                ],
                [
                    'name_en' => 'Sevan',
                    'name_ru' => 'Севан',
                    'name_am' => 'Սևան'
                ],
                [
                    'name_en' => 'Chambarak',
                    'name_ru' => 'Чамбарак',
                    'name_am' => 'Ճամբարակ'
                ]
            ],
            'Kotayk' => [
                [
                    'name_en' => 'Hrazdan',
                    'name_ru' => 'Раздан',
                    'name_am' => 'Հրազդան'
                ],
                [
                    'name_en' => 'Abovyan',
                    'name_ru' => 'Абовян',
                    'name_am' => 'Աբովյան'
                ],
                [
                    'name_en' => 'Byureghavan',
                    'name_ru' => 'Бюрегаван',
                    'name_am' => 'Բյուրեղավան'
                ],
                [
                    'name_en' => 'Yeghvard',
                    'name_ru' => 'Егвард',
                    'name_am' => 'Եղվարդ'
                ],
                [
                    'name_en' => 'Nor Hachn',
                    'name_ru' => 'Нор Ачин',
                    'name_am' => 'Նոր Հաճն'
                ],
                [
                    'name_en' => 'Tsakhadzor',
                    'name_ru' => 'Цахкадзор',
                    'name_am' => 'Ծաղկաձոր'
                ],
                [
                    'name_en' => 'Charencavan',
                    'name_ru' => 'Чаренцаван',
                    'name_am' => 'Չարենցավան'
                ]
            ],
            'Lori' => [
                [
                    'name_en' => 'Vanadzor',
                    'name_ru' => 'Ванадзор',
                    'name_am' => 'Վանաձոր'
                ],
                [
                    'name_en' => 'Alaverdi',
                    'name_ru' => 'Алаверди',
                    'name_am' => 'Ալավերդի'
                ],
                [
                    'name_en' => 'Aghtala',
                    'name_ru' => 'Ахтала',
                    'name_am' => 'Ախթալա'
                ],
                [
                    'name_en' => 'Spitak',
                    'name_ru' => 'Спитак',
                    'name_am' => 'Սպիտակ'
                ],
                [
                    'name_en' => 'Stepanavan',
                    'name_ru' => 'Степанаван',
                    'name_am' => 'Ստեփանավան'
                ],
                [
                    'name_en' => 'Tashir',
                    'name_ru' => 'Ташир',
                    'name_am' => 'Տաշիր'
                ],
                [
                    'name_en' => 'Tumanyan ',
                    'name_ru' => 'Туманян',
                    'name_am' => 'Թումանյան'
                ],
                [
                    'name_en' => 'Shamlugh',
                    'name_ru' => 'Шамлуг',
                    'name_am' => 'Շամլուղ'
                ]
            ],
            'Shirak' => [
                [
                    'name_en' => 'Gyumri',
                    'name_ru' => 'Гюмри',
                    'name_am' => 'Գյումրի'
                ],
                [
                    'name_en' => 'Artik',
                    'name_ru' => 'Артик',
                    'name_am' => 'Արթիկ'
                ],
                [
                    'name_en' => 'Maralik',
                    'name_ru' => 'Маралик',
                    'name_am' => 'Մարալիկ'
                ]
            ],
            'Syunik' => [
                [
                    'name_en' => 'Kapan',
                    'name_ru' => 'Капан',
                    'name_am' => 'Կապան'
                ],
                [
                    'name_en' => 'Agarak',
                    'name_ru' => 'Агарак',
                    'name_am' => 'Ագարակ'
                ],
                [
                    'name_en' => 'Goris',
                    'name_ru' => 'Горис',
                    'name_am' => 'Գորիս'
                ],
                [
                    'name_en' => 'Dastakert',
                    'name_ru' => 'Дастакерт',
                    'name_am' => 'Դաստակերտ'
                ],
                [
                    'name_en' => 'Kajaran',
                    'name_ru' => 'Каджаран',
                    'name_am' => 'Քաջարան'
                ],
                [
                    'name_en' => 'Meghri',
                    'name_ru' => 'Мегри',
                    'name_am' => 'Մեղրի'
                ],
                [
                    'name_en' => 'Sisian',
                    'name_ru' => 'Сисиан',
                    'name_am' => 'Սիսիան'
                ]
            ],
            'Tavush' => [
                [
                    'name_en' => 'Ijevan',
                    'name_ru' => 'Иджеван',
                    'name_am' => 'Իջևան'
                ],
                [
                    'name_en' => 'Berd',
                    'name_ru' => 'Берд',
                    'name_am' => 'Բերդ'
                ],
                [
                    'name_en' => 'Dilijan',
                    'name_ru' => 'Дилижан',
                    'name_am' => 'Դիլիջան'
                ],
                [
                    'name_en' => 'Noyemberyan',
                    'name_ru' => 'Ноемберян',
                    'name_am' => 'Դիլիջան'
                ]
            ],
            'Vayots Dzor' => [
                [
                    'name_en' => 'Yeghegnadzor',
                    'name_ru' => 'Ехегнадзор',
                    'name_am' => 'Եղեգնաձոր'
                ],
                [
                    'name_en' => 'Vayk',
                    'name_ru' => 'Вайк',
                    'name_am' => 'Վայք'
                ],
                [
                    'name_en' => 'Jermuk',
                    'name_ru' => 'Джермук',
                    'name_am' => 'Ջերմուկ'
                ]
            ],
        ];

        return $groupedCities[$region->name_en];
    }
}
