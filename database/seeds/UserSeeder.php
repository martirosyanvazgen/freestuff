<?php

use App\Models\User;
use App\Models\Region;
use App\Models\City;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserSeeder
 */
class UserSeeder extends Seeder
{
    /**
     * @var User $superUser
     */
    private $superUser;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $region = Region::first();
        $city = City::where("region_id", $region->id)->first();
        $this->superUser = $this->seedSuperAdminUser($region->id, $city->id);
        $this->seedUser($region->id, $city->id);
        $this->seedSecondUser($region->id, $city->id);
    }

    /**
     * @return User|Model
     */
    private function seedSuperAdminUser($region_id, $city_id)
    {
        $user = new User();

        $user->name = 'Super Admin';
        $user->login = 'SuperAdmin';
        $user->email = 'super@admin.com';
        $user->region_id = $region_id;
        $user->city_id = $city_id;
        $user->address = 'Arayan 44/2';
        $user->phone_number = '+374-99-88-77-66';
        $user->password = Hash::make('password');
        $user->password_changed_at = Carbon::now();
        $user->surname = 'SuperAdmin';
        $user->active = true;
        $user->notification_on = true;

        $user->save();

        $user->assignRole('super_admin', 'web');
    }

    private function seedUser($region_id, $city_id)
    {
        $user = new User();

        $user->name = 'User';
        $user->login = 'User123';
        $user->email = 'user@user.com';
        $user->region_id = $region_id;
        $user->city_id = $city_id;
        $user->address = 'Arayan 44/3';
        $user->phone_number = '+374-99-88-77-55';
        $user->password = Hash::make('User123');
        $user->password_changed_at = Carbon::now();
        $user->surname = 'User';
        $user->active = true;
        $user->notification_on = true;

        $user->save();

        $user->assignRole('user', 'api');
    }

    private function seedSecondUser($region_id, $city_id)
    {
        $user = new User();

        $user->name = 'User';
        $user->login = 'User1234';
        $user->email = 'user1234@user.com';
        $user->region_id = $region_id;
        $user->city_id = $city_id;
        $user->address = 'Arayan 44/4';
        $user->phone_number = '+374-99-88-77-44';
        $user->password = Hash::make('User1234');
        $user->password_changed_at = Carbon::now();
        $user->surname = 'User';
        $user->active = true;
        $user->notification_on = true;

        $user->save();

        $user->assignRole('user', 'api');
    }
}
