<?php

use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regionData = [
            [
                'name_en' => 'Yerevan',
                'name_ru' => 'Ереван',
                'name_am' => 'Երևան'
            ],
            [
                'name_en' => 'Aragatsotn',
                'name_ru' => 'Арагацотн',
                'name_am' => 'Արագածոտն',

            ],
            [
                'name_en' => 'Ararat',
                'name_ru' => 'Арарат',
                'name_am' => 'Արարատ',
            ],
            [
                'name_en' => 'Armavir',
                'name_ru' => 'Армавир',
                'name_am' => 'Արմավիր',
            ],
            [
                'name_en' => 'Gegharkunik',
                'name_ru' => 'Гегаркуник',
                'name_am' => 'Գեղարքունիք',
            ],
            [
                'name_en' => 'Kotayk',
                'name_ru' => 'Котайк',
                'name_am' => 'Կոտայք',
            ],
            [
                'name_en' => 'Lori',
                'name_ru' => 'Лори',
                'name_am' => 'Լոռի',
            ],
            [
                'name_en' => 'Shirak',
                'name_ru' => 'Ширак',
                'name_am' => 'Շիրակ',
            ],
            [
                'name_en' => 'Syunik',
                'name_ru' => 'Сюник',
                'name_am' => 'Սյունիք',
            ],
            [
                'name_en' => 'Tavush',
                'name_ru' => 'Тавуш',
                'name_am' => 'Տավուշ',
            ],
            [
                'name_en' => 'Vayots Dzor',
                'name_ru' => 'Вайоц Дзор',
                'name_am' => 'Վայոց ձոր',
            ],
        ];

        DB::table('regions')->insert($regionData);
    }
}
