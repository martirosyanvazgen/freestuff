<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

/**
 * Class RoleSeeder
 */
class RoleSeeder extends Seeder
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|Permission[] $permissions
     */
    private $permissions;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->permissions = Permission::all();

        $this->seedSuperAdminRole();
        $this->seedAdminRole();
        $this->seedEditorRole();
        $this->seedUserRole();
    }

    private function seedSuperAdminRole()
    {
        Role::create([
            'name' => 'super_admin',
        ])->givePermissionTo($this->permissions->pluck('name'));
    }

    private function seedAdminRole()
    {
        Role::create([
            'name' => 'admin',
        ])->givePermissionTo($this->permissions->whereNotIn('name', [
            'create_user_not_sa',
            'edit_user_not_sa',
        ])->pluck('name'));
    }

    private function seedEditorRole()
    {
        Role::create([
            'name' => 'editor',
        ])->givePermissionTo([
            'create_menu',
            'edit_menu',
            'delete_menu',
            'create_category',
            'edit_category',
            'delete_category',
            'create_news',
            'edit_news',
            'delete_news',
            'create_page',
            'edit_page',
            'delete_page',
        ]);
    }

    private function seedUserRole()
    {
        Role::create([
            'name' => 'user',
            'guard_name' => 'api'
        ])->givePermissionTo([
            'create_post',
            'edit_post',
            'delete_post'
        ]);;
    }
}
