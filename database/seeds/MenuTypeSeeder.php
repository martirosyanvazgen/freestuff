<?php

use Illuminate\Database\Seeder;

class MenuTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('menu_types')->insert([
            'name' =>'header',
        ]);
        \DB::table('menu_types')->insert([
            'name' =>'footer',
        ]);
    }
}
