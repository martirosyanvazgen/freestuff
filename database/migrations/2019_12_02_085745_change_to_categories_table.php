<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class  ChangeToCategoriesTable extends Migration{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('title_en');
            $table->dropColumn('title_am');
            $table->dropColumn('title_ru');
            $table->string('name_en')->nullable(false);
            $table->string('name_am')->nullable(false);
            $table->string('name_ru')->nullable(false);
            $table->string('cover')->nullable(true);
            $table->string('icon')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::table('categories', function (Blueprint $table) {

        });
    }
}
