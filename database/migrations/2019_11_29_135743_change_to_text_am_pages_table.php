<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeToTextAmPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pages', function (Blueprint $table) {
            $table->dropColumn('name_en');
            $table->dropColumn('name_ru');
            $table->dropColumn('name_am');
            $table->dropColumn('cover');
            $table->dropColumn('slug');
            $table->dropColumn('description_en');
            $table->dropColumn('description_ru');
            $table->dropColumn('description_am');
            $table->dropColumn('text_en');
            $table->text('text_am')->nullable(false)->change();
            $table->text('text_ru')->nullable(true)->change();
            $table->string('title_ru')->nullable(true);
            $table->string('title_am')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pages', function (Blueprint $table) {
            //
        });
    }
}
