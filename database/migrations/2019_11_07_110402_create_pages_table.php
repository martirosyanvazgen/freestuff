<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_en')->nullable(false);
            $table->string('name_am')->nullable(false);
            $table->string('name_ru')->nullable(false);
            $table->string('slug')->unique()->nullable(false);
            $table->string('description_en');
            $table->string('description_am');
            $table->string('description_ru');
            $table->text('text_en');
            $table->text('text_am');
            $table->text('text_ru');
            $table->string('cover');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('pages');
    }
}
