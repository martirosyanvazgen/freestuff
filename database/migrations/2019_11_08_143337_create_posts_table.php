<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title_en')->nullable();
            $table->string('title_am')->nullable();
            $table->string('title_ru')->nullable();
            $table->string('description_en')->nullable();
            $table->string('description_am')->nullable();
            $table->string('description_ru')->nullable();
            $table->text('text_en')->nullable();
            $table->text('text_am')->nullable();
            $table->text('text_ru')->nullable();
            $table->string('cover')->nullable(true);
            $table->boolean('is_active')->default(true)->nullable(false);
            $table->unsignedBigInteger('created_user_id')->nullable(false);
            $table->foreign('created_user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
