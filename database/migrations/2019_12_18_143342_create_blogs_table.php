<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title_am');
            $table->string('title_ru');
            $table->string('title_en');
            $table->string('description_am');
            $table->string('description_ru');
            $table->string('description_rn');
            $table->text('text_am');
            $table->text('text_ru');
            $table->text('text_en');
            $table->unsignedBigInteger('banner_media_id')->nullable();
            $table->unsignedBigInteger('cover_media_id')->nullable();

            $table->foreign('banner_media_id')
                ->references('id')
                ->on('media')
                ->onDelete('cascade');

            $table->foreign('cover_media_id')
                ->references('id')
                ->on('media')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
