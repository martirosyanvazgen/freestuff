<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('menus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title_en')->nullable(false);
            $table->string('title_am')->nullable(false);
            $table->string('title_ru')->nullable(false);
            $table->string('slug')->unique()->nullable(false);
            $table->integer('page_id');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->unsignedBigInteger('type')->nullable();
            $table->foreign('parent_id')->references('id')->on('menus')->onDelete('cascade');
            $table->foreign('type')->references('id')->on('menu_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
