<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToTitlePagesTable extends Migration{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::table('pages', function (Blueprint $table) {
            $table->text('text_en')->nullable(false)->after('text_am');
            $table->string('title_en')->nullable(false)->after('text_ru');
            $table->string('title_ru')->nullable(true)->after('title_en')->change();
            $table->string('title_am')->nullable(false)->after('title_ru')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::table('pages', function (Blueprint $table) {

        });
    }
}
