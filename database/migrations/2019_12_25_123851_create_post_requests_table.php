<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('request_text')->nullable(false);
            $table->boolean('approved')->default(false)->nullable(false);
            $table->unsignedBigInteger('post_id')->nullable(false);
            $table->unsignedBigInteger('created_user_id')->nullable(false);
            $table->unique(['post_id', 'created_user_id']);
            $table->foreign('post_id')->references('id')->on('posts');
            $table->foreign('created_user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_requests');
    }
}
