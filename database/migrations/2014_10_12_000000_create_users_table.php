<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('login')->unique()->nullable(false);
            $table->string('email')->unique()->nullable(false);
            $table->string('password')->index()->nullable(false);
            $table->dateTime('password_changed_at')->nullable(true)->index();
            $table->string('name')->index()->nullable(false);
            $table->string('surname')->index()->nullable(false);
            $table->boolean('active')->index()->nullable(false)->default(false);
            $table->boolean('notification_on')->index()->nullable(false)->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
