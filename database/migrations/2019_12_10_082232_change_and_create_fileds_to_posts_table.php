<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAndCreateFiledsToPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function($table) {
            $table->dropColumn('is_active');
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->boolean('admin_seen')->default(false)->after('created_user_id');
            $table->boolean('approved')->default(false)->after('created_user_id');
            $table->string('status_of_stuff')->after('created_user_id');
            $table->boolean('given')->default(false)->after('created_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            //
        });
    }
}
