<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('labels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title_am')->nullable(false);
            $table->string('title_ru')->nullable(true);
            $table->string('title_en')->nullable(true);
            $table->string('description_am')->nullable(true);
            $table->string('description_ru')->nullable(true);
            $table->string('description_en')->nullable(true);
            $table->string('key')->nullable(false)->unique();
            $table->string('icon')->nullable(true)->unique();
            $table->string('ext')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('labels');
    }
}
