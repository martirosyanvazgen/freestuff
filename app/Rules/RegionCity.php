<?php

namespace App\Rules;

use App\Models\User;
use App\Models\Region;
use App\Models\City;
use Illuminate\Contracts\Validation\Rule;

class RegionCity implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $region_id;

    public function __construct($region_id)
    {
        $this->region_id = $region_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $city = City::where("id", $value)->firstOrFail();
        if($city->region_id == $this->region_id) {
            return true;
        } else {
            $this->message();
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Вы можете выбрать только города в этом регионе.';
    }
}
