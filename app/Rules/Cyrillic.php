<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Cyrillic implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!is_null($value)){
            return preg_match('/^[а-яА-яЁ0-9-№ъ!@#$%^&*()_+|~=`{}\[\]\\:";\'<>?,.\/\s]+$/', $value);
        }else{
            return true;
        }

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute should contain only Russian letters,numbers and special charters.';
    }
}
