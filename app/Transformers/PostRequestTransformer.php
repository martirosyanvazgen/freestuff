<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\PostRequest;

class PostRequestTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'createdUser',
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(PostRequest $postRequest)
    {
        return [
            'id' => $postRequest->id,
            'request_text' => $postRequest->request_text,
            'post_id' => $postRequest->post_id,
            'created_user_id' => $postRequest->created_user_id,
        ];
    }

    public function includeCreatedUser(PostRequest $postRequest)
    {
        if ($postRequest->created_user_id) {
            return $this->item($postRequest->createdUser, new UserTransformer($phone = false));
        }

        return $this->primitive(null);
    }
}
