<?php

namespace App\Transformers;

use App\Models\News;
use League\Fractal\TransformerAbstract;

class NewsTransformer extends TransformerAbstract
{
    public function transform(News $news)
    {
        return [
            'id'                   => (int)$news->id,
            'title_en'             => $news->title_en,
            'title_ru'             => $news->title_ru,
            'title_am'             => $news->title_am,
            'description_en'       => $news->description_en,
            'description_ru'       => $news->description_ru,
            'description_am'       => $news->description_am,
            'text_en'              => $news->text_en,
            'text_ru'              => $news->text_ru,
            'text_am'              => $news->text_am,
            'cover'                => $news->cover,
        ];
    }
}
