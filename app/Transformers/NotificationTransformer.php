<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class NotificationTransformer extends TransformerAbstract
{
    protected $lang;

    public function __construct($lang)
    {
        $this->lang = $lang;
    }

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'request',
        'post'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($notification)
    {
        return [
            'id' => (int)$notification->id,
            'user_id' => $notification->user_id,
            'approved' => $notification->approved,
            'canceled' => $notification->canceled,
        ];
    }

    public function includeRequest($notification)
    {
        if ($notification->postRequest) {
            return $this->item($notification->postRequest, new PostRequestTransformer());
        }
    }

    public function includePost($notification)
    {
        if ($notification->post) {
            return $this->item($notification->post, new PostTransformer($this->lang));
        }
    }
}
