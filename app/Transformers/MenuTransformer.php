<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class MenuTransformer extends TransformerAbstract
{
    protected $lang = 'am';

    public function __construct($lang)
    {
        if (isset($lang)) {
            $this->lang = $lang;
        }
    }

    /**
     * A Fractal transformer.
     *
     * @return array
     */

    protected $availableIncludes = [
        'child',
        'page'
    ];

    protected $defaultIncludes = [
        //
    ];

    public function transform($menu)
    {
        return [
            'id' => $menu->id,
            'title' => $menu['title_' . $this->lang],
            'slug' => $menu->slug,
            'order' => $menu->order,
            'page_id' => $menu->page_id,
            'parent_id' => $menu->parent_id,
            'type' => $menu->type,
        ];
    }

    public function includeChild($menu)
    {
        if (!is_null($menu->child)) {
            return $this->collection($menu->child, new MenuTransformer($this->lang));
        }
    }

    public function includePage($menu)
    {
        if (!is_null($menu->menuPage)) {
            return $this->item($menu->menuPage, new PageTransformer($this->lang));
        }
    }

}
