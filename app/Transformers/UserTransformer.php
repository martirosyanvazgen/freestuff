<?php

namespace App\Transformers;

use App\Models\User;
use League\Fractal\TransformerAbstract;

/**
 * Class UserTransformer
 *
 * @package App\Transformers
 */
class UserTransformer extends TransformerAbstract
{
    protected $phone;

    protected $availableIncludes = [
            'role'
    ];

    protected $defaultIncludes = [

    ];

    public function __construct($phone = false)
    {
        $this->phone = $phone;
    }

    /**
     * A Fractal transformer.
     *
     * @param User $user
     *
     * @return array
     */
    public function transform($user)
    {
        if($this->phone == true) {
            $phone = $user->phone_number;
        } else if ($this->phone == false) {
            $phone = null;
        }

        return [
            'id' => (int)$user->getKey(),
            'login' => $user->login,
            'email' => $user->email,
            'name' => $user->name,
            'surname' => $user->surname,
            'region_id' => $user->region_id,
            'city_id' => $user->city_id,
            'address' => $user->address,
            'phone_number' => $phone,
            'fullName' => "{$user->name} {$user->surname}",
            'isActive' => $user->active,
            'isNotificationOn' => $user->notification_on,
        ];
    }
}
