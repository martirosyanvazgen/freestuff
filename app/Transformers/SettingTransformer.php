<?php

namespace App\Transformers;

use App\Models\Setting;
use League\Fractal\TransformerAbstract;

class SettingTransformer extends TransformerAbstract
{
    protected $lang = 'am';

    public function __construct($lang){
        if(isset($lang)){
            $this->lang = $lang;
        }
    }

    public function transform(Setting $setting)
    {
        return [
            'id'    => (int)$setting->id,
            'kay'   => $setting->kay,
            'value' => $setting['value_'.$this->lang],
        ];
    }
}
