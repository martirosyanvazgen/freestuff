<?php

namespace App\Transformers;

use App\Models\Category;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{

    protected $lang = 'am';

    public function __construct($lang){
        if(isset($lang)){
            $this->lang = $lang;
        }
    }

    protected $availableIncludes = [
        'child',
    ];

    protected $defaultIncludes = [
        //
    ];

    public function transform(Category $category)
    {
        return [
            'id'       => (int)$category->id,
            'name'  => $category['name_'.$this->lang],
            'cover'    => $category->cover,
            'ext'      => $category->ext,
            'icon'     => $category->icon,
            'slug'     => $category->slug,
        ];
    }

    public function includeChild(Category $category)
    {
        return $this->collection($category->child, new CategoryTransformer($this->lang));
    }
}
