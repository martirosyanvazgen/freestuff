<?php

namespace App\Transformers;

use App\Models\Page;
use League\Fractal\TransformerAbstract;

class PageTransformer extends TransformerAbstract
{

    protected $lang = 'am';

    public function __construct($lang)
    {
        if (isset($lang)) {
            $this->lang = $lang;
        }
    }

    public function transform($page)
    {
        return [
            'id' => (int)$page->id,
            'title' => $page['title_' . $this->lang],
            'text' => $page['text_' . $this->lang],
        ];
    }
}
