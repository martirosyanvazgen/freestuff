<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class BlogTransformer extends TransformerAbstract
{
    protected $lang = 'am';

    public function __construct($lang)
    {
        if (isset($lang)) {
            $this->lang = $lang;
        }
    }

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'media'
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'cover'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($blog)
    {
        return [
            'id' => (int)$blog->id,
            'title' => $blog['title_' . $this->lang],
            'description' => $blog['description_' . $this->lang],
            'text' => $blog['text_' . $this->lang]
        ];
    }

    public function includeMedia($blog)
    {
        if ($blog->media) {
            return $this->collection($blog->media, new MediaTransformer());
        }
    }

    public function includeCover($blog)
    {
        if ($blog->cover) {
            return $this->item($blog->cover, new MediaTransformer());
        }
    }
}
