<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\City;

class CityTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */

    protected $lang = 'am';

    public function __construct($lang){
        if(isset($lang)){
            $this->lang = $lang;
        }
    }

    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(City $city)
    {
        return [
            'id' => $city['id'],
            'name' => $city['name_'.$this->lang],
            'region_id' => $city['region_id']
        ];
    }
}
