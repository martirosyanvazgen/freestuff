<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class FilterGroupTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'filter'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($categoryFilterGroup)
    {
        return [
            'id' => (int)$categoryFilterGroup->filterGroup->id,
            'key' => $categoryFilterGroup->filterGroup->key,
            'name' => $categoryFilterGroup->filterGroup->name_en,
        ];
    }

    public function includeFilter($categoryFilterGroup)
    {

        return $this->collection($categoryFilterGroup->filterGroup->filters, new FilterTransformer());
    }
}
