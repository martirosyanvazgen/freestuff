<?php

namespace App\Transformers;

use App\Models\Media;
use League\Fractal\TransformerAbstract;

class MediaTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($media)
    {
        return [
            'id' => (int)$media->id,
            'cover' => $media->cover,
            'ext' => $media->ext,
            'type' => $media->type,
            'type_id' => $media->type_id,
            'sizes' => (boolean)$media->sizes,
        ];
    }
}
