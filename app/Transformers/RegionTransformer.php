<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Region;

class RegionTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $lang = 'am';

    public function __construct($lang){
        if(isset($lang)){
            $this->lang = $lang;
        }
    }

    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'cities',
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Region $region)
    {
        return [
            'id' => $region['id'],
            'name' => $region['name_'.$this->lang]
        ];
    }

    public function includeCities(Region $region)
    {
        if ($region->cities) {
            return $this->collection($region->cities, new CityTransformer($this->lang));
        }

        return $this->primitive(null);
    }
}
