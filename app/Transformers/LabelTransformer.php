<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Label;
use App;

class LabelTransformer extends TransformerAbstract
{

    protected $lang = 'am';

    public function __construct($lang){
        if(isset($lang)){
            $this->lang = $lang;
        }
    }


    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Label $label)
    {
        return [
            'title' => $label['title_'.$this->lang],
            'description' => $label['description_'.$this->lang],
            'key' =>$label['key'],
            'icon' => $label->icon,
            'ext' => $label->ext,
        ];
    }
}
