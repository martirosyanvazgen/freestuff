<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class PostTransformer extends TransformerAbstract
{
    protected $lang;

    public function __construct($lang)
    {
        $this->lang = $lang;
    }

    protected $availableIncludes = [
        'createdUser',
        'media',
        'categories'
    ];

    protected $defaultIncludes = [

    ];

    public function transform($post)
    {
        $lang = $this->lang;
        $bid = $this->bid($post);
        return [
            'id' => (int)$post->id,
            'title' => $post['title_' . $lang],
            'description' => $post['description_' . $lang],
            'text' => $post['text_' . $lang],
            'city_id' => $post->city_id,
            'address' => $post->address,
            'status_of_stuff' => $post->status_of_stuff,
            'bid' => $bid
        ];
    }

    public function includeCreatedUser($post)
    {
        $phone = false;

        if ($post->created_user_id) {
            return $this->item($post->createdUser, new UserTransformer($phone));
        }

        return $this->primitive(null);
    }

    public function includeMedia($post)
    {
        if ($post->media) {
            return $this->collection($post->media, new MediaTransformer());
        }

        return $this->primitive(null);
    }

    public function includeCategories($post)
    {
        if ($post->category) {
            return $this->collection($post->category, new CategoryTransformer($this->lang));
        }

        return $this->primitive(null);
    }

    public function bid($post)
    {
        if (auth('api')->check()) {
            $thisPostRequestCreatedUserIDs = array();
            foreach ($post->postRequest as $postRequest) {
                array_push($thisPostRequestCreatedUserIDs, $postRequest->created_user_id);
            }
            $auth_id = auth('api')->user()->id;
            if ($post->created_user_id != $auth_id && in_array($auth_id, $thisPostRequestCreatedUserIDs) == false) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
