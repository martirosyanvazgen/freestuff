<?php

namespace App\Transformers;

use App\Models\Post;
use League\Fractal\TransformerAbstract;

class SearchPostTransformer extends TransformerAbstract
{
    protected $lang;

    public function __construct($lang) {
        $this->lang = $lang;
    }

    protected $availableIncludes = [
        'createdUser',
        'firstMedia',
        'details'
    ];

    protected $defaultIncludes = [
        'createdUser',
        'firstMedia',
    ];

    public function transform(Post $post)
    {
        $lang = $this->lang;

        return [
            'id'                  => (int)$post->id,
            'title'               => $post['title_'.$lang],
            'description'         => $post['description_'.$lang],
            'city_id'             => $post->city_id,
            'address'             => $post->address,
            'status_of_stuff'     => $post->status_of_stuff,
        ];
    }

    public function includeCreatedUser(Post $post)
    {
        if ($post->created_user_id) {
            return $this->item($post->createdUser, new UserTransformer($phone = false));
        }

        return $this->primitive(null);
    }

    public function includeFirstMedia(Post $post)
    {
        if ($post->firstMedia) {
            return $this->item($post->firstMedia, new MediaTransformer());
        }

        return $this->primitive(null);
    }

    public function includeDetails(Post $post)
    {
        if ($post->details) {
            return $this->collection($post->details, new PostDetailTransformer());
        }

        return $this->primitive(null);
    }
}
