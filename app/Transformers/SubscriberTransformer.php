<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class SubscriberTransformer extends TransformerAbstract
{
    protected $lang = 'am';

    public function __construct($lang)
    {
        if (isset($lang)) {
            $this->lang = $lang;
        }
    }

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($subscriber)
    {
        return [
            'email' => $subscriber['email'],
            'active' => $subscriber['active']
        ];
    }
}
