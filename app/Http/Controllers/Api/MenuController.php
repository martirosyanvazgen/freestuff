<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\MenuTransformer;
use App\Repositories\Api\MenuRepository;

class MenuController extends Controller
{
    protected $lang;

    public function __construct(Request $request)
    {
        $requestLang = $request->header('language');
        if (isset($requestLang)) {
            $this->lang = $requestLang;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $menuRepository = new MenuRepository();

        $menu = $menuRepository->get();

        return fractal($menu, new MenuTransformer($this->lang))
            ->parseIncludes(['child'])
            ->respond();

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function show($slug)
    {
        $menuRepository = new MenuRepository();

        $menu = $menuRepository->show($slug);

        if (!is_null($menu)) {
            return fractal($menu, new MenuTransformer($this->lang))
                ->parseIncludes(['page'])
                ->respond();
        } else {
            return response()->json([
                'status' => 'Error',
                'message' => 'Invalid slug'
            ]);
        }
    }
}