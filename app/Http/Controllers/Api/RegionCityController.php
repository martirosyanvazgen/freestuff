<?php

namespace App\Http\Controllers\Api;

use App\Models\Region;
use App\Models\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\RegionTransformer;
use App\Transformers\CityTransformer;

class RegionCityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $lang;

    public function __construct(Request $request)
    {
        $requestLang = $request->header('language');
        if(isset($requestLang)){
            $this->lang = $requestLang;
        } else {
            $lang = null;
        }
    }

    public function indexRegionCity(Request $request)
    {
        $regions = Region::with('cities')->get();
        return fractal($regions, new RegionTransformer($this->lang))
            ->parseIncludes(
                [
                    'cities',
                ]
            )->respond();
    }

    public function indexCity(Request $request)
    {
        $cities = City::All();
        return fractal($cities, new CityTransformer($this->lang))->respond();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
