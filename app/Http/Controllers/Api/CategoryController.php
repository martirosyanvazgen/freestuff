<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Category\DeleteRequest;
use App\Http\Requests\Api\Category\IndexRequest;
use App\Http\Requests\Api\Category\StoreRequest;
use App\Http\Requests\Api\Category\UpdateRequest;
use App\Models\Category;
use App\Repositories\Api\CategoryRepository;
use App\Transformers\CategoryTransformer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    protected $lang;

    public function __construct(Request $request)
    {
        $requestLang = $request->header('language');
        if (isset($requestLang)) {
            $this->lang = $requestLang;
        }
    }

    public function index(IndexRequest $request)
    {
        $categoryRepository = new CategoryRepository();

        $categories = $categoryRepository->get();

        return fractal($categories, new CategoryTransformer($this->lang))
            ->parseIncludes(['child'])
            ->respond();
    }

    public function show(Category $category)
    {
        return fractal($category, new CategoryTransformer($this->lang))
            ->parseIncludes(['child'])
            ->respond();
    }

}
