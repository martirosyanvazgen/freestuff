<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Post\IndexRequest;
use App\Http\Requests\Api\Post\StoreRequest;
use App\Http\Requests\Api\Post\UpdateRequest;
use App\Http\Requests\Api\Post\DeleteRequest;
use App\Models\Post;
use App\Models\PostView;
use App\Repositories\Api\PostRepository;
use App\Http\Controllers\Controller;
use App\Transformers\PostTransformer;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function __construct(Request $request)
    {
        $language = $request->header('language');
        if (isset($language)) {
            if ($language == 'en' || $language == 'am' || $language == 'ru') {
                $this->lang = $language;
            } else {
                $this->lang = 'en';
            }

        } else {
            $this->lang = 'en';
        }
    }

    public function index(IndexRequest $request)
    {
        $created_user_id = $request->get("created_user_id");
        $category_id = $request->get("category_id");

        $postRepository = new PostRepository();

        $posts = $postRepository->get(
            $created_user_id,
            $category_id
        );

        return fractal($posts, new PostTransformer($this->lang))
            ->parseIncludes(
                [
                    'media',
                    'createdUser',
                    'categories'
                ]
            )->respond();
    }

    public function store(StoreRequest $request)
    {
        $postRepository = new PostRepository();
        $post = $postRepository->store(
            $request->get('title_en', null),
            $request->get('title_ru', null),
            $request->get('title_am', null),
            $request->get('description_en', null),
            $request->get('description_ru', null),
            $request->get('description_am', null),
            $request->get('text_en', null),
            $request->get('text_ru', null),
            $request->get('text_am', null),
            $request->get('city_id', null),
            $request->get('address', null),
            $request->file('cover', null),
            $request->get('status_of_stuff'),
            $request->get('category_ids')
        );

        return fractal($post, new PostTransformer($this->lang))
            ->parseIncludes(
                [
                    'media',
                    'createdUser',
                    'categories'
                ]
            )->respond();
    }

    public function show(Request $request, Post $post)
    {
        $ip = \Request::ip();
        $post_view = PostView::where("IP_address", $ip)->where("post_id", $post['id'])->first();
        if (!$post_view) {
            PostView::create(['post_id' => $post['id'], 'IP_address' => $ip]);

            $post->update([
                $post->view_count = $post->view_count + 1
            ]);
        }

        return fractal($post, new PostTransformer($this->lang))
            ->parseIncludes(
                [
                    'media',
                    'createdUser',
                    'categories'
                ]
            )->respond();
    }

    public function update(UpdateRequest $request, Post $post)
    {
        $postRepository = new PostRepository();

        $post = $postRepository->update(
            $post,
            $request->get('title_en', null),
            $request->get('title_ru', null),
            $request->get('title_am', null),
            $request->get('description_en', null),
            $request->get('description_ru', null),
            $request->get('description_am', null),
            $request->get('text_en', null),
            $request->get('text_ru', null),
            $request->get('text_am', null),
            $request->get('city_id', null),
            $request->get('address', null),
            $request->file('cover', null),
            $request->get('status_of_stuff'),
            $request->get('category_ids')
        );


        return fractal($post, new PostTransformer($this->lang))->respond();
    }

    public function destroy(DeleteRequest $request, Post $post)
    {
        $postRepository = new PostRepository();

        $post = $postRepository->delete(
            $post
        );

        return response()->json($post);
    }
}
