<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Api\SettingRepository;
use App\Transformers\SettingTransformer;

class SettingController extends Controller
{
    protected $lang;

    public function __construct(Request $request)
    {
        $requestLang = $request->header('language');
        if(isset($requestLang)){
            $this->lang = $requestLang;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settingRepository = new SettingRepository();

        $settings = $settingRepository->get();

        return fractal($settings, new SettingTransformer($this->lang))->respond();
    }
}
