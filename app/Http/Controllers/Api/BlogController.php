<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Api\BlogRepository;
use App\Transformers\BlogTransformer;

class BlogController extends Controller
{
    protected $lang;

    public function __construct(Request $request)
    {
        $requestLang = $request->header('language');
        if (isset($requestLang)) {
            $this->lang = $requestLang;
        }
    }

    public function index()
    {
        $blogRepository = new BlogRepository();

        $blog = $blogRepository->get();
        return fractal($blog, new BlogTransformer($this->lang))
            ->parseIncludes('cover')
            ->respond();
    }

    public function show($id)
    {
        $blogRepository = new BlogRepository();

        $blog = $blogRepository->show($id);
        return fractal($blog, new BlogTransformer($this->lang))
            ->respond();
    }
}