<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Label\ShowRequest;
use App\Http\Controllers\Controller;
use App\Transformers\LabelTransformer;
use App\Repositories\Api\LabelRepository;
use Illuminate\Http\Request;

class LabelController extends Controller
{
    protected $lang;

    public function __construct(Request $request)
    {
        $requestLang = $request->header('language');
        if (isset($requestLang)) {
            $this->lang = $requestLang;
        }
    }

    public function index()
    {
        $labelRepository = new LabelRepository();

        $labels = $labelRepository->get();

        return fractal($labels, new LabelTransformer($this->lang))->respond();
    }

    public function show(ShowRequest $request)
    {
        $keys = $request->keys;
        $labelRepository = new LabelRepository();

        $labels = $labelRepository->show($keys);

        return fractal($labels, new LabelTransformer($this->lang))->respond();
    }
}
