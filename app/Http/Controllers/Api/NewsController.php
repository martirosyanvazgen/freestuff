<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\News\IndexRequest;
use App\Http\Requests\Api\News\StoreRequest;
use App\Http\Requests\Api\News\UpdateRequest;
use App\Http\Requests\Api\News\DeleteRequest;
use App\Http\Controllers\Controller;
use App\Models\News;
use App\Transformers\NewsTransformer;
use App\Repositories\Api\NewsRepository;

class NewsController extends Controller{

    public function index(IndexRequest $request){
        $newsRepository = new NewsRepository();

        $news = $newsRepository->get();

        return fractal($news, new NewsTransformer())->respond();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(StoreRequest $request){

        $newsRepository = new NewsRepository();

        $news = $newsRepository->store(
            $request->get('title_en'),
            $request->get('title_am'),
            $request->get('title_ru'),
            $request->get('description_en'),
            $request->get('description_ru'),
            $request->get('description_am'),
            $request->get('text_en'),
            $request->get('text_ru'),
            $request->get('text_am'),
            $request->file('cover')
        );
        return fractal($news, new NewsTransformer())->respond();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show(News $news){

        return fractal($news, new NewsTransformer())->respond();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(UpdateRequest $request, News $news){

        $news->update([
            'title_en'        => $request->get('title_en'),
            'title_ru'        => $request->get('title_ru'),
            'title_am'        => $request->get('title_am'),
            'description_en'  => $request->get('description_en'),
            'description_ru'  => $request->get('description_ru'),
            'description_am'  => $request->get('description_am'),
            'text_en'         => $request->get('text_en'),
            'text_ru'         => $request->get('text_ru'),
            'text_am'         => $request->get('text_am'),
            'cover'           => $request->get('cover')
        ]);

        return fractal($news, new NewsTransformer())->respond();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(DeleteRequest $request,News $news){

        $news->delete();

        return response()->json('success');
    }
}
