<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Api\NotificationRepository;
use App\Transformers\NotificationTransformer;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    protected $lang;

    public function __construct(Request $request)
    {
        $requestLang = $request->header('language');
        if (isset($requestLang)) {
            $this->lang = $requestLang;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notificationRepository = new NotificationRepository();
        $notifications = $notificationRepository->get();
        return fractal($notifications, new NotificationTransformer($this->lang))
            ->parseIncludes(["post", "request"])
            ->respond();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notificationRepository = new NotificationRepository();
        $notification = $notificationRepository->show($id);
        if ($notification) {
            if ($notification->canceled == true) {
                return fractal($notification, new NotificationTransformer($this->lang))
                    ->parseIncludes(["post"])
                    ->respond();
            } else {
                return fractal($notification, new NotificationTransformer($this->lang))
                    ->parseIncludes(["post", "request"])
                    ->respond();
            }
        } else {
            return response()->json([
                'status' => 'Error',
                'message' => 'Notification not found or this notification is not for you.'
            ]);
        }
    }
}
