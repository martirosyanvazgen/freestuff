<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Search\SearchRequest;
use App\Repositories\Api\SearchRepository;
use App\Transformers\SearchPostTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    public function __construct(Request $request)
    {
        $language = $request->header('language');
        if (isset($language)) {
            if ($language == 'en' || $language == 'am' || $language == 'ru') {
                $this->lang = $language;
            } else {
                $this->lang = 'en';
            }

        } else {
            $this->lang = 'en';
        }
    }

    public function index(SearchRequest $request)
    {
        $postRepository = new SearchRepository();

        $posts = $postRepository->getResult(
            $request->get('term'),
            $this->lang
        );

        return fractal($posts, new SearchPostTransformer($this->lang))
            ->parseIncludes(['media','details'])
            ->respond();
    }
}
