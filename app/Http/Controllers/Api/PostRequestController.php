<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PostRequest;
use App\Models\Post;
use App\Models\Notification;
use App\Http\Requests\Api\PostRequest\StoreRequest;
use App\Http\Requests\Api\PostRequest\ShowRequest;
use App\Http\Requests\Api\PostRequest\PostRequestConfirmed;
use App\Http\Requests\Api\PostRequest\PostGivenRequest;
use App\Http\Requests\Api\PostRequest\DeleteRequest;
use App\Repositories\Api\PostRequestRepository;
use App\Repositories\Api\NotificationRepository;
use App\Transformers\PostRequestTransformer;

class PostRequestController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $auth_id = auth('api')->user()->id;
        $post = Post::where("id", $request->get("post_id"))->with("postRequest")->firstOrFail();
        if ($post->postRequest->where("created_user_id", $auth_id)->count() === 0 && $post->created_user_id != $auth_id) {
            $postRequestRepository = new PostRequestRepository();
            $newPostRequest = $postRequestRepository->store(
                $request->get('request_text'),
                $request->get('post_id')
            );
        } else {
            return response()->json([
                'status' => '',
                'message' => 'You cant create post request.'
            ]);
        }

        return fractal($newPostRequest, new PostRequestTransformer())
            ->parseIncludes(['createdUser',])
            ->respond();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(ShowRequest $request, $id)
    {
        $postRequest = PostRequest::where("id", $id)->with('notification')->first();
        if (!$postRequest) {
            return response()->json([
                'status' => 'Error',
                'message' => 'Post request not found.'
            ]);
        } else if ($postRequest->notification->read != true) {
            $postRequest->notification->update(['read' => true]);
        }

        return fractal($postRequest, new PostRequestTransformer())
            ->parseIncludes(['createdUser',])
            ->respond();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteRequest $request, $id)
    {
        $auth = auth('api')->user();
        $postRequest = PostRequest::where("id", $id)->where("created_user_id", $auth->id)->first();

        if ($postRequest) {
            if ($postRequest->approved == true) {
                $postRequest->delete();
                $post = $postRequest->post;
                $notificationRepository = new NotificationRepository();
                $notification = $notificationRepository->store(
                    $post,
                    null,
                    $postId = $post->id,
                    $request = false,
                    $approved = false,
                    $canceled = true
                );

                $postRequest->post->update(['approved' => false]);
            } else {
                $postRequest->delete();
            }
            return response()->json([
                'status' => 'Success',
                'message' => 'Your post request successfully deleted.'
            ]);
        } else {
            return response()->json([
                'status' => 'Error',
                'message' => 'Post request not found or you can only delete the post request you created.'
            ]);
        }
    }

    public function postRequestConfirmed(PostRequestConfirmed $request)
    {
        $notification = Notification::where("id", $request->notification_id)->with('postRequest.post.user')->first();
        if ($notification->postRequest->post->user->id != auth('api')->user()->id) {
            return response()->json([
                'status' => 'Error',
                'message' => 'You can approved only your posts.'
            ]);
        }
        if ($notification->postRequest->post->approved == true) {
            return response()->json([
                'status' => 'Error',
                'message' => 'You already approved this post.'
            ]);
        }
        $notification->update(['read' => true]);
        $notification->postRequest->update(['approved' => true]);
        $notification->postRequest->post->update(['approved' => true]);

        $notificationRepository = new NotificationRepository();
        $confirmedNotification = $notificationRepository->store(
            $notification->postRequest,
            $notification->postRequest,
            $notification->post_id,
            $request = false,
            $approved = true
        );

        return response()->json([
            'status' => 'Success'
        ]);
    }

    public function postGiven(PostGivenRequest $request)
    {
        $auth_id = auth('api')->user()->id;
        $post = Post::where("id", $request->post_id)
            ->where("approved", true)
            ->first();
        $postRequest = PostRequest::where("post_id", $request->post_id)
            ->where("created_user_id", $auth_id)
            ->where("approved", true)
            ->first();
        if ($post->created_user_id === $auth_id || $postRequest) {
            $post->given = true;
            $post->save();
            $post->delete();
            return response()->json([
                'status' => 'success',
            ]);
        } else {
            return response()->json([
                'status' => 'Error',
                'message' => 'This post yet can not make given.'
            ]);
        }
    }

}
