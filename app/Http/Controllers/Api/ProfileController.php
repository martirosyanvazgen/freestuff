<?php

namespace App\Http\Controllers\Api;

use App\Models\Post;
use App\Repositories\Api\PostRepository;
use App\Transformers\PostTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Post\IndexRequest;

class ProfileController extends Controller
{
    public function __construct(Request $request)
    {
        $language = $request->header('language');
        if (isset($language)) {
            if ($language == 'en' || $language == 'am' || $language == 'ru') {
                $this->lang = $language;
            } else {
                $this->lang = 'en';
            }

        } else {
            $this->lang = 'en';
        }
    }

    public function posts($id)
    {
        $posts = Post::where('created_user_id',$id)->with('media')->where('deleted_at', NULL)->get();

        return fractal($posts, new PostTransformer($this->lang))->parseIncludes(
            [
                'media',
            ]
        )->respond();
    }

    public function donatedPostsApproved()
    {
        $auth_id = \Auth::id();
        $posts = Post::where("created_user_id", $auth_id)->where("approved", true)->with("media")->get();

        return fractal($posts, new PostTransformer($this->lang))->parseIncludes(
            [
                'media',
            ]
        )->respond();
    }

    public function takenPostsApproved()
    {
        $auth_id = \Auth::id();
        $posts = Post::where("approved", true)->findByConfirmedPostRequest($auth_id)->with("media")->get();

        return fractal($posts, new PostTransformer($this->lang))->parseIncludes(
            [
                'media',
            ]
        )->respond();
    }

    public function donatedPostsUnapproved()
    {
        $auth_id = \Auth::id();
        $posts = Post::where("created_user_id", $auth_id)->where("approved", false)->with("media")->get();

        return fractal($posts, new PostTransformer($this->lang))->parseIncludes(
            [
                'media',
            ]
        )->respond();
    }

    public function takenPostsUnapproved()
    {
        $auth_id = \Auth::id();
        $posts = Post::where("approved", false)->findByConfirmedPostRequest($auth_id)->with("media")->get();

        return fractal($posts, new PostTransformer($this->lang))->parseIncludes(
            [
                'media',
            ]
        )->respond();
    }


}
