<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Contact\ContactUsRequest;
use App\Mail\ContactMail;
use App\Http\Controllers\Controller;
use Mail;

class ContactController extends Controller
{
    public function __invoke(ContactUsRequest $request){
        $app_email = config('mail.username');

        $request->replace(['to' => $app_email]);

        Mail::send(new ContactMail($request));
        return response()->json(['success' => true]);
    }
}
