<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Role\AttachRequest;
use App\Http\Requests\Api\Role\DetachRequest;
use App\Http\Requests\Api\Role\IndexRequest;
use App\Http\Requests\Api\Role\StoreRequest;
use App\Repositories\Api\RoleRepository;
use App\Transformers\PermissionTransformer;
use App\Transformers\RoleTransformer;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function index(IndexRequest $request){
        $roles = Role::with('permissions')->get();

        return fractal($roles, new RoleTransformer())->respond();
    }

    public function storePermission(StoreRequest $request){
        $roleRepository = new RoleRepository();

        $role = $roleRepository->store(
            $request->get('permission')
        );

        return fractal($role, new PermissionTransformer())->respond();
    }

    public function show(Role $role){
        return fractal($role, new RoleTransformer())->respond();
    }

    public function attachPermissions(AttachRequest $request){
        $roleRepository = new RoleRepository();

        $roles = $roleRepository->attach(
            $request->get('roles'),
            $request->get('permissions')
        );

        return fractal($roles, new RoleTransformer())->respond();
    }

    public function detachPermissions(DetachRequest $request){
        $roleRepository = new RoleRepository();

        $roles = $roleRepository->detach(
            $request->get('roles'),
            $request->get('permissions')
        );

        return fractal($roles, new RoleTransformer())->respond();
    }

    public function deletePermission($id){
        $roleRepository = new RoleRepository();

        $role = $roleRepository->delete($id);

        return response()->json($role);
    }

}
