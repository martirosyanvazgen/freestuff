<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CategoryFilterGroup\UpdateRequest;
use App\Models\Category;
use App\Models\FilterGroup;
use App\Repositories\Admin\CategoryFilterGroupRepository;
use App\Http\Controllers\Controller;

class CategoryFilterGroupsController extends Controller
{
    public function index()
    {
        $categories = Category::where('parent_id',null)->paginate(10);

        return view('admin.categoryFilterGroup.index',[
            'categories' => $categories,
        ]);
    }

    public function edit($id)
    {
        $category = Category::where('id',$id)->with('filterGroups')->firstOrfail();
        $filterGroups = FilterGroup::all();

        return view('admin.categoryFilterGroup.edit',[
            'category' => $category,
            'filterGroups' => $filterGroups,
        ]);
    }

    public function update(UpdateRequest $request, $id)
    {
        $category = Category::where('id',$id)->firstOrfail();

        $categoryFilterGroupRepository = new CategoryFilterGroupRepository();

        $response = $categoryFilterGroupRepository->update(
            $category,
            $request->get('filterGroups')
        );

        return redirect()->route('admin.filters.categoryFilterGroups')->with('success',$response);
    }
}
