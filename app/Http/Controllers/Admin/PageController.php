<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Page\StoreRequest;
use App\Http\Requests\Admin\Page\UpdateRequest;
use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Repositories\Admin\PageRepository;

class PageController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $pageRepository = new PageRepository();
        $page = $pageRepository->get();
        return view('admin.page.index')->with([
                'pages' =>$page
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('admin.page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request){
        $pageRepository = new PageRepository();
        $page = $pageRepository->store(
            $request->get('title_en'),
            $request->get('title_am'),
            $request->get('title_ru'),
            $request->get('text_en'),
            $request->get('text_am'),
            $request->get('text_ru')
        );
        return redirect()->route('admin.pages');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id){
        $page = Page::where('id',$id)->firstOrfail();
        return view('admin.page.edit')->with([
            'page' => $page,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(UpdateRequest $request, Page $page){
        $page->update([
            'title_en' => $request->get('title_en'),
            'title_am' => $request->get('title_am'),
            'title_ru' => $request->get('title_ru'),
            'text_en'  => $request->get('text_en'),
            'text_am'  => $request->get('text_am'),
            'text_ru'  => $request->get('text_ru'),
        ]);
        return redirect()->route('admin.pages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id){
        $pages = Page::where('id',$id)->firstOrfail();
        $pages->delete();
        return redirect()->route('admin.pages');
    }
}
