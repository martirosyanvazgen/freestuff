<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Menu\StoreRequest;
use App\Http\Requests\Admin\Menu\UpdateRequest;
use App\Repositories\Admin\MenuRepository;
use App\Models\Menu;
use App\Models\Page;
use App\Models\MenuType;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $menus = Menu::orderBy('id', 'desc')->paginate(10);
        return view('admin.menu.index')->with(['menus' => $menus]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pages = Page::All();
        $menus = Menu::All();
        $menu_types = MenuType::All();
        return view('admin.menu.create', ['pages' => $pages, 'menus' => $menus, 'menu_types' => $menu_types]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $menuRepository = new MenuRepository();

        $menu = $menuRepository->store(
            $request->title_en,
            $request->title_am,
            $request->title_ru,
            $request->slug,
            $request->order,
            $request->page_id,
            $request->parent_id,
            $request->type
        );

        return redirect()->route('admin.menus')->with('success', 'Меню успешно создано.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Menu::where('id', $id)->firstOrFail();
        $menus = Menu::where('type', $item['type'])->where('id', '!=', $item['id'])->get();
        $pages = Page::All();
        $menu_types = MenuType::All();
        return view('admin.menu.edit')->with(['menus' => $menus, 'pages' => $pages, 'menu_types' => $menu_types,'item' => $item]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $menuRepository = new MenuRepository();

        $menu = $menuRepository->update(
            $request->get('title_en'),
            $request->get('title_am'),
            $request->get('title_ru'),
            $request->get('parent_id'),
            $request->get('order'),
            $request->get('slug'),
            $request->get('page_id'),
            $id
        );

        return redirect()->route('admin.menus')->with('success', 'Меню успешно обновлено.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        $menu->delete();
        return redirect()->route('admin.menus')->with('success', 'Меню успешно удалено.');
    }

    public function updateType()
    {
        $new_type = '';
        $menu = Menu::where("id", $_POST['id'])->firstOrFail();
        if($_POST['type'] == 'header'){
            $menu_type = MenuType::where('name', 'footer')->firstOrFail();
            $new_type = $menu_type['id'];
        }else if($_POST['type'] == 'footer'){
            $menu_type = MenuType::where('name', 'header')->firstOrFail();
            $new_type = $menu_type['id'];
        }
        if($menu->parent_id != null){
            $menu->parent_id = null;
        }
        $menu->type = $new_type;
        $menu->save();
    }
}