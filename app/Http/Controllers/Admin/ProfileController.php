<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Http\Requests\Admin\Profile\UpdateRequest;
use App\Http\Requests\Admin\Profile\ChangePasswordRequest;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index()
    {
        return view('admin.profile.edit');
    }

    public function update(UpdateRequest $request)
    {
        auth()->user()->update([
            'login' => $request->get('login'),
            'name' => $request->get('name'),
            'surname' => $request->get('surname'),
            'email' => $request->get('email')
            ]);

        return redirect()->route('admin.profile')->with('profile_update', __('Профиль успешно обновлен..'));
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        if (Hash::check($request->old_password, auth()->user()->password)) {
            auth()->user()->update(['password' => Hash::make($request->get('password'))]);
            return back()->with('password_status', __('Пароль успешно обновлен.'));
        } else {
            return back()->with('password_status_false', __('Действующий пароль не совпадает.'));
        }
    }
}
