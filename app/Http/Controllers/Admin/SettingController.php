<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\Setting\StoreRequest;
use App\Http\Requests\Admin\Setting\UpdateRequest;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Repositories\Admin\SettingRepository;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Setting::orderBy('id', 'desc')->paginate(10);
        return view('admin.setting.index', compact('settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        return view('admin.setting.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $settingRepository = new SettingRepository();
        $setting = $settingRepository->store(
            $request->get('value_am'),
            $request->get('value_ru'),
            $request->get('value_en'),
            $request->get('key')
        );

        return redirect()->route('admin.settings')->with('success', 'Настройка успешно создано.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Setting $setting
     * @return \Illuminate\Http\Response
     */

    public function edit(Setting $setting)
    {
        return view('admin.setting.edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Setting $setting
     * @return \Illuminate\Http\Response
     */

    public function update(UpdateRequest $request, $id)
    {
        $settingRepository = new SettingRepository();
        $setting = $settingRepository->update(
            $request->get('value_am'),
            $request->get('value_ru'),
            $request->get('value_en'),
            $id
        );

        return redirect()->route('admin.settings')->with('success', 'Настройка успешно обновлено.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Setting $setting
     * @return \Illuminate\Http\Response
     */

    public function destroy(Setting $setting)
    {
        $setting->delete();

        return redirect()->route('admin.settings')->with('success', 'Настройка успешно удалено.');
    }
}
