<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Filter\StoreRequest;
use App\Http\Requests\Admin\Filter\UpdateRequest;
use App\Models\Filter;
use App\Models\FilterGroup;
use App\Repositories\Admin\FilterRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

class FiltersController extends Controller
{
    public function index()
    {

        $filters = Filter::orderBy('id','DESC')->with('group')->paginate(10);
        return view('admin.filter.index',[
            'filters' => $filters,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $filterGroups = FilterGroup::all();

        return view('admin.filter.create',['filterGroups' => $filterGroups]);
    }


    /**
     * @param StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $filterRepository = new FilterRepository();

        $response = $filterRepository->store(
            $request->get('value_en'),
            $request->get('value_ru'),
            $request->get('value_am'),
            $request->get('group_id')
        );

        return redirect()->route('admin.filters')->with('success',$response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $filter = Filter::where('id',$id)->with('group')->firstOrfail();

        $filterGroups = FilterGroup::all();

        return view('admin.filter.edit',['filter' => $filter,'filterGroups' => $filterGroups]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $filter = Filter::where('id',$id)->firstOrfail();

        $filterRepository = new FilterRepository();

        $response = $filterRepository->update(
            $filter,
            $request->get('value_en'),
            $request->get('value_ru'),
            $request->get('value_am'),
            $request->get('group_id')
        );

        return redirect()->route('admin.filters')->with('success',$response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $filter = Filter::where('id',$id)->firstOrfail();

        $filterRepository = new FilterRepository();

        $response = $filterRepository->delete($filter);

        return redirect()->route('admin.filters')->with('success',$response);
    }
}
