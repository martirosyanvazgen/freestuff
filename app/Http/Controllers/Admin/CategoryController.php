<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\UploadPhotoRepository;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\Category\StoreRequest;
use App\Http\Requests\Admin\Category\UpdateRequest;
use App\Models\Category;
use App\Repositories\Admin\CategoryRepository;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $categoryRepository = new CategoryRepository();
        $category = $categoryRepository->get();
        return view('admin.category.index')->with([
            'categories' => $category
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $categoryRepository = new CategoryRepository();
        $category = $categoryRepository->create();
        return view('admin.category.create', ['categories' => $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(StoreRequest $request)
    {
        $categoryRepository = new CategoryRepository();
        $category = $categoryRepository->store(
            $request->get('name_en'),
            $request->get('name_am'),
            $request->get('name_ru'),
            $request->get('slug'),
            $request->file('icon'),
            $request->file('cover')
        );

        return redirect()->route('admin.categories')->with('success', 'Категория успешно создано.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $categories = Category::where('id', '!=', $category['id'])->get();
        return view('admin.category.edit')->with([
            'item' => $category,
            'categories' => $categories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function update(UpdateRequest $request, Category $category)
    {
        $categoryRepository = new CategoryRepository();
        $category = $categoryRepository->update(
            $request->get('name_en'),
            $request->get('name_am'),
            $request->get('name_ru'),
            $request->get('slug'),
            $request->get('parent_id'),
            $request->get('old_cover'),
            $request->file('cover'),
            $request->get('old_icon'),
            $request->file('icon'),
            $category
        );

        return redirect()->route('admin.categories')->with('success', 'Категория успешно обновлено.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Category $category)
    {
        $categoryRepository = new CategoryRepository();
        $category = $categoryRepository->destroy(
            $category
        );
        return redirect()->route('admin.categories')->with('success', 'Категория успешно удалено.');
    }

    public function deleteImage($id)
    {
        $category = Category::where("id", $id)->firstOrFail();
        $categoryRepository = new UploadPhotoRepository();
        $cover = $categoryRepository->delete($category, $cover = 'cover');
        $category->cover = null;
        $category->ext = null;
        $category->save();
        return redirect()->route('admin.category.edit', $id)->with('success', 'Картинка успешно удалено');
    }
}
