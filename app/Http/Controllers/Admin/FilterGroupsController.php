<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\FilterGroup\StoreRequest;
use App\Http\Requests\Admin\FilterGroup\UpdateRequest;
use App\Models\FilterGroup;
use App\Repositories\Admin\FilterGroupRepository;
use App\Http\Controllers\Controller;

class FilterGroupsController extends Controller
{
    public function index(){

        $filterGroups = FilterGroup::orderBy('id', 'desc')->paginate(10);

        return view('admin.filterGroup.index')->with(['filterGroups' => $filterGroups]);
    }

    public function create()
    {
        return view('admin.filterGroup.create');
    }

    public function store(StoreRequest $request)
    {
        $filterGroupRepository = new FilterGroupRepository();

        $response = $filterGroupRepository->store(
            $request->get('key'),
            $request->get('name_en'),
            $request->get('name_ru'),
            $request->get('name_am'),
            $request->get('value_am',[]),
            $request->get('value_ru',[]),
            $request->get('value_en',[])
        );
        return redirect()->route('admin.filters.filterGroups')->with('success',$response);
    }

    public function show($id)
    {
        $filterGroup = FilterGroup::where('id',$id)->with('filters')->firstOrfail();


        return view('admin.filterGroup.show',['filterGroup' => $filterGroup]);
    }

    public function edit($id)
    {
        $filterGroup = FilterGroup::where('id',$id)->with('filters')->firstOrfail();

        return view('admin.filterGroup.edit',['filterGroup' => $filterGroup]);
    }

    public function update(UpdateRequest $request, $id)
    {
        $filterGroup = FilterGroup::where('id',$id)->firstOrfail();

        $filterGroupRepository = new FilterGroupRepository();

        $response = $filterGroupRepository->update(
            $filterGroup,
            $request->get('key'),
            $request->get('name_en'),
            $request->get('name_ru'),
            $request->get('name_am'),
            $request->get('old_value_am',[]),
            $request->get('old_value_ru',[]),
            $request->get('old_value_en',[]),
            $request->get('value_am',[]),
            $request->get('value_ru',[]),
            $request->get('value_en',[])
        );

        return redirect()->route('admin.filters.filterGroups')->with('success',$response);
    }

    public function destroy($id)
    {
        $filterGroup = FilterGroup::where('id',$id)->firstOrfail();

        $filterGroupRepository = new FilterGroupRepository();

        $response = $filterGroupRepository->delete($filterGroup);

        return redirect()->route('admin.filters.filterGroups')->with('success',$response);

    }

}
