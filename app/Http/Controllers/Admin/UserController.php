<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Role;
use App\Models\Region;
use App\Models\City;
use App\Models\Subscriber;
use App\Repositories\UploadPhotoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\StoreRequest;
use App\Http\Requests\Admin\User\UpdateRequest;
use App\Repositories\Admin\UserRepository;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('id', 'desc')->paginate(10);
        return view('admin.user.index')->with([
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $regions = Region::All();
        $cities = City::All();
        return view('admin.user.create', compact('regions','cities'));
    }

    public function getSubscriber()
    {
        $subscribers = Subscriber::All();
        return view('admin.subscriber.index')->with([
            'subscribers' => $subscribers
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $userRepository = new UserRepository();
        $user = $userRepository->store(
            $request->get('name'),
            $request->get('surname'),
            $request->get('login'),
            $request->get('email'),
            $request->get('region_id'),
            $request->get('city_id'),
            $request->get('address'),
            $request->get('phone_number'),
            $request->get('password'),
            $request->file('image'),
            $request->has('notification')
        );

        return redirect()->route('admin.users')->with('success', 'Информация о пользователе успешно добавлено');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $regions = Region::All();
        $cities = City::All();
        return view('admin.user.edit', compact('user', 'regions', 'cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, User $user)
    {
        $userRepository = new UserRepository();
        $user = $userRepository->update(
            $request->get('name'),
            $request->get('surname'),
            $request->get('login'),
            $request->get('email'),
            $request->get('region_id'),
            $request->get('city_id'),
            $request->get('address'),
            $request->get('phone_number'),
            $request->file('image'),
            $request->has('notification'),
            $user
        );

        return redirect()->route('admin.users')->with('success', 'Информация о пользователе успешно обновлено');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if($user->image){
            $userRepository = new UserRepository();
            $deletedUser = $userRepository->destroy($user);
        }

        $user->delete();
        return redirect()->route('admin.users')->with('success', 'Информация о пользователе успешно удалено.');

    }

    public function deleteProfileImage($id)
    {
        $user = User::where("id", $id)->firstOrFail();
        $userRepository = new UploadPhotoRepository();
        $image = $userRepository->delete($user, $cover = false, $field_image = 'image');
        $user->image = null;
        $user->save();
        return redirect()->route('admin.user.edit', $id)->with('success', 'Картинка успешно удалено');
    }
}
