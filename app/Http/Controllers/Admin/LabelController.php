<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Label;
use App\Http\Requests\Admin\Label\StoreRequest;
use App\Http\Requests\Admin\Label\UpdateRequest;
use App\Repositories\Admin\LabelRepository;

class LabelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $labels = Label::orderBy('id','DESC')->paginate(10);
        return view('admin.label.index', compact('labels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.label.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $labelRepository = new LabelRepository();

        $label = $labelRepository->store(
            $request->get('title_am'),
            $request->get('title_ru'),
            $request->get('title_en'),
            $request->get('description_am'),
            $request->get('description_ru'),
            $request->get('description_en'),
            $request->get('key'),
            $request->file('icon',null)
        );

        return redirect()->route('admin.labels')->with('success', 'Метка успешно создано.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Label $label)
    {
        return view('admin.label.edit', compact('label'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $labelRepository = new LabelRepository();

        $label = $labelRepository->update(
            $request->get('title_am'),
            $request->get('title_ru'),
            $request->get('title_en'),
            $request->get('description_am'),
            $request->get('description_ru'),
            $request->get('description_en'),
            $request->file('icon',null),
            $request->get('has_icon'),
            $id
        );

        return redirect()->route('admin.labels')->with('success', 'Метка успешно обновлено.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Label $label)
    {
        $deleteLabel = new LabelRepository();
        $label = $deleteLabel->destroy(
            $label
        );

        return redirect()->route('admin.labels')->with('success', 'Метка успешно удалено.');
    }
}
