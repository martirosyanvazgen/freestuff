<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Blog\DeleteImageRequest;
use App\Http\Requests\Admin\Blog\StoreRequest;
use App\Http\Requests\Admin\Blog\UpdateRequest;
use App\Models\Blog;
use App\Models\Media;
use App\Repositories\Admin\BlogRepository;
use App\Repositories\UploadPhotoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogCollection = Blog::orderBy('id','DESC')->with('cover')->paginate(10);

        return view('admin.blog.index')->with([
            'blogCollection' => $blogCollection
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $blogRepository = new BlogRepository();

        $response = $blogRepository->store(
            $request->get('title_en'),
            $request->get('title_ru'),
            $request->get('title_am'),
            $request->get('description_en'),
            $request->get('description_ru'),
            $request->get('description_am'),
            $request->get('text_en'),
            $request->get('text_ru'),
            $request->get('text_am'),
            $request->file('banner',null),
            $request->file('cover',null)
        );

        return redirect()->route('admin.blog')->with('success',$response);
    }

    public function edit($id)
    {
        $blog = Blog::where('id',$id)->with('media')->firstOrfail();

        $banner = null;
        $cover = null;

        if ($blog->media->count()){
            foreach ($blog->media as $media){
                if ($media->type == 'Blog/banner'){
                    $banner = $media;
                }else if($media->type == 'Blog/cover') {
                    $cover = $media;
                }
            }
        }

        return view('admin.blog.edit')->with([
            'blog' => $blog,
            'banner' => $banner,
            'cover' => $cover,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $blog = Blog::where('id',$id)->with('media')->firstOrfail();
        $blogRepository = new BlogRepository();

        $response = $blogRepository->update(
            $blog,
            $request->get('title_en'),
            $request->get('title_ru'),
            $request->get('title_am'),
            $request->get('description_en'),
            $request->get('description_ru'),
            $request->get('description_am'),
            $request->get('text_en'),
            $request->get('text_ru'),
            $request->get('text_am'),
            $request->file('banner',null),
            $request->file('cover',null)
        );

        return redirect()->route('admin.blog')->with('success',$response);
    }

    public function deleteImage(DeleteImageRequest $request,$id)
    {
        $blog = Blog::where('id',$id)->firstOrfail();
        $media = Media::where('id',$request->get('id'))->get();

        $photoRepository = new UploadPhotoRepository();
        $photoRepository->deleteMediaTable($blog,$media);
        Media::where('id',$request->get('id'))->delete();

        return redirect()->route('admin.blog.edit',$id)->with('success','картинка успешно удалено');
    }

    public function destroy($id)
    {
        $blog = Blog::where('id',$id)->firstOrfail();

        $blogRepository = new BlogRepository();

        $response = $blogRepository->delete($blog);

        return redirect()->route('admin.blog')->with('success',$response);
    }
}
