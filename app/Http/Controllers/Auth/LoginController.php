<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
//use App\Models\Admin;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;
use Auth;
use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    use HasRoles;

    protected $frontUser = 'user';

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->stateless()->user();
        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser, true);
        return redirect($this->redirectTo);
    }

    public function beforeLoginSocial($request)
    {
//        $this->validateLogin($request);
        $user = User::where('email', $request->email)->first();
        if ($user) {
            if ($request->password === $user->password) {
                $role = $user->roles->pluck('name');
                if($role[0] != $this->frontUser)
                {
                    return $this->login($request);
                }else{
                    $this->incrementLoginAttempts($request);

                    return $this->sendFailedLoginResponse($request);
                }
            } else {
                $this->incrementLoginAttempts($request);

                return $this->sendFailedLoginResponse($request);
            }
        } else {
            $this->incrementLoginAttempts($request);

            return $this->sendFailedLoginResponse($request);
        }

    }

    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::where("provider_id", $user['id'])->first();
        if($authUser){
            return $authUser;
        }

        $userGoogle = new User();

        $userGoogle->name = $user->name;
        $userGoogle->login = $user->email;
        $userGoogle->email = $user->email;
        $userGoogle->password = Hash::make($user->id);
        $userGoogle->password_changed_at = Carbon::now();
        $userGoogle->surname = $user->name;
        $userGoogle->active = true;
        $userGoogle->notification_on = true;
        $userGoogle->provider = strtoupper($provider);
        $userGoogle->provider_id = $user->id;

        $userGoogle->save();

        $userGoogle->assignRole('super_admin', 'web');
        return $userGoogle;

    }

    public function beforeLogin(Request $request)
    {
        $this->validateLogin($request);

        $user = User::where('email', $request->email)->first();

        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $role = $user->roles->pluck('name');
                if($role[0] != $this->frontUser)
                {
                    return $this->login($request);
                }else{
                    $this->incrementLoginAttempts($request);

                    return $this->sendFailedLoginResponse($request);
                }
            } else {
                $this->incrementLoginAttempts($request);

                return $this->sendFailedLoginResponse($request);
            }
        } else {
            $this->incrementLoginAttempts($request);

            return $this->sendFailedLoginResponse($request);
        }

    }
}
