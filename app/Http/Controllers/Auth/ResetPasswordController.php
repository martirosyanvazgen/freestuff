<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\ResetPasswordRequest;
use App\Mail\ResetPasswordMail;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ResetPasswordController
 *
 * @package App\Http\Controllers\Auth
 */
class ResetPasswordController extends Controller
{
    /**
     * @param ResetPasswordRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendEmail(ResetPasswordRequest $request)
    {
        if (!$this->validateEmail($request->get('email'))) {
            $this->failedResponse();
        }

        $this->send($request->get('email'));

        return $this->successResponse();
    }

    /**
     * @param string $email
     */
    private function send($email)
    {
        $token = $this->getResetPasswordToken($email);
        Mail::to($email)->send(new ResetPasswordMail($token, $email));
    }

    /**
     * @param string $email
     *
     * @return string
     */
    private function getResetPasswordToken($email)
    {
        $oldToken = DB::table('password_resets')->where('email', $email)->first();

        if ($oldToken) {
            return $oldToken->token;
        }

        $token = Str::random(60);
        $this->saveToken($token, $email);

        return $token;
    }

    /**
     * @param string $token
     * @param string $email
     */
    private function saveToken($token, $email)
    {
        DB::table('password_resets')->insert([
            'email' => $email,
            'token' => $token,
            'created_at' => Carbon::now(),
        ]);
    }

    /**
     * @param string $email
     *
     * @return bool
     */
    private function validateEmail($email)
    {
        return User::whereEmail($email)->exists();
    }

    /**
     * @return void
     */
    private function failedResponse()
    {
        throw new NotFoundHttpException();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    private function successResponse()
    {
        return response()->json(['success' => 'Ссылка на сброс пароля отправлен на вашу эл.почту']);
    }
}