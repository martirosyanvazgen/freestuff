<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\ChangeOldPaswordRequest;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Http\Requests\Api\Auth\UpdateRequest;
use App\Http\Requests\Api\Auth\RegisterRequest;
use App\Models\User;
use App\Repositories\UploadPhotoRepository;
use App\Transformers\UserTransformer;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

use Auth;
use Socialite;


/**
 * Class AuthController
 *
 * @package App\Http\Controllers\Auth
 */
class AuthController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['login', 'register', 'verify', 'handleProviderCallback']]);
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider, Request $request)
    {
//        $user = Socialite::driver($provider)->stateless()->user();

        $user = ['name' => 'first_name', 'email' => 'abcdafsdgdfdsfdhg.abcd.@gmail.com', 'id' => '123456fgfdgfsdgdgsfdg789abcdefjhk'];

        $authUser = $this->findOrCreateUser($user, $provider);
//        $jwtToken = $this->socialLogin($social = $authUser, $socialPassword = $user->id);
        $jwtToken = $this->socialLogin($social = $authUser, $socialPassword = $user['id']);
        return $jwtToken;
    }

    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::where("provider_id", $user['id'])->first();
        if ($authUser) {
            return $authUser;
        }

        $userGoogle = new User();

        $userGoogle->name = $user['name'];
        $userGoogle->login = $user['email'];
        $userGoogle->email = $user['email'];
        $userGoogle->password = Hash::make($user['id']);
        $userGoogle->password_changed_at = Carbon::now();
        $userGoogle->surname = 'userGoogle';
        $userGoogle->active = true;
        $userGoogle->notification_on = true;
        $userGoogle->provider = strtoupper($provider);
        $userGoogle->provider_id = $user['id'];

        $userGoogle->save();

        $userGoogle->assignRole('user', 'api');
        return $userGoogle;

    }

    public function socialLogin($social, $socialPassword)
    {
        if ($social) {
            $credentials = ['login' => $social['login'], 'password' => $socialPassword];

            if (!$token = auth('api')->attempt($credentials)) {
                return response()->json(['error' => 'Unauthorized'], 401);
            }
            return $token;
        }
    }

    public function register(RegisterRequest $request)
    {
        $email = $request->get('email');
        $name = $request->get('name');

        $user = new User();
        $user->login = $request->get('login');
        $user->email = $request->get('email');
        $user->name = $request->get('name');
        $user->surname = $request->get('surname');
        $user->region_id = $request->get('region_id');
        $user->city_id = $request->get('city_id');
        $user->address = $request->get('address');
        $user->phone_number = $request->get('phone_number');
        $user->password = Hash::make($request->get('password'));
        $user->active = false;
        $user->notification_on = true;
        $image = $request->get('profile');
        if ($image) {
            $user->image = $this->saveImgBase64($image, 'User');
        }
        $user->save();

        $user->assignRole('user', 'api');

        $verification_code = str_random(30); //Generate verification code

        DB::table('user_verifications')
            ->insert([
                'user_id' => $user->id,
                'token' => $verification_code
            ]);

        $subject = "Please verify your email address.";

        Mail::send('email.verify', ['name' => $name, 'verification_code' => $verification_code],
            function ($mail) use ($email, $name, $subject) {
                $mail->from(config('mail.username'), 'FreeStuff.am verification');
                $mail->to($email, $name);
                $mail->subject($subject);
            });

        return response()->json(['success' => true, 'message' => 'Thanks for signing up! Please check your email to complete your registration.']);
    }

    public function verify($verification_code)
    {
        $check = DB::table('user_verifications')->where('token', $verification_code)->first();
        if (!is_null($check)) {
            $user = User::find($check->user_id);
            if ($user->active == 1) {
                return response()->json([
                    'success' => true,
                    'message' => 'Account already verified..'
                ]);
            }
            $user->update(['active' => 1]);
            DB::table('user_verifications')->where('token', $verification_code)->delete();
            return response()->json([
                'success' => true,
                'message' => 'You have successfully verified your email address.'
            ]);
        }
        return response()->json(['success' => false, 'error' => "Verification code is invalid."]);
    }


    /**
     * Get a JWT via given credentials.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(LoginRequest $request)
    {
        $credentials = $request->only(['login', 'password']);
        $credentials['active'] = 1;
        if (method_exists($this, 'hasTooManyLoginAttempts') && $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            $this->sendLockoutResponse($request);
        }

        if (!$tokenResponse = auth('api')->attempt($credentials)) {
            $this->incrementLoginAttempts($request);
            $this->sendFailedLoginResponse($request);
        }

        return $tokenResponse;
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return fractal(auth('api')->user(), new UserTransformer($phone = true))->respond();
    }

    public function update(UpdateRequest $request)
    {
        auth('api')->user()->update([
            'name' => $request->get('name'),
            'surname' => $request->get('surname'),
            'region_id' => $request->get('region_id'),
            'city_id' => $request->get('city_id'),
            'address' => $request->get('address'),
            'phone_number' => $request->get('phone_number')
        ]);

        return fractal(auth('api')->user(), new UserTransformer($phone = true))->respond();
    }

    public function changePassword(ChangeOldPaswordRequest $request)
    {
        $user = auth('api')->user();

        if (Hash::check($request->get('old_password'), $user->password)) {
            $user->password = Hash::make($request->get('password'));
            $user->password_changed_at = Carbon::now();
            $user->save();
            return response()->json(['success' => 'Пароль успешно изменён']);
        } else {
            return response()->json(['error' => 'Пароль введён неправильно']);
        }
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return auth()->refresh();
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'login';
    }

    protected function saveImgBase64($param, $folder)
    {
        list($extension, $content) = explode(';', $param);
        $tmpExtension = explode('/', $extension);
        preg_match('/.([0-9]+) /', microtime(), $m);
        $fileName = sprintf('img%s%s.%s', date('YmdHis'), $m[1], $tmpExtension[1]);
        $content = explode(',', $content)[1];
        $storage = Storage::disk('public');

        $checkDirectory = $storage->exists($folder);

        if (!$checkDirectory) {
            $storage->makeDirectory($folder);
        }

        $storage->put($folder . '/' . $fileName, base64_decode($content), 'public');

        return $folder . '/' . $fileName;
    }
}

