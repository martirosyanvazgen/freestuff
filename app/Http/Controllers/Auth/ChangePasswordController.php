<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\ChangePasswordRequest;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Class ChangePasswordController
 *
 * @package App\Http\Controllers\Auth
 */
class ChangePasswordController extends Controller
{
    /**
     * @param ChangePasswordRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function process(ChangePasswordRequest $request)
    {
        $tableRow = $this->getPasswordResetTableRow($request->get('token'));
        $this->changePassword($request->get('password'), $tableRow->email, $tableRow->token);

        return \response()->json(['success' => 'Пароль успешно изменен']);
    }

    /**
     * @param string $token
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    private function getPasswordResetTableRow($token)
    {
        return DB::table('password_resets')->where('token', '=', $token)->first();
    }

    /**
     * @param string $password
     * @param string $email
     * @param string $token
     *
     * @return void
     */
    private function changePassword($password, $email, $token)
    {
        $user = User::whereEmail($email)->first();

        $user->update([
            'password' => \Hash::make($password),
            'password_changed_at' => Carbon::now(),
        ]);

        DB::table('password_resets')->where('token', '=', $token)->delete();
    }
}
