<?php

namespace App\Http\Requests\Api\Role;

use Illuminate\Foundation\Http\FormRequest;

class AttachRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check() && \Auth::user()->hasRole('super_admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'roles' => [
                'required','array'
            ],
            'roles.*' => [
                'exists:roles,name',
            ],
            'permissions' => [
                'required','array'
            ],
            'permissions.*' => [
                'exists:permissions,name',
            ],
        ];
    }
}
