<?php

namespace App\Http\Requests\Api\Category;

use App\Models\Category;
use Illuminate\Foundation\Http\FormRequest;
use Auth;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check() && Auth::user()->can('create', Category::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en' => [
                'required','max:255'
            ],
            'name_am' => [
                'required','max:255'
            ],
            'name_ru' => [
                'required','max:255'
            ],
            'slug' => [
                'required','unique:categories'
            ],
            'cover' => [
                'mimes:jpeg,jpg,png,gif','max:2048'
            ],
            'icon' => [
               'required', 'mimes:jpeg,jpg,png,gif','max:2048'
            ],
        ];
    }
}
