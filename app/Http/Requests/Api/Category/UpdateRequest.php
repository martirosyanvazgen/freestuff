<?php

namespace App\Http\Requests\Api\Category;

use App\Models\Category;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    public function authorize()
    {
        return \Auth::check() && \Auth::user()->can('update', Category::class);
    }

    public function rules()
    {
        return [
            'title_ru' => [
                'required','max:255'
            ],
            'title_am' => [
                'required','max:255'
            ],
            'slug' => [
                'required',
                Rule::unique('categories')->ignore($this->category->id)
            ],
            'parent_id' => [
                'integer','exists:categories,id',
            ],
            'cover' => [
                'mimes:jpeg,jpg,png,gif','max:2048'
            ],
        ];
    }
}
