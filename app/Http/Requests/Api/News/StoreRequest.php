<?php

namespace App\Http\Requests\Api\News;

use App\Models\News;
use Illuminate\Foundation\Http\FormRequest;
use Auth;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_en' => [
                'required','max:255'
            ],
            'title_ru' => [
                'required','max:255'
            ],
            'title_am' => [
                'required','max:255'
            ],
            'description_en' => [
                'required','max:255'
            ],
            'description_ru' => [
                'required','max:255'
            ],
            'description_am' => [
                'required','max:255'
            ],
            'text_en' => [
                'required'
            ],
            'text_ru' => [
                'required'
            ],
            'text_am' => [
                'required'
            ],
            'cover' => [
                'required',
//                'mimes:jpeg,png,jpg,gif,svg|max:2048'
            ],
        ];
    }
}
