<?php

namespace App\Http\Requests\Api\News;

use App\Models\News;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function authorize()
    {
//        return \Auth::check() && \Auth::user()->can('update', News::class);
        return true;
    }

    public function rules()
    {
        return [
            'title_en' => [
                'required','max:255'
            ],
            'title_ru' => [
                'required','max:255'
            ],
            'title_am' => [
                'required','max:255'
            ],
            'description_en' => [
                'required','max:255'
            ],
            'description_ru' => [
                'required','max:255'
            ],
            'description_am' => [
                'required','max:255'
            ],
            'text_en' => [
                'required'
            ],
            'text_ru' => [
                'required'
            ],
            'text_am' => [
                'required'
            ],
            'cover' => [
                'image','mimes:jpeg,png,jpg,gif,svg|max:2048'
            ],


        ];
    }
}
