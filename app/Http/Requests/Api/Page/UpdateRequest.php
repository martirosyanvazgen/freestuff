<?php

namespace App\Http\Requests\Api\Page;

use App\Models\Page;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest{

    public function authorize(){

        return \Auth::check() && \Auth::user()->can('update', Page::class);

    }

    public function rules(){
        return [
            'title_am' => [
                'required','max:255'
            ],
            'text_am' => [
                'required'
            ],
        ];
    }
}
