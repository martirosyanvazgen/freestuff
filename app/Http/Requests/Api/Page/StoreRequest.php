<?php

namespace App\Http\Requests\Api\Page;

use App\Models\Page;
use Illuminate\Foundation\Http\FormRequest;
use Auth;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check() && Auth::user()->can('create', Page::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_am' => [
                'required','max:255'
            ],
            'text_am' => [
                'required'
            ]
        ];
    }
}
