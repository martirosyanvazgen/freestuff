<?php

namespace App\Http\Requests\Api\Post;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check() && Auth::user()->can('update', $this->route('post'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_en' => [
                'required_without_all:title_ru,title_am', 'max:255'
            ],
            'title_ru' => [
                'required_without_all:title_am,title_en', 'max:255'
            ],
            'title_am' => [
                'required_without_all:title_en,title_ru', 'max:255'
            ],
            'description_en' => [
                'max:255'
            ],
            'description_ru' => [
                'max:255'
            ],
            'description_am' => [
                'max:255'
            ],
            'text_en' => [
                'required_without_all:text_ru,text_am'
            ],
            'text_ru' => [
                'required_without_all:text_en,text_am'
            ],
            'text_am' => [
                'required_without_all:text_en,text_ru'
            ],
            'cover' => [
                'array'
            ],
            'cover.*' => [
                'file', 'mimes:jpeg,jpg,png,gif', 'max:2048'
            ],
            'city_id' => [
                'required', 'integer', 'exists:cities,id'
            ],
            'address' => [
                'required', 'string', 'max:255'
            ],
            'status_of_stuff' => [
                'required', 'string', 'max:255'
            ],
            'category_ids' => [
                'required', 'array'
            ],
            'category_ids.*' => [
                'integer', 'exists:categories,id'
            ]
        ];
    }
}
