<?php

namespace App\Http\Requests\Api\Post;

use App\Rules\Armenian;
use App\Rules\Cyrillic;
use App\Rules\English;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_en' => [
                'required_without_all:title_ru,title_am','max:255',
                new English
            ],
            'title_ru' => [
                'required_without_all:title_am,title_en','max:255',
                new Cyrillic
            ],
            'title_am' => [
                'required_without_all:title_en,title_ru','max:255',
                new Armenian
            ],
            'description_en' => [
                'max:255',
                new English
            ],
            'description_ru' => [
                'max:255',
                new Cyrillic
            ],
            'description_am' => [
                'max:255',
                new Armenian
            ],
            'text_en' => [
                'nullable',
                new English
            ],
            'text_ru' => [
                'nullable',
                new Cyrillic
            ],
            'text_am' => [
                'nullable',
                new Armenian
            ],
            'cover' => [
                'array'
            ],
            'cover.*' => [
                'file','mimes:jpeg,jpg,png,gif','max:2048'
            ],
            'city_id' => [
                'required','integer','exists:cities,id'
            ],
            'address' => [
                'required','string','max:255'
            ],
            'status_of_stuff' => [
                'required','string','max:255'
            ],
            'detail_name' => [
                'array'
            ],
            'detail_name.*' => [
                'required_with:detail_value.*', 'max:255'
            ],
            'detail_value' => [
                'array'
            ],
            'detail_value.*' => [
                'required_with:detail_name.*', 'max:255'
            ],
            'category_ids' => [
                'required','array'
            ],
            'category_ids.*' => [
                'integer','exists:categories,id'
            ],
        ];
    }
}
