<?php

namespace App\Http\Requests\Api\Auth;

use App\Rules\RegionCity;
use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login' => [
                'required','unique:users','alpha_dash','max:255',
            ],
            'email' => [
                'required','email','unique:users','max:255'
            ],
            'password' => [
                'required','max:255'
            ],
            'name' => [
                'required','max:255'
            ],
            'surname' => [
                'required','max:255'
            ],
            'region_id' => [
                'required', 'exists:regions,id'
            ],
            'city_id' => [
                'required', 'exists:cities,id', new RegionCity(request()->region_id)
            ],
            'address' => [
                'nullable', 'string', 'max:255'
            ],
            'phone_number' => [
                'required', 'string', 'max:30'
            ],
            'cover' => [
                'file','mimes:jpeg,jpg,png,gif','max:2048'
            ]
        ];
    }
}
