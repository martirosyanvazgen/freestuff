<?php

namespace App\Http\Requests\Api\Auth;

use App\Rules\RegionCity;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:255',
            'surname' => 'required|min:3:max:255',
            'region_id' => 'required|exists:regions,id',
            'city_id' => ['required', 'exists:cities,id', new RegionCity(request()->region_id)],
            'address' => 'nullable|string|max:255',
            'phone_number' => 'required|string|max:30'
        ];
    }
}
