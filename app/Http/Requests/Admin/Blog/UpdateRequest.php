<?php

namespace App\Http\Requests\Admin\Blog;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check() && \Auth::user()->hasRole('super_admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_am' => [
                'required','max:255'
            ],
            'title_ru' => [
                'required','max:255'
            ],
            'title_en' => [
                'required','max:255'
            ],
            'description_am' => [
                'required','max:255'
            ],
            'description_ru' => [
                'required','max:255'
            ],
            'description_en' => [
                'required','max:255'
            ],
            'text_am' => [
                'required'
            ],
            'text_ru' => [
                'required'
            ],
            'text_en' => [
                'required'
            ],
            'banner' => [
                'nullable','file','mimes:jpeg,jpg,png,gif','max:2048'
            ],
            'cover' => [
                'nullable','file','mimes:jpeg,jpg,png,gif','max:2048'
            ],
        ];
    }
}
