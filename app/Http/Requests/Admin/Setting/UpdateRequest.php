<?php

namespace App\Http\Requests\Admin\Setting;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check() && \Auth::user()->hasRole('super_admin');

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'value_am' => 'required|string|max:255',
            'value_ru' => 'nullable|string|max:255',
            'value_en' => 'nullable|string|max:255',
            'key' => 'required|alpha_dash|max:50|unique:settings,key,'. $this->route("setting"),
        ];
    }
}
