<?php

namespace App\Http\Requests\Admin\User;

use App\Rules\RegionCity;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required | string | min:3 | max:20',
            'surname' => 'required | string | min:3 | max:30',
            'login' => 'required | string | min:4 | max:30 | unique:users,login,' . $this->route("user")->id,
            'email' => 'required | string | email | max:255 | unique:users,email,' . $this->route("user")->id,
            'region_id' => 'required | exists:regions,id',
            'city_id' => ['required', 'exists:cities,id', new RegionCity(request()->region_id)],
            'address' => 'nullable | string | max:255',
            'phone_number' => 'required | string | max:30',
            'image' => 'nullable | mimes:jpeg,jpg,png,gif | max:2048',
        ];
    }
}
