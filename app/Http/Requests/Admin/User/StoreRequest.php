<?php

namespace App\Http\Requests\Admin\User;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Auth;
use App\Rules\RegionCity;
class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required | string | min:3 | max:20',
            'surname' => 'required | string | min:3 | max:30',
            'login' => 'required | string | min:4 | max:30, unique:users',
            'email' => 'required | string | email | unique:users',
            'region_id' => 'required', 'exists:regions,id',
            'city_id' => ['required', 'exists:cities,id', new RegionCity(request()->region_id)],
            'address' => 'nullable | string | max:255',
            'phone_number' => 'required | string | max:30',
            'password' => 'required | confirmed |string | min:5 | max: 30',
            'image' => 'nullable | mimes:jpeg,jpg,png,gif | max:2048',
            'notification' => 'boolean'
        ];
    }
}
