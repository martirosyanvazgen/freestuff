<?php

namespace App\Http\Requests\Admin\Page;

use App\Models\Page;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest{

    public function authorize()
    {
        return \Auth::check() && \Auth::user()->hasRole('super_admin');
    }

    public function rules(){
        return [
            'title_am' => [
                'required','max:255'
            ],
            'text_am' => [
                'required'
            ],
        ];
    }
}
