<?php

namespace App\Http\Requests\Admin\FilterGroup;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Menu;
use Auth;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check() && \Auth::user()->hasRole('super_admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'key' => [
                'required','max:255','unique:filter_groups'
            ],
            'name_am' => [
                'required','max:255'
            ],
            'name_ru' => [
                'required','max:255'
            ],
            'name_en' => [
                'required','max:255'
            ],
            'value_am' => [
                'array'
            ],
            'value_ru' => [
                'array'
            ],
            'value_en' => [
                'array'
            ],
            'value_am.*' => [
                'max:255','required_with:value_ru,value_en'
            ],
            'value_ru.*' => [
                'max:255','required_with:value_am,value_en'
            ],
            'value_en.*' => [
                'max:255','required_with:value_am,value_ru'
            ],
        ];
    }
}
