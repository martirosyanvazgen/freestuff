<?php

namespace App\Http\Requests\Admin\FilterGroup;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check() && \Auth::user()->hasRole('super_admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'key' => [
                'required','max:255',
                'unique:menus,slug,' . $this->route('filter'),
            ],
            'name_am' => [
                'required','max:255'
            ],
            'name_ru' => [
                'required','max:255'
            ],
            'name_en' => [
                'required','max:255'
            ],

            //old values for filters
            'old_value_am' => [
                'array'
            ],
            'old_value_ru' => [
                'array'
            ],
            'old_value_en' => [
                'array'
            ],
            'old_value_am.*' => [
                'max:255','required_with:old_value_ru,value_en'
            ],
            'old_value_ru.*' => [
                'max:255','required_with:old_value_am,value_en'
            ],
            'old_value_en.*' => [
                'max:255','required_with:old_value_am,value_ru'
            ],

            //values for filters
            'value_am' => [
                'array'
            ],
            'value_ru' => [
                'array'
            ],
            'value_en' => [
                'array'
            ],
            'value_am.*' => [
                'max:255','required_with:value_ru,value_en'
            ],
            'value_ru.*' => [
                'max:255','required_with:value_am,value_en'
            ],
            'value_en.*' => [
                'max:255','required_with:value_am,value_ru'
            ],
        ];
    }
}
