<?php

namespace App\Http\Requests\Admin\Profile;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login' => 'required | max:30, unique:users,login,' . auth()->user()->id,
            'name' => 'required | max:20',
            'surname' => 'required | max:30',
            'email' => 'required | email | unique:users,email,' . auth()->user()->id,
        ];
    }
}
