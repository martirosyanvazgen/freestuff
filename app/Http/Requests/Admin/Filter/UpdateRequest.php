<?php

namespace App\Http\Requests\Admin\Filter;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check() && \Auth::user()->hasRole('super_admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'value_en' => [
                'required'
            ],
            'value_ru' => [
                'required'
            ],
            'value_am' => [
                'required'
            ],
            'group_id' => [
                'required','integer','exists:filter_groups,id'
            ]
        ];
    }
}
