<?php

namespace App\Http\Requests\Admin\Menu;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Menu;
use Auth;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check() && \Auth::user()->hasRole('super_admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_am' => 'required | string',
            'title_ru' => 'nullable | string',
            'title_en' => 'nullable | string',
            'slug' => 'required | alpha_dash | unique:menus',
            'order' => 'nullable | integer',
            'page_id' => 'required | exists:pages,id',
            'type' => 'required | exists:menu_types,id',
            'parent_id' => 'nullable | exists:menus,id',
        ];
    }
}
