<?php

namespace App\Http\Requests\Admin\Menu;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check() && \Auth::user()->hasRole('super_admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_en' => 'nullable | string | max:30',
            'title_am' => 'required | string | max:30',
            'title_ru' => 'nullable | string | max:30',
            'slug' => 'required | max:25 | unique:menus,slug,' . $this->route("menu"),
            'order' => 'required | int',
            'page_id' => 'required | exists:pages,id',
            'parent_id' => 'nullable | exists:menus,id',
        ];
    }
}
