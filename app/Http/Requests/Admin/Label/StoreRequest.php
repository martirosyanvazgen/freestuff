<?php

namespace App\Http\Requests\Admin\Label;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check() && \Auth::user()->hasRole('super_admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_am' => 'required|string|max:255',
            'title_ru' => 'nullable|string|max:255',
            'title_en' => 'nullable|string|max:255',
            'description_am' => 'nullable|string|max:255',
            'description_ru' => 'nullable|string|max:255',
            'description_en' => 'nullable|string|max:255',
            'key' => 'required|alpha_dash|unique:labels,key|max:50',
            'icon' => 'nullable|image|mimes:jpeg,jpg,png,gif|max:2048',
        ];
    }
}
