<?php

namespace App\Http\Requests\Admin\CategoryFilterGroup;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check() && \Auth::user()->hasRole('super_admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'filterGroups' => [
                'array'
            ],
            'filterGroups.*' => [
                'integer','exists:filter_groups,id'
            ],
        ];
    }
}
