<?php

namespace App\Http\Requests\Admin\Category;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check() && \Auth::user()->hasRole('super_admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_ru' => [
                'nullable', 'max:255'
            ],
            'name_am' => [
                'required', 'max:255'
            ],
            'name_en' => [
                'required', 'max:255'
            ],
            'slug' => [
                'required',
                Rule::unique('categories')->ignore($this->category->id)
            ],
            'parent_id' => [
                'nullable', 'integer', 'exists:categories,id',
            ],
            'icon' => [
                'nullable', 'file', 'mimes:jpeg,jpg,png,gif', 'max:2048'
            ],
            'cover' => [
                'nullable', 'file', 'mimes:jpeg,jpg,png,gif', 'max:2048'
            ],
        ];
    }
}
