<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class UserPolicy
 *
 * @package App\Policies
 */
class UserPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     *
     * @return bool|void
     */
    public function before(User $user)
    {
        if ($user->hasRole('super_admin')) {
            return true;
        }
    }

    /**
     * @param User $user
     *
     * @return void
     */
    public function create(User $user)
    {
        $user->hasPermissionTo('create_user_not_sa');
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function delete(User $user)
    {
        return $user->hasPermissionTo('delete_user');
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function activateUser(User $user)
    {
        return $user->hasPermissionTo('activate_user');
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function deactivateUser(User $user)
    {
        return $user->hasPermissionTo('deactivate_user');
    }

    /**
     * @param User $user
     * @param User $targetUser
     *
     * @return bool
     */
    public function edit(User $user, User $targetUser)
    {
        return $user->hasPermissionTo('edit_user_not_sa') && !$targetUser->hasRole('super_admin');
    }
}
