<?php

namespace App\Policies;

use App\Models\Post;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    public function before(User $user)
    {
        if ($user->hasRole('super_admin')) {
            return true;
        }
    }


    public function create(User $user)
    {
        return $user->hasPermissionTo('create_post', 'api');
    }

    public function update(User $user , Post $post)
    {
        return $user->hasPermissionTo('edit_post','api') && $this->checkUserAvailable($user,$post);
    }

    public function delete(User $user , Post $post)
    {
        return $user->hasPermissionTo('delete_post','api') && $this->checkUserAvailable($user,$post);
    }

    private function checkUserAvailable(User $user, Post $post)
    {
        return $user->id === $post->created_user_id;
    }
}
