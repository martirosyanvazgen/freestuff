<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MenuPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function before(User $user)
    {
        if ($user->hasRole('super_admin')) {
            return true;
        }
    }

    public function create(User $user)
    {
        return $user->hasPermissionTo('create_menu');
    }

    public function update(User $user)
    {
        return $user->hasPermissionTo('update_menu');
    }

    public function delete(User $user)
    {
        return $user->hasPermissionTo('delete_menu');
    }
}
