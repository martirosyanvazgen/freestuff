<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PagePolicy{
    use HandlesAuthorization;

    public function before(User $user){
        if ($user->hasRole('super_admin')) {
            return true;
        }
    }

    public function create(User $user){
        return $user->hasPermissionTo('create_page');
    }

    public function update(User $user){
        return $user->hasPermissionTo('update_page');
    }

    public function delete(User $user){
        return $user->hasPermissionTo('delete_page');
    }
}
