<?php

namespace App\Traits;

use App\Models\User;
use App\Models\Page;

/**
 * Trait ModeCreatedByUser
 *
 * @package App\Traits
 */
trait ModelCreatedByUser
{
    /**
     * @var string $createdUserIdAttribute
     */
    protected $createdUserIdAttribute = 'created_user_id';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function createdUser()
    {
        return $this->belongsTo(User::class, $this->createdUserIdAttribute);
    }
}
