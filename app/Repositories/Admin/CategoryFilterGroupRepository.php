<?php


namespace App\Repositories\Admin;

class CategoryFilterGroupRepository
{
    public function update($category,$filterGroupsIds)
    {

        $existingFilterGroups = $category->getFilterGroupsIdsAttribute()->toArray();
        $attachingIds = [];
        if (empty($existingFilterGroups)){
            $category->filterGroups()->attach($filterGroupsIds);
        }
        else if(empty($filterGroupsIds)){
            $category->filterGroups()->detach($existingFilterGroups);
        }
        else
        {
            foreach ($filterGroupsIds as $id){
                if (in_array($id, $existingFilterGroups)){
                    $key = array_search($id, $existingFilterGroups);
                    unset($existingFilterGroups[$key]);

                }else{
                    array_push($attachingIds,$id);
                }
            }
            $category->filterGroups()->attach($attachingIds);
            $category->filterGroups()->detach($existingFilterGroups);
        }


        $msg = 'Категория успешно обнавлено';

        return $msg;
    }
}
