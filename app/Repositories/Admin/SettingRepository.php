<?php


namespace App\Repositories\Admin;

use App\Models\Setting;

class SettingRepository
{
    public function store($valueAm, $valueRu, $valueEn, $key)
    {
        $setting = new Setting();

        $setting->value_am = $valueAm;
        $setting->value_ru = $valueRu;
        $setting->value_en = $valueEn;
        $setting->key = $key;

        $setting->save();

        return true;
    }

    public function update($valueAm, $valueRu, $valueEn, $id)
    {
        $setting = Setting::where("id", $id)->firstOrFail();

        $setting->value_am = $valueAm;
        $setting->value_ru = $valueRu;
        $setting->value_en = $valueEn;

        $setting->save();

        return true;
    }

    public function delete($setting)
    {
        $setting->delete();
        return redirect()->route('admin.settings');
    }
}
