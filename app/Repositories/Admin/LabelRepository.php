<?php


namespace App\Repositories\Admin;

use App\Models\Label;
use App\Repositories\UploadPhotoRepository;

class LabelRepository
{
    public function store($labelAm, $labelRu, $labelEn, $descriptionAm, $descriptionRu, $descriptionEn, $key, $icon)
    {
        $label = new Label();

        $label->title_am = $labelAm;
        $label->title_ru = $labelRu;
        $label->title_en = $labelEn;
        $label->description_am = $descriptionAm;
        $label->description_ru = $descriptionRu;
        $label->description_en = $descriptionEn;
        $label->key = $key;
        if ($icon) {
            $uploadPhotoRepository = new UploadPhotoRepository();
            $icon = $uploadPhotoRepository->store($label, $icon, $sizes_cover = [['700', '700'], ['400', '400'], ['100', '100']]);
            $label->icon = $icon['cover'];
            $label->ext = $icon['ext'];
        }
        $label->save();

        return true;
    }

    public function update($labelAm, $labelRu, $labelEn, $descriptionAm, $descriptionRu, $descriptionEn, $icon, $has_icon, $id)
    {
        $label = Label::where("id", $id)->firstOrFail();

        $label->title_am = $labelAm;
        $label->title_ru = $labelRu;
        $label->title_en = $labelEn;
        $label->description_am = $descriptionAm;
        $label->description_ru = $descriptionRu;
        $label->description_en = $descriptionEn;
        if($has_icon == 'false' && !$icon) {
            $uploadPhotoRepository = new UploadPhotoRepository();
            $icon = $uploadPhotoRepository->delete($label, $field = 'icon');
            $label->icon = null;
            $label->ext = null;
        } else if ($icon) {
            $uploadPhotoRepository = new UploadPhotoRepository();
            $icon = $uploadPhotoRepository->update($label, $icon, $field = 'icon', $field_icon = false, $sizes_cover = [['700', '700'], ['400', '400'], ['100', '100']]);
            $label->icon = $icon['cover'];
            $label->ext = $icon['ext'];
        } else if($has_icon == 'empty' && $icon) {
            $uploadPhotoRepository = new UploadPhotoRepository();
            $icon = $uploadPhotoRepository->store($label, $icon, $sizes_cover = [['700', '700'], ['400', '400'], ['100', '100']]);
            $label->icon = $icon['cover'];
            $label->ext = $icon['ext'];
        }

        $label->save();

        return true;
    }


    public function destroy($label)
    {
        if($label->icon) {
            $uploadPhotoRepository = new UploadPhotoRepository();
            $icon = $uploadPhotoRepository->delete($label, $field = 'icon');
        }

        $label->delete();
        return true;
    }
}