<?php


namespace App\Repositories\Admin;


use App\Models\Blog;
use App\Repositories\UploadPhotoRepository;

class BlogRepository
{
    /**
     * Create new category
     */
    public function store($titleEn,$titleRu,$titleAm,$descriptionEn,$descriptionRu,$descriptionAm,$textEn,$textRu,$textAm,$banner = null,$cover = null)
    {
        $blog = new Blog();

        $blog->title_en = $titleEn;
        $blog->title_ru = $titleRu;
        $blog->title_am = $titleAm;
        $blog->description_en = $descriptionEn;
        $blog->description_ru = $descriptionRu;
        $blog->description_am = $descriptionAm;
        $blog->text_en = $textEn;
        $blog->text_ru = $textRu;
        $blog->text_am = $textAm;
        $blog->save();

        if ($banner){
            $field = 'banner';
            $photoRepository = new UploadPhotoRepository();
            $photoRepository->mediaTableStore($blog,$banner,$field,true);
        }

        if ($cover){
            $field = 'cover';
            $photoRepository = new UploadPhotoRepository();
            $photoRepository->mediaTableStore($blog,$cover,$field,true);
        }

        $msg = 'блог успешно добавлено';

        return $msg;
    }
    /**
     * Update this menu
     */
    public function update($blog,$titleEn,$titleRu,$titleAm,$descriptionEn,$descriptionRu,$descriptionAm,$textEn,$textRu,$textAm,$banner = null,$cover = null)
    {
        $blog->title_en = $titleEn;
        $blog->title_ru = $titleRu;
        $blog->title_am = $titleAm;
        $blog->description_en = $descriptionEn;
        $blog->description_ru = $descriptionRu;
        $blog->description_am = $descriptionAm;
        $blog->text_en = $textEn;
        $blog->text_ru = $textRu;
        $blog->text_am = $textAm;
        $blog->save();

        if ($banner){
            $field = 'banner';
            $photoRepository = new UploadPhotoRepository();
            $photoRepository->mediaTableUpdate($blog,$banner,$field,true);
        }

        if ($cover){
            $field = 'cover';
            $photoRepository = new UploadPhotoRepository();
            $photoRepository->mediaTableUpdate($blog,$cover,$field,true);
        }

        $msg = 'блог успешно обнавлено';

        return $msg;
    }

    public function delete($blog){
        $msg = 'Блог успешно удалено';
        if ($blog->media->count()){
            $photoRepository = new UploadPhotoRepository();
            $photoRepository->deleteMediaTable($blog,$blog->media);
        }
        $blog->delete();

        return $msg;
    }
}
