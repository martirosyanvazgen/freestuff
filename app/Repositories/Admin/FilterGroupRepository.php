<?php


namespace App\Repositories\Admin;


use App\Models\Filter;
use App\Models\FilterGroup;
use Illuminate\Support\Facades\Config;

class FilterGroupRepository
{

    public function store($key,$nameEn,$nameRu,$nameAm,$valuesAm = [],$valuesRu = [],$valuesEn = [])
    {
        $filterGroup = new FilterGroup();

        $filterGroup->key = $key;
        $filterGroup->name_en = $nameEn;
        $filterGroup->name_ru = $nameRu;
        $filterGroup->name_am = $nameAm;

        $filterGroup->save();
        if (!empty($valuesAm)) {
            $count = count($valuesAm);
            $data = [];
            for ($i = 0; $i < $count; $i++) {
                $data[] = [
                    'value_am' => $valuesAm[$i],
                    'value_ru' => $valuesRu[$i],
                    'value_en' => $valuesEn[$i],
                    'group_id' => $filterGroup->id
                ];
            }
            Filter::insert($data);
        }
        $msg = 'Группа Фильтров успешно добавлено';

        return $msg;

     }

    public function update($filterGroup,$key,$nameEn,$nameRu,$nameAm,$oldValuesAm = [],$oldValuesRu = [],$oldValuesEn = [],$valuesAm = [],$valuesRu = [],$valuesEn = [])
    {
        $filterGroup->key = $key;
        $filterGroup->name_en = $nameEn;
        $filterGroup->name_ru = $nameRu;
        $filterGroup->name_am = $nameAm;

        $filterGroup->save();
        //get filter group saved filter ids
        $savedFilterIds = $filterGroup->getFilterIds()->toArray();
        foreach ($oldValuesAm as $k => $oldValueAM){
            if(in_array($k,$savedFilterIds)){
                $filterGroup->filters()->where('id',$k)->update([
                    'value_am' =>  $oldValuesAm[$k],
                    'value_ru' =>  $oldValuesRu[$k],
                    'value_en' =>  $oldValuesEn[$k]
                ]);
                $key = array_search($k, $savedFilterIds);
                unset($savedFilterIds[$key]);
            }
        }
        //if array has elements
        if (!empty($savedFilterIds)){
            $filterGroup->filters()->whereIn('id',$savedFilterIds)->delete();
        }

        if (!empty($valuesAm)) {
            $count = count($valuesAm);
            $data = [];
            for ($i = 0; $i < $count; $i++) {
                $data[] = [
                    'value_am' => $valuesAm[$i],
                    'value_ru' => $valuesRu[$i],
                    'value_en' => $valuesEn[$i],
                    'group_id' => $filterGroup->id
                ];
            }
            Filter::insert($data);
        }

        $msg = 'Группа Фильтров успешно обнавлено';

        return $msg;
    }

    public function delete($filterGroup){

        $msg = 'Группа Фильтров успешно удалено';

        $filterGroup->delete();

        return $msg;
    }
}
