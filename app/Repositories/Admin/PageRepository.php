<?php

namespace App\Repositories\Admin;

use App\Models\Page;

class PageRepository
{
    public function get()
    {
        $page = Page::orderBy('id', 'desc')->paginate(10);
        return $page;
    }

    public function store($titleEn, $titleAm, $titleRu, $textEn, $textAm, $textRu){
        $page = new Page();
        $page->title_en = $titleEn;
        $page->title_am = $titleAm;
        $page->title_ru = $titleRu;
        $page->text_en = $textEn;
        $page->text_am = $textAm;
        $page->text_ru = $textRu;
        $page->save();
        return $page;
    }
    public function delete($page){
        $page->delete();
        return redirect()->route('admin.pages');
    }
}
