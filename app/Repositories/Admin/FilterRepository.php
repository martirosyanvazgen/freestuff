<?php


namespace App\Repositories\Admin;


use App\Models\Filter;
use Illuminate\Support\Facades\Config;

class FilterRepository
{
    /**
     * Create new category
     */
    public function store($valueEn,$valueRu,$valueAm,$groupId)
    {
        $filter = new Filter();

        $filter->value_en = $valueEn;
        $filter->value_ru = $valueRu;
        $filter->value_am = $valueAm;
        $filter->group_id = $groupId;

        $filter->save();

        $msg = 'Фильтр успешно добавлено';

        return $msg;
    }
    /**
     * Update this menu
     */
    public function update($filter,$valueEn,$valueRu,$valueAm,$groupId)
    {
        $filter->value_en = $valueEn;
        $filter->value_ru = $valueRu;
        $filter->value_am = $valueAm;
        $filter->group_id = $groupId;

        $filter->save();

        $msg = 'Фильтр успешно обнавлено';

        return $msg;
    }

    public function delete($filter){
        $msg = 'Фильтр успешно удалено';

        $filter->delete();

        return $msg;
    }
}
