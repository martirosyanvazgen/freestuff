<?php


namespace App\Repositories\Admin;


use App\Models\Category;
use App\Repositories\UploadPhotoRepository;

class CategoryRepository{

    public function get(){
        $categories = Category::orderBy('created_at', 'desc')->paginate(10);
        return $categories;
    }

    public function create(){
        $categories = Category::all();
        return $categories;
    }

    public function store($nameEn,$nameAm,$nameRu,$slug,$icon,$cover){

        $category = new Category();
        $category->name_en = $nameEn;
        $category->name_am = $nameAm;
        $category->name_ru = $nameRu;
        $category->slug    = $slug;
        $categoryRepository = new UploadPhotoRepository();
        $icon = $categoryRepository->store($category, $icon, $sizes_cover = false, $sizes_icon = ['50', '50']);
        $category->icon = $icon;
        if ($cover){
            $uploadPhotoRepository = new UploadPhotoRepository();
            $cover = $uploadPhotoRepository->store($category, $cover, $sizes_cover = [['700', '700'], ['400', '400'], ['300', '300']]);
            $category->cover = $cover['cover'];
            $category->ext = $cover['ext'];

        }
        $category->save();
        return $category;
    }

    public function edit($id){
        $categories = Category::where('id',$id)->firstOrfail();
        return $categories;
    }

    public function update($nameEn,$nameAm,$nameRu,$slug,$parentId,$oldCover,$cover,$oldIcon,$icon,$category)
    {
        $category->name_en = $nameEn;
        $category->name_am = $nameAm;
        $category->name_ru = $nameRu;
        $category->slug = $slug;
        $category->parent_id = $parentId;

        if($oldCover != "true" && $cover != null)
        {
            $categoryRepository = new UploadPhotoRepository();
            $cover = $categoryRepository->update($category, $cover, $field = 'cover', $icon = false, $sizes = [['700', '700'], ['400', '400'], ['300', '300']]);
            $category->cover = $cover['cover'];
            $category->ext = $cover['ext'];
        }else if($oldCover != "true" && $cover == null){
            $categoryRepository = new UploadPhotoRepository();
            $cover = $categoryRepository->delete($category, $cover = 'cover');
            $category->cover = null;
            $category->ext = null;
        }

        if($icon != null)
        {
            $categoryRepository = new UploadPhotoRepository();
            $icon = $categoryRepository->update($category, $icon, $field = false, $icon = 'icon',$sizes = ['50', '50']);
            $category->icon = $icon;
        }

        $category->save();

        return $category;
    }

    public function destroy($category)
    {
        $childCategory = Category::where("parent_id", $category['id'])->first();
        if($childCategory){
            $childCategory->parent_id = null;
            $childCategory->save();
        }
        if($category['cover']){
            $categoryRepository = new UploadPhotoRepository();
            $cover = $categoryRepository->delete($category, $cover = 'cover');
        }

        if($category['icon']){
            $categoryRepository = new UploadPhotoRepository();
            $icon = $categoryRepository->delete($category, $cover = false, $field_icon = 'icon');
        }
        $category->delete();

        return true;
    }

}