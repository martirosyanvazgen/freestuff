<?php

namespace App\Repositories\Admin;

use App\Models\User;
use App\Repositories\UploadPhotoRepository;
use Illuminate\Support\Facades\Hash;

class UserRepository
{
    public function store($name, $surname, $login, $email, $region_id, $city_id, $address, $phone_number, $password, $image, $notification)
    {
        $user = new User();

        $user->name = $name;
        $user->surname = $surname;
        $user->login = $login;
        $user->email = $email;
        if($region_id == 0){
            $user->region_id = null;
        } else {
            $user->region_id = $region_id;
        }
        $user->city_id = $city_id;
        $user->address = $address;
        $user->phone_number = $phone_number;
        $user->password = Hash::make($password);
        $user->active = 0;
        $user->notification_on = $notification;
        if ($image) {
            $uploadPhotoRepository = new UploadPhotoRepository();
            $image = $uploadPhotoRepository->store($user, $image, $sizes_cover = false, $sizes_icon = ['150', '100']);
            $user->image = $image;
        }

        $user->save();
        $user->assignRole('user', 'api');
        return true;
    }

    public function update($name, $surname, $login, $email, $region_id, $city_id, $address, $phone_number, $image, $notification, $user)
    {
        $user->name = $name;
        $user->surname = $surname;
        $user->login = $login;
        $user->email = $email;
        if($region_id == 0){
            $user->region_id = null;
        } else {
            $user->region_id = $region_id;
        }
        $user->city_id = $city_id;
        $user->address = $address;
        $user->phone_number = $phone_number;
        if($notification){
            $user->notification_on = true;
        } else {
            $user->notification_on = false;
        }
        if ($user->image && $image != null) {
            $uploadPhotoRepository = new UploadPhotoRepository();
            $createdImage = $uploadPhotoRepository->update($user, $image, $field = false, $field = 'image', $sizes = ['150', '100']);
            $deletedImage = $uploadPhotoRepository->delete($user, $field = false, $image = 'image');
            $user->image = $createdImage;
        } else if ($user->image && $image == null) {
            $uploadPhotoRepository = new UploadPhotoRepository();
            $deletedImage = $uploadPhotoRepository->delete($user, $field = false, $image = 'image');
            $user->image = null;
        } else if(!$user->image && $image != null) {
            $uploadPhotoRepository = new UploadPhotoRepository();
            $image = $uploadPhotoRepository->store($user, $image, $sizes_cover = false, $sizes_icon = ['150', '100']);
            $user->image = $image;
        }

        $user->save();
        return true;
    }

    public function destroy($user)
    {
        $uploadPhotoRepository = new UploadPhotoRepository();
        $image = $uploadPhotoRepository->delete($user, $field = false, $fieldImage = 'image');
        return true;

    }

}