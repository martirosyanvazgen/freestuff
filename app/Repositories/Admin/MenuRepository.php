<?php

namespace App\Repositories\Admin;

use App\Models\Menu;

class MenuRepository
{
    public function store($title_en, $title_am, $title_ru, $slug, $order, $page_id, $parent_id, $type)
    {
        $menu = new Menu();

        if($order != null){
            $menu->order = $order;
        }

        $menu->title_en = $title_en;
        $menu->title_am = $title_am;
        $menu->title_ru = $title_ru;
        $menu->slug = $slug;
        $menu->page_id = $page_id;
        $menu->parent_id = $parent_id;
        $menu->type = $type;

        $menu->save();
        return $menu;
    }

    public function update($title_en, $title_am, $title_ru, $parent_id, $order, $slug, $page_id, $id)
    {
        $menu = Menu::find($id);

        $menu->title_en = $title_en;
        $menu->title_am = $title_am;
        $menu->title_ru = $title_ru;
        $menu->parent_id = $parent_id;
        $menu->order = $order;
        $menu->slug = $slug;
        $menu->page_id = $page_id;

        $menu->save();
        return $menu;
    }
}