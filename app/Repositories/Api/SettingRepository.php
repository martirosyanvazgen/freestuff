<?php


namespace App\Repositories\Api;

use App\Models\Setting;

class SettingRepository
{
    public function get()
    {
        return Setting::orderBy('id', 'desc')->paginate(10);
    }

}
