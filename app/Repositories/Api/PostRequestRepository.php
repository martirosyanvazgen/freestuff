<?php

namespace App\Repositories\Api;

use App\Models\Post;
use App\Models\PostRequest;
use App\Repositories\Api\NotificationRepository;

class PostRequestRepository
{
    public function store($requestText, $postId)
    {
        $newPostRequest = new PostRequest();

        $newPostRequest->request_text = $requestText;
        $newPostRequest->post_id = $postId;
        $newPostRequest->created_user_id = \Auth::id();

        $newPostRequest->save();
        $post = Post::where("id", $postId)->firstOrFail();
        $notificationRepository = new NotificationRepository();
        $notification = $notificationRepository->store(
            $post,
            $newPostRequest,
            $post->id,
            $request = true
        );
        return $newPostRequest;
    }

}