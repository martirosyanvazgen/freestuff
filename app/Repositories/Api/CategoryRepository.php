<?php


namespace App\Repositories\Api;


use App\Models\Category;
use App\Repositories\UploadPhotoRepository;

class CategoryRepository{

    public function get(){
//        $categories = Category::where('parent_id',null)->with('child')->get();
        $categories = Category::where('parent_id',null)->with('child')->paginate(15);
        return $categories;
    }

    public function create(){
        $categories = Category::all();
        return $categories;
    }

    public function store($nameEN,$nameAm,$nameRu,$slug,$icon,$cover){

        $category = new Category();
        $category->name_en = $nameEN;
        $category->name_am = $nameAm;
        $category->name_ru = $nameRu;
        $category->slug    = $slug;
        $categoryRepository = new UploadPhotoRepository();
        $icon = $categoryRepository->store($category,$icon, $sizes = false, $icon = ["50", "50"]);
        $category->icon = $icon;
        if ($cover){
            $categoryRepository = new UploadPhotoRepository();
            $cover = $categoryRepository->store($category,$cover,$sizes = [['700', '700'], ['400', '400'], ['300', '300']]);
            $category->cover = $cover['cover'];
            $category->ext = $cover['ext'];

        }
        $category->save();
        return $category;
    }

    public function edit($id){
        $categories = Category::where('id',$id)->firstOrfail();
        return $categories;
    }
}
