<?php

namespace App\Repositories\Api;

use App\Models\Menu;

class MenuRepository
{
    public function get()
    {
        return Menu::where('parent_id', null)->with('child')->orderBy('order')->get();
    }

    public function show($slug)
    {
        return Menu::where("slug", $slug)->first();
    }
}