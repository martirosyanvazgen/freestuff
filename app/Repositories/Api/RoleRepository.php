<?php


namespace App\Repositories\Api;

use Carbon\Carbon;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;

class RoleRepository
{

    public function store($name){

        $permission = new Permission();

        $permission->name = $name;
        $permission->created_at = Carbon::now();
        $permission->updated_at = Carbon::now();
        $permission->guard_name = 'api';

        $permission->save();

        return $permission;
    }

    public function attach($roles , $permissions){
        foreach ($roles as $role){
            $roleItem = Role::where('name',$role)->first();

            $roleItem->givePermissionTo(
                $permissions
            );
        }
        $rolesQuery = Role::whereIn('name',$roles)->get();
        return $rolesQuery;
    }

    public function detach($roles , $permissions){
        foreach ($roles as $role){
            $roleItem = Role::where('name',$role)->first();

            $roleItem->revokePermissionTo(
                $permissions
            );
        }
        $rolesQuery = Role::whereIn('name',$roles)->get();
        return $rolesQuery;
    }

    public function delete($id){
        $delete = DB::table('permissions')->delete($id);
        if ($delete == true){
            $status = 'success';
        }else{
            $status = 'error';
        }

        return $status;
    }
}