<?php


namespace App\Repositories\Api;
use App\Models\Filter;
use App\Models\CategoryFilterGroup;

class FilterRepository
{
    public function get($category_id)
    {
        $gategor_filter_groups = CategoryFilterGroup::where("category_id", $category_id)->with("filterGroup.filters")->get();
        return $gategor_filter_groups;
    }
}