<?php


namespace App\Repositories\Api;

use App\Models\Label;

class LabelRepository
{
    public function get()
    {
        return Label::orderBy("id", "desc")->paginate(10);
    }

    public function show($keys)
    {
        return Label::whereIn("key", $keys)->get();
    }
}
