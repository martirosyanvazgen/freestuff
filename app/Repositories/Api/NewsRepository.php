<?php

namespace App\Repositories\Api;

use App\Models\News;

class NewsRepository{

    public function get(){
        $pages = News::query()->paginate();
        return $pages;
    }

    public function store($titleEn,$titleRu,$titleAm,$descriptionEn,$descriptionRu,$descriptionAm,$textEn,$textRu,$textAm,$cover){

        $news = new News();
        $news->title_en = $titleEn;
        $news->title_ru = $titleRu;
        $news->title_am = $titleAm;
        $news->description_en = $descriptionEn;
        $news->description_ru = $descriptionRu;
        $news->description_am = $descriptionAm;
        $news->text_en = $textEn;
        $news->text_ru = $textRu;
        $news->text_am = $textAm;
        $news->cover = $cover;
        $news->save();

        return $news;
    }
}
