<?php


namespace App\Repositories\Api;

use App\Models\Blog;

class BlogRepository
{
    public function get()
    {
        $blogs = Blog::orderBy("id", "DESC")->with("cover", "media")->get();
        return $blogs;
    }

    public function show($id)
    {
        $blogs = Blog::where("id", $id)->with("media")->first();
        return $blogs;
    }
}