<?php


namespace App\Repositories\Api;

use App\Models\Post;
use App\Models\PostDetail;
use App\Models\PostCategory;
use App\Repositories\UploadPhotoRepository;

class PostRepository
{
    public $imageField = 'cover';

    public function get($createdUserId, $categoryId)
    {
        if ($createdUserId && !$categoryId) {
            $posts = Post::where("given", false)->where("created_user_id", $createdUserId)->with("createdUser")->get();
        } else if (!$createdUserId && $categoryId) {
            $posts = Post::where("given", false)->findByCategoryId($categoryId)->with("createdUser")->get();
        } else if ($createdUserId && $categoryId) {
            $posts = Post::where("given", false)->where("created_user_id", $createdUserId)->findByCategoryId($categoryId)->with("createdUser")->get();
        } else {
            $posts = Post::where("given", false)->with("createdUser")->get();
        }
        return $posts;
    }

    /**
     * @param null $titleEn
     * @param null $titleRu
     * @param null $titleAm
     * @param null $descriptionEn
     * @param null $descriptionRu
     * @param null $descriptionAm
     * @param null $textEn
     * @param null $textRu
     * @param null $textAm
     * @param $cityId
     * @param $address
     * @param $cover
     * @param $statusOfStuff
     * @param array $detailNames
     * @param array $detailValues
     * @return Post
     */
    public function store($titleEn, $titleRu, $titleAm,
                          $descriptionEn, $descriptionRu, $descriptionAm,
                          $textEn, $textRu, $textAm,
                          $cityId, $address,
                          $cover,
                          $statusOfStuff,
                          $categoryIds
    )
    {
        $post = new Post();

        $post->title_en = $titleEn;
        $post->title_ru = $titleRu;
        $post->title_am = $titleAm;
        $post->description_en = $descriptionEn;
        $post->description_ru = $descriptionRu;
        $post->description_am = $descriptionAm;
        $post->text_en = $textEn;
        $post->text_ru = $textRu;
        $post->text_am = $textAm;
        $post->city_id = $cityId;
        $post->address = $address;
        $post->status_of_stuff = $statusOfStuff;
        $post->created_user_id = \Auth::id();

        $post->save();
        $post->category()->attach($categoryIds);

        if ($cover) {
            $photoRepository = new UploadPhotoRepository();
            $photoRepository->mediaTableStore($post, $cover, false, true);
        }

        return $post;
    }

    /**
     * @param $post
     * @param null $titleEn
     * @param null $titleRu
     * @param null $titleAm
     * @param null $descriptionEn
     * @param null $descriptionRu
     * @param null $descriptionAm
     * @param null $textEn
     * @param null $textRu
     * @param null $textAm
     * @param $cover
     * @param $isActive
     * @return mixed
     */
    public function update($postUpdate,
                           $titleEn, $titleRu, $titleAm,
                           $descriptionEn, $descriptionRu, $descriptionAm,
                           $textEn, $textRu, $textAm,
                           $cityId,
                           $address,
                           $cover,
                           $statusOfStuff,
                           $categoryIds
    )
    {
        $postUpdate->title_en = $titleEn;
        $postUpdate->title_ru = $titleRu;
        $postUpdate->title_am = $titleAm;
        $postUpdate->description_en = $descriptionEn;
        $postUpdate->description_ru = $descriptionRu;
        $postUpdate->description_am = $descriptionAm;
        $postUpdate->text_en = $textEn;
        $postUpdate->text_ru = $textRu;
        $postUpdate->text_am = $textAm;
        $postUpdate->city_id = $cityId;
        $postUpdate->address = $address;

        $exsistingCategories = $postUpdate->getCategoryIds();
        $postUpdate->category()->detach($exsistingCategories);
        $postUpdate->category()->attach($categoryIds);

        if ($cover) {
            $photoRepository = new UploadPhotoRepository();
            $photoRepository->mediaTableUpdate($postUpdate, $cover, false, true);
        } else {
            $photoRepository = new UploadPhotoRepository();
            $photoRepository->mediaTableUpdate($postUpdate, $cover = [], false, true);
        }

        $postUpdate->status_of_stuff = $statusOfStuff;
        $postUpdate->created_user_id = \Auth::id();

        $postUpdate->save();

        return $postUpdate;
    }

    public function delete($post)
    {
        $media = $post->media;
        if ($media != null) {
            $photoRepository = new UploadPhotoRepository();
            $post->cover = $photoRepository->deleteMediaTable($post, $media);
        }
        $post->delete();

        return 'success';
    }

}
