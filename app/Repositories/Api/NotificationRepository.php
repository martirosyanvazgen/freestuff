<?php


namespace App\Repositories\Api;

use App\Models\Notification;
use App\Notifications\BidRequestConfirm;

class NotificationRepository
{
    public function get()
    {
        $auth_id = auth('api')->user()->id;
        return Notification::where("user_id", $auth_id)->where("read", false)->get();
    }

    public function store($notificationUser, $newPostRequest, $postId, $request = false, $approved = false, $canceled = false, $read = false)
    {

        $notification = new Notification();

        $notification->user_id = $notificationUser->created_user_id;
        if ($newPostRequest != null) {
            $notification->request_id = $newPostRequest->id;
        }
        $notification->post_id = $postId;
        $notification->request = $request;
        $notification->approved = $approved;
        $notification->canceled = $canceled;
        $notification->read = $read;

        $notification->save();
        $notificationUser->user->notify(new BidRequestConfirm);

        return true;
    }

    public function show($id)
    {
        $auth_id = auth('api')->user()->id;
        $notification = Notification::where("id", $id)->where("user_id", $auth_id)->with("post","postRequest")->first();
        return $notification;
    }
}