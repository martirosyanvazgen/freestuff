<?php


namespace App\Repositories\Api;

use App\Models\Post;

class SearchRepository
{
    public function getResult($term,$lang)
    {
        $search = Post::where('given',false)
            ->where('title_' . $lang, 'like', '%' . "$term" . '%')
            ->orWhere('description_' . $lang, 'like', '%' . "$term" . '%')
            ->orWhere('text_' . $lang, 'like', '%' . "$term" . '%')
            ->with('firstMedia')
            ->with('details')
            ->orderBy('created_at','DESC')
            ->paginate(10);

        return $search;
    }
}