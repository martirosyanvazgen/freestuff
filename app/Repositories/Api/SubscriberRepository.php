<?php


namespace App\Repositories\Api;
use App\Models\Subscriber;


class SubscriberRepository
{
    public function store($request)
    {
        $subscriber = new Subscriber();
        $subscriber->email = $request->email;
        $subscriber->category_id = $request->category_id;
        $subscriber->save();
        return $subscriber;
    }
}