<?php


namespace App\Repositories;

use App\Models\Media;
use Image;
use File;

class UploadPhotoRepository
{

    public function store($model, $file ,$sizes_cover = false, $sizes_icon = false)
    {
        $storage_path = storage_path('app/public');
        $path = class_basename($model);

        if(!File::exists($storage_path.'/'.$path)) {
            File::makeDirectory($storage_path.'/'.$path,0777,true);
        }

        $fileName = $file->getClientOriginalName();
        $ext = $file->getClientOriginalExtension();
        $nameHash = str_random(32);

        if($sizes_icon){
            $nameWithExt = $nameHash . '.' . $ext;
            $source = $storage_path .'/'. $path . '/' .$nameWithExt;
            Image::make($file->getRealPath())->fit($sizes_icon[0], $sizes_icon[1])->save( $source );
            return $path . "/" . $nameWithExt;
        }else if ($sizes_cover){
            if($sizes_cover === true){
                $sizes = [
                    ['700', '700'],
                    ['400', '400'],
                    ['100', '100']
                ];
            }else{
                $sizes = $sizes_cover;
            }
            $sizeTypes = ['large', 'medium', 'small'];

            foreach ($sizeTypes as $key => $type) {
                $nameWithExt = $nameHash . '_' . $type. '.' . $ext;
                $source = $storage_path .'/'. $path . '/' .$nameWithExt;
                $info = getimagesize($file->getRealPath());
                if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($file->getRealPath());
                elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($file->getRealPath());
                elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($file->getRealPath());

                imagejpeg($image, $source,70);

                Image::make($file->getRealPath())->resize($sizes[$key][0], $sizes[$key][1])->save( $source );
            }
            $coverField =   $path.'/'.$nameHash;
        }else{
            $nameWithExt = $nameHash . "." . $ext;
            $source = $storage_path .'/'. $path . '/' .$nameWithExt;
            Image::make($file)->save( $source );

            $coverField =   $path.'/'.$nameHash;
        }

            return ['cover' =>$coverField , 'ext' => $ext ];

    }

    public function update($model , $file , $field = false , $field_icon = false, $sizes_cover = false)
    {
        $storage_path = storage_path('app/public');
        $path = class_basename($model);
        if(!File::exists($storage_path.'/'.$path))


        File::makeDirectory($storage_path.'/'.$path,0777,true);
        if ($model->$field != null)
            $oldImage = $model->$field;

        $fileName = $file->getClientOriginalName();
        $ext = $file->getClientOriginalExtension();
        $nameHash = str_random(32);

        if($field_icon != false) {
            $nameWithExt = $nameHash . '.' . $ext;
            $oldImage = $model['icon'];
            $source = $storage_path .'/' . $path . '/' .$nameWithExt;
            if($field_icon === true){
                $sizes = ['20', '20'];
            }else{
                $sizes = $sizes_cover;
            }

            Image::make($file->getRealPath())->resize($sizes[0], $sizes[1])->save( $source );

            if (isset($oldImage)){
                $oldImagePath = $storage_path . '/' .$oldImage;
                if (File::exists($oldImagePath )) {
                    unlink($oldImagePath);
                }
            }

            return $path . "/" . $nameWithExt;
        }else if ($sizes_cover){
            if($sizes_cover === true){
                $sizes = [
                    ['700', '700'],
                    ['400', '400'],
                    ['100', '100']
                ];
            }else{
                $sizes = $sizes_cover;
            }
            $sizeTypes = ['large', 'medium', 'small'];

            foreach ($sizeTypes as $key => $type) {
                $nameWithExt = $nameHash . '_' . $type. '.' . $ext;
                $source = $storage_path .'/'. $path . '/' .$nameWithExt;
                $info = getimagesize($file->getRealPath());
                if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($file->getRealPath());
                elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($file->getRealPath());
                elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($file->getRealPath());

                imagejpeg($image, $source,70);

                Image::make($file->getRealPath())->resize($sizes[$key][0], $sizes[$key][1])->save( $source );

                if (isset($oldImage)){
                    $oldImagePath = $storage_path . '/' .$oldImage .'_' . $type. '.' . $model['ext'];
                    if (File::exists($oldImagePath )) {
                        unlink($oldImagePath);
                    }
                }
            }
            $coverField =   $path.'/'.$nameHash;
        }else{
            $nameWithExt = $nameHash . $ext;
            $source = $storage_path .'/'. $path . '/' .$nameWithExt;
            Image::make($file)->save( $source );

            if (isset($oldImage)){
                $oldImagePath = $storage_path . '/' .$oldImage .$ext;
                if (File::exists($oldImagePath )) {
                    unlink($oldImagePath);
                }
            }

            $coverField =   $path.'/'.$nameHash;
        }

        return ['cover' => $coverField , 'ext' => $ext ];


    }

    public function mediaTableStore($model, $file ,$field = false,$sizes = false){
        $storage_path = storage_path('app/public');
        $path = class_basename($model);
        $sizeTypes = ['large', 'medium', 'small'];
        if(!File::exists($storage_path.'/'.$path)) {
            File::makeDirectory($storage_path.'/'.$path,0777,true);
        }
        if(is_array($file)) {
            $files = $file;
            foreach ($files as $file) {
                $file->getClientOriginalName();
                $ext = $file->getClientOriginalExtension();
                $nameHash = str_random(32);
                if ($sizes) {
                    $sizes = [['700', '700'], ['400', '400'], ['100', '100']];
                    foreach ($sizeTypes as $key => $type) {
                        $nameWithExt = $nameHash . '_' . $type . '.' . $ext;
                        $source = $storage_path . '/' . $path . '/' . $nameWithExt;
                        $info = getimagesize($file->getRealPath());
                        if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($file->getRealPath());
                        elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($file->getRealPath());
                        elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($file->getRealPath());

                        imagejpeg($image, $source, 70);
                        Image::make($file->getRealPath())->resize($sizes[$key][0], $sizes[$key][1])->save($source);
                    }
                    $coverField = $path . '/' . $nameHash;
                } else {
                    $nameWithExt = $nameHash . $ext;
                    $source = $storage_path . '/' . $path . '/' . $nameWithExt;
                    Image::make($file)->save($source);
                    $coverField = $path . '/' . $nameHash;
                }
                $media = new Media();
                $media->cover = $coverField;
                $media->ext = $ext;
                if ($path == 'Blog'){
                    $media->type = $path.'/'.$field;
                }else{
                    $media->type = $path;
                }
                $media->type_id = $model->id;
                if ($sizes){
                    $media->sizes = true;
                }
                $media->save();
            }
        }else{
            $file->getClientOriginalName();
            $ext = $file->getClientOriginalExtension();
            $nameHash = str_random(32);
            if ($sizes) {
                $sizes = [['700', '700'], ['400', '400'], ['100', '100']];
                foreach ($sizeTypes as $key => $type) {
                    $nameWithExt = $nameHash . '_' . $type . '.' . $ext;
                    $source = $storage_path . '/' . $path . '/' . $nameWithExt;
                    $info = getimagesize($file->getRealPath());
                    if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($file->getRealPath());
                    elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($file->getRealPath());
                    elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($file->getRealPath());

                    imagejpeg($image, $source, 70);
                    Image::make($file->getRealPath())->resize($sizes[$key][0], $sizes[$key][1])->save($source);
                }
                $coverField = $path . '/' . $nameHash;
            } else {
                $nameWithExt = $nameHash . $ext;
                $source = $storage_path . '/' . $path . '/' . $nameWithExt;
                Image::make($file)->save($source);
                $coverField = $path . '/' . $nameHash;
            }
            $media = new Media();
            $media->cover = $coverField;
            $media->ext = $ext;
            if ($path == 'Blog'){
                $media->type = $path.'/'.$field;
            }else{
                $media->type = $path;
            }
            $media->type_id = $model->id;
            if ($sizes){
                $media->sizes = true;
            }
            $media->save();
        }

        return true;
    }

    public function mediaTableUpdate($model, $file ,$field = null,$sizes = false){
        $storage_path = storage_path('app/public');
        $path = class_basename($model);
        $sizeTypes = ['large', 'medium', 'small'];
        if(!File::exists($storage_path.'/'.$path)) {
            File::makeDirectory($storage_path.'/'.$path,0777,true);
        }
        if ($field) {
            $existingMedia = Media::where('type', $path . '/' . $field)->where('type_id', $model->getKey())->get();
        } else {
            $existingMedia = Media::where('type', $path)->where('type_id', $model->getKey())->get();
        }
        if(is_array($file)) {
            $files = $file;
            if ($existingMedia){
                foreach ($existingMedia as $itemMedia) {

                    if ($itemMedia['sizes'] == false) {
                        $oldImagePath = $storage_path . '/' . $itemMedia['cover'] . '.' . $itemMedia['ext'];
                        if (File::exists($oldImagePath)) {
                            unlink($oldImagePath);
                        }
                    } else {
                        foreach ($sizeTypes as $type) {
                            $oldImagePath = $storage_path . '/' . $itemMedia['cover'] . '_' . $type . '.' . $itemMedia['ext'];
                            if (File::exists($oldImagePath)) {
                                unlink($oldImagePath);
                            }
                        }
                    }
                }
                if ($field) {
                    Media::where('type', $path . '/' . $field)->where('type_id', $model->getKey())->delete();
                } else {
                    Media::where('type', $path)->where('type_id', $model->getKey())->delete();
                }
            }
            foreach ($files as $file) {
                $fileName = $file->getClientOriginalName();
                $ext = $file->getClientOriginalExtension();
                $nameHash = str_random(32);
                if ($sizes) {
                    $sizes = [['700', '700'], ['400', '400'], ['100', '100']];
                    foreach ($sizeTypes as $key => $type) {
                        $nameWithExt = $nameHash . '_' . $type . '.' . $ext;
                        $source = $storage_path . '/' . $path . '/' . $nameWithExt;
                        $info = getimagesize($file->getRealPath());
                        if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($file->getRealPath());
                        elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($file->getRealPath());
                        elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($file->getRealPath());

                        imagejpeg($image, $source, 70);
                        Image::make($file->getRealPath())->resize($sizes[$key][0], $sizes[$key][1])->save($source);
                    }
                    $coverField = $path . '/' . $nameHash;
                } else {
                    $nameWithExt = $nameHash . $ext;
                    $source = $storage_path . '/' . $path . '/' . $nameWithExt;
                    Image::make($file)->save($source);
                    $coverField = $path . '/' . $nameHash;
                }
                $media = new Media();
                $media->cover = $coverField;
                $media->ext = $ext;
                if ($field){
                    $media->type = $path.'/'.$field;
                }else{
                    $media->type = $path;
                }
                $media->type_id = $model->id;
                if ($sizes){
                    $media->sizes = true;
                }
                $media->save();
            }
        }else{
            $fileName = $file->getClientOriginalName();
            $ext = $file->getClientOriginalExtension();
            $nameHash = str_random(32);
            if ($sizes) {
                $sizes = [['700', '700'], ['400', '400'], ['100', '100']];
                foreach ($sizeTypes as $key => $type) {
                    $nameWithExt = $nameHash . '_' . $type . '.' . $ext;
                    $source = $storage_path . '/' . $path . '/' . $nameWithExt;
                    $info = getimagesize($file->getRealPath());
                    if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($file->getRealPath());
                    elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($file->getRealPath());
                    elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($file->getRealPath());

                    imagejpeg($image, $source, 70);
                    Image::make($file->getRealPath())->resize($sizes[$key][0], $sizes[$key][1])->save($source);
                }
                $coverField = $path . '/' . $nameHash;
            } else {
                $nameWithExt = $nameHash . $ext;
                $source = $storage_path . '/' . $path . '/' . $nameWithExt;
                Image::make($file)->save($source);
                $coverField = $path . '/' . $nameHash;
            }
            if ($existingMedia){
                foreach ($existingMedia as $itemMedia) {

                    if ($itemMedia['sizes'] == false) {
                        $oldImagePath = $storage_path . '/' . $itemMedia['cover'] . '.' . $itemMedia['ext'];
                        if (File::exists($oldImagePath)) {
                            unlink($oldImagePath);
                        }
                    } else {
                        foreach ($sizeTypes as $type) {
                            $oldImagePath = $storage_path . '/' . $itemMedia['cover'] . '_' . $type . '.' . $itemMedia['ext'];
                            if (File::exists($oldImagePath)) {
                                unlink($oldImagePath);
                            }
                        }
                    }
                }
                if ($field) {
                    Media::where('type', $path . '/' . $field)->where('type_id', $model->getKey())->delete();
                } else {
                    Media::where('type', $path)->where('type_id', $model->getKey())->delete();
                }
            }
            $media = new Media();
            $media->cover = $coverField;
            $media->ext = $ext;
            if ($field){
                $media->type = $path.'/'.$field;
            }else{
                $media->type = $path;
            }
            $media->type_id = $model->id;
            if ($sizes){
                $media->sizes = true;
            }
            $media->save();

        }
        return true;
    }

    public function deleteMediaTable($model,$media){
        $storage_path = storage_path('app/public');
        $path = class_basename($model);
        $sizeTypes = ['large', 'medium', 'small'];
        foreach ($media as $existingMedia){
            if ($existingMedia->sizes == true) {
                foreach ($sizeTypes as $type) {
                    $oldImagePath = $storage_path . '/' . $existingMedia->cover . '_' . $type . '.' . $existingMedia->ext;
                    if (File::exists($oldImagePath)) {
                        unlink($oldImagePath);
                    }
                }
            }else{
                $oldImagePath = $storage_path . '/' . $existingMedia->cover .'.'. $existingMedia->ext;
                if (File::exists($oldImagePath)) {
                    unlink($oldImagePath);
                }
            }
        }
        if($path == "Post") {
            Media::where('type', $path)->where('type_id', $model->getKey())->delete();
        }
        return true;
    }

    public function multiStore($model, $file ,$sizes = false){
        $storage_path = storage_path('app/public');
        $path = class_basename($model);
        $sizeTypes = ['large', 'medium', 'small'];

        if(!File::exists($storage_path.'/'.$path)) {
            File::makeDirectory($storage_path.'/'.$path,0777,true);
        }

        if(is_array($file)){
            $files = $file;
            foreach ($files as $file){
                $fileName = $file->getClientOriginalName();
                $ext = $file->getClientOriginalExtension();
                $nameHash = str_random(32);

                if ($sizes){
                    $sizes = [
                        ['700', '700'],
                        ['400', '400'],
                        ['100', '100']
                    ];

                    foreach ($sizeTypes as $key => $type) {
                        $nameWithExt = $nameHash . '_' . $type. '.' . $ext;
                        $source =$storage_path .'/'. $path . '/' .$nameWithExt;
                        $info = getimagesize($file->getRealPath());
                        if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($file->getRealPath());
                        elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($file->getRealPath());
                        elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($file->getRealPath());

                        imagejpeg($image, $source,70);

                        Image::make($file->getRealPath())->resize($sizes[$key][0], $sizes[$key][1])->save( $source );
                    }
                    $coverField =   $path.'/'.$nameHash;

                }else{
                    $nameWithExt = $nameHash . $ext;
                    $source = $storage_path .'/'. $path . '/' .$nameWithExt;
                    Image::make($file)->save( $source );

                    $coverField =   $path.'/'.$nameHash;
                }
                $media = new Media();
                $media->cover = $coverField;
                $media->ext = $ext;
                $media->type = $path;
                $media->type_id = $model->id;
                if ($sizes){
                    $media->sizes = true;
                }
                $media->save();

            }
        }
        return true;
    }

    public function multiUpdate($model, $file ,$sizes = false){
        $storage_path = storage_path('app/public');
        $path = class_basename($model);

        if(!File::exists($storage_path.'/'.$path)) {
            File::makeDirectory($storage_path.'/'.$path,0777,true);
        }
        $existingMedia = Media::where('post_id',$model->getKey())->get();
        if(is_array($file)){
            $files = $file;
            foreach ($files as $file){
                $fileName = $file->getClientOriginalName();
                $ext = $file->getClientOriginalExtension();
                $nameHash = str_random(32);

                if ($sizes){
                    $sizes = [
                        ['700', '700'],
                        ['400', '400'],
                        ['100', '100']
                    ];
                    $sizeTypes = ['large', 'medium', 'small'];

                    foreach ($sizeTypes as $key => $type) {
                        $nameWithExt = $nameHash . '_' . $type. '.' . $ext;
                        $source =$storage_path .'/'. $path . '/' .$nameWithExt;
                        $info = getimagesize($file->getRealPath());
                        if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($file->getRealPath());
                        elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($file->getRealPath());
                        elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($file->getRealPath());

                        imagejpeg($image, $source,70);

                        Image::make($file->getRealPath())->resize($sizes[$key][0], $sizes[$key][1])->save( $source );

                        if ($existingMedia->count()){
                            foreach ($existingMedia as $media){
                                $oldImagePath = $storage_path . '/' .$media->cover .'_'. $type .'.'.$media->ext;
                                if (File::exists($oldImagePath )) {
                                    unlink($oldImagePath);
                                }
                                $media->delete();
                            }
                        }
                    }
                    $coverField =   $path.'/'.$nameHash;

                }else{
                    $nameWithExt = $nameHash . $ext;
                    $source = $storage_path .'/'. $path . '/' .$nameWithExt;
                    Image::make($file)->save( $source );

                    $coverField =   $path.'/'.$nameHash;

                    if ($existingMedia->count()){
                        foreach ($existingMedia as $media){
                            $oldImagePath = $storage_path . '/' .$media->cover .'.'.$media->ext;
                            if (File::exists($oldImagePath )) {
                                unlink($oldImagePath);
                            }
                            $media->delete();
                        }
                    }
                }
            }
        }
        return true;
    }

    public function delete($model, $field = false, $field_icon = false)
    {
        $storage_path = storage_path('app/public');

        if($field){
            $sizeTypes = ['large', 'medium', 'small'];
            foreach($sizeTypes as $sizeType){
                $deletdImage = $storage_path . '/' . $model->$field . "_" . $sizeType . "." . $model->ext;
                if (File::exists($deletdImage)) {
                    unlink($deletdImage);
                }
            }
        }

        if($field_icon){
            $deletdImage = $storage_path . '/' . $model[$field_icon];
            if (File::exists($deletdImage)) {
                unlink($deletdImage);
            }
        }

        return true;
    }
}
