<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\PostView;

class DeletePostsView extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:posts_view';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'All views of the previous day successfully deleted.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        PostView::truncate();
        $this->info($this->description);
    }
}
