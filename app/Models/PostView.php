<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostView extends Model
{
    protected $table = 'posts_view';

    protected $fillable = [
        'post_id',
        'IP_address',
    ];
}
