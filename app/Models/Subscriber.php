<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    public function subscriber()
    {
        return $this->belongsToMany(Category::class, 'subscribers', 'id', 'category_id');
    }

    public function category()
    {
        return $this->hasOne(Category::class, 'subscribers', 'id', 'category_id');
    }
}
