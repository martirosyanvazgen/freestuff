<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title_en',
        'title_en',
        'title_en',
        'description_en',
        'description_en',
        'description_en',
        'text_en',
        'text_en',
        'text_en'
    ];

    public function media()
    {
        return $this->hasMany(Media::class, 'type_id', 'id')
            ->where('type', 'Blog/banner')
            ->orWhere('type', 'Blog/cover');
    }

    public function cover()
    {
        return $this->hasOne(Media::class, 'type_id', 'id')
            ->orWhere('type', 'Blog/cover');
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($blog) {
            $blog->media()->delete();
        });
    }
}
