<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    public $timestamps = false;

    public function group(){
        return $this->hasOne(FilterGroup::class,'id','group_id');
    }
}
