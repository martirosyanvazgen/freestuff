<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryFilterGroup extends Model
{
    public function filterGroup(){
        return $this->hasOne(FilterGroup::class,'id','filter_group_id');
    }
}
