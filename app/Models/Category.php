<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name_en',
        'name_am',
        'name_ru',
        'parent_id',
        'slug',
        'cover',
        'icon'
    ];

    public function child(){
        return $this->hasMany(Category::class,'parent_id','id');
    }
    public function parent(){
        return $this->hasOne(Menu::class,'id','parent_id');
    }

    public function filterGroups()
    {
        return $this->belongsToMany(FilterGroup::class,'category_filter_groups');
    }

    public function getFilterGroupsIdsAttribute()
    {
        return $this->filterGroups->pluck('id');
    }
}
