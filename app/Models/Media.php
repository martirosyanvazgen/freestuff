<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title_en',
        'title_ru',
        'title_am',
        'description_en',
        'description_ru',
        'description_am',
        'text_en',
        'text_ru',
        'text_am',
        'city_id',
        'address',
        'is_active',
        'created_user_id'
    ];
}
