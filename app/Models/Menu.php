<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title_en',
        'title_ru',
        'title_am',
        'slug',
        'page_id',
        'type',
        'parent_id',
    ];

    public function parent(){
        return $this->hasOne(Menu::class,'id','parent_id');
    }

    public function child(){
        return $this->hasMany(Menu::class,'parent_id','id');
    }

    public function menuPage()
    {
        return $this->hasOne(Page::class, 'id', 'page_id');
    }

    public function menuType(){
        return $this->hasOne(MenuType::class, 'id', 'type');
    }
}
