<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FilterGroup extends Model
{
    public $timestamps = false;

    public function filters(){
        return $this->hasMany(Filter::class,'group_id','id');
    }

    public function threeFilters(){
        return $this->hasMany(Filter::class,'group_id','id')->take(3);
    }

    public function getFilterIds(){
        return $this->filters()->pluck('id');
    }
}
