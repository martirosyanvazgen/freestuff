<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = [
        'title_en',
        'title_ru',
        'title_am',
        'description_en',
        'description_ru',
        'description_am',
        'text_en',
        'text_ru',
        'text_am',
        'cover'
    ];
}
