<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostDetail extends Model
{
    public $timestamps = false;
}
