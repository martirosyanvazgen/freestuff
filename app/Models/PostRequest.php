<?php

namespace App\Models;

use App\Traits\ModelCreatedByUser;
use Illuminate\Database\Eloquent\Model;

class PostRequest extends Model
{

    use ModelCreatedByUser;

    protected $fillable = ['request_text', 'approved', 'post_id', 'created_user_id'];
    public function post()
    {
        return $this->hasOne(Post::class, 'id', 'post_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'created_user_id');
    }

    public function notification()
    {
        return $this->hasOne(Notification::class, 'request_id', 'id');
    }
}
