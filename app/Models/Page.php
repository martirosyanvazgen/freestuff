<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [
        'title_en',
        'title_ru',
        'title_am',
        'text_en',
        'text_ru',
        'text_am',
    ];
}
