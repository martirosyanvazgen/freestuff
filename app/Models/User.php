<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Traits\MultyGuard;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
//    use HasRoles;
    use MultyGuard;

//    protected $guard_name = 'api';

    protected $fillable = [
        'login',
        'email',
        'password',
        'password_changed_at',
        'name',
        'surname',
        'region_id',
        'city_id',
        'address',
        'phone_number',
        'active',
        'notification_on',
        'provider',
        'provider_id'
    ];

    protected $dates = [
        'password_changed_at',
    ];

    protected $casts = [
        'notification_on' => 'boolean',
        'active' => 'boolean',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
