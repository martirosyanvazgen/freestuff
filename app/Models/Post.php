<?php

namespace App\Models;

use App\Traits\ModelCreatedByUser;
use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App;

class Post extends Model
{
    use ModelCreatedByUser;
    use SoftDeletes, CascadeSoftDeletes;

    protected $lang;

    protected $cascadeDeletes = ['post_requests', 'post_categories'];

    protected $dates = ['deleted_at'];

    public function __construct(array $attributes = ['post_requests', 'post_categories'])
    {
        parent::__construct($attributes);
        $this->lang = 'en';
    }

    public function post_requests()
    {
        return $this->hasMany(PostRequest::class);
    }

    public function post_categories()
    {
        return $this->hasMany(PostCategory::class);
    }

    protected $fillable = [
        'title_en',
        'title_ru',
        'title_am',
        'description_en',
        'description_ru',
        'description_am',
        'text_en',
        'text_ru',
        'text_am',
        'city_id',
        'address',
        'is_active',
        'created_user_id',
        'approved'
    ];

    public function scopeFindByCategoryId($query, $categoryId)
    {
        return $query->whereHas('postCategory', function ($query) use ($categoryId) {
            $query->where('category_id', $categoryId);
        });
    }

    public function scopeFindByConfirmedPostRequest($query, $auth_id)
    {
        return $query->whereHas('postRequest', function ($query) use ($auth_id) {
            $query->where('created_user_id', $auth_id);
        });
    }

    public function postCategory()
    {
        return $this->belongsToMany(Category::class,'post_categories');
    }

    public function postRequest()
    {
        return $this->hasMany(PostRequest::class);
    }

    public function user()
    {
        return $this->hasOne(User::class,'id', 'created_user_id');
    }

    public function media()
    {
        return $this->hasMany(Media::class, 'type_id', 'id')
            ->Where('type','Post');
    }

    public function firstMedia()
    {
        return $this->hasOne(Media::class, 'post_id', 'id');
    }

    public function details()
    {
        return $this->hasMany(PostDetail::class, 'post_id', 'id');
    }

    public function category()
    {
        return $this->belongsToMany(Category::class, 'post_categories')->select(
            'categories.id as id',
            "slug",
            "parent_id",
            "name_".$this->lang,
            "cover",
            "icon"
        );
    }

    public function getCategoryIds(){
        return $this->category->pluck('id');
    }
}
